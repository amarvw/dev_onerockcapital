# Generated by Django 3.0.8 on 2021-06-11 08:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('assessment_app', '0003_auto_20210611_1249'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='userdetails',
            name='added_by',
        ),
        migrations.RemoveField(
            model_name='userdetails',
            name='added_on',
        ),
        migrations.RemoveField(
            model_name='userdetails',
            name='modified_by',
        ),
        migrations.RemoveField(
            model_name='userdetails',
            name='modified_on',
        ),
        migrations.AlterField(
            model_name='userdetails',
            name='is_orc_employee',
            field=models.BooleanField(choices=[('', 'Employee Type'), (True, 'One Rock Capital Employee'), (False, 'Non One Rock Capital Employee')]),
        ),
    ]
