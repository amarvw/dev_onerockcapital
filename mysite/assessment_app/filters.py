import django_filters
from django.contrib.auth.models import User
from .models import *

is_active= (
    ('0','Active'),
    ('1','Inactive'),
)

active_tup = (
    ('1','Active'),
    ('0','Inactive'),
)


class UserFormFilter(django_filters.FilterSet):
    is_delete = django_filters.ChoiceFilter(choices=is_active, label='Status', initial='0')

    class Meta:
        model = UserDetails
        fields = ['is_delete', 'user','designation_title']

    def __init__(self, data, *args, **kwargs):
        if not data.get('is_delete'):
            data = data.copy()
            data['is_delete'] = '0'
        super().__init__(data, *args, **kwargs)


class PortcoFormFilter(django_filters.FilterSet):
    is_delete = django_filters.ChoiceFilter(choices=is_active, label='Status')

    class Meta:
        model = PorfolioCompanyMaster
        fields = ['portco_name','is_delete']

    def __init__(self, data, *args, **kwargs):
        if not data.get('is_delete'):
            data = data.copy()
            try:
                if data['is_delete'] == '':
                    data['is_delete'] = ''
                else:
                    data['is_delete'] = '0'
            except:
                data['is_delete'] = '0'
        super().__init__(data, *args, **kwargs)


class PlantFormFilter(django_filters.FilterSet):
    is_delete = django_filters.ChoiceFilter(choices=is_active, label='Status')

    class Meta:
        model = PlantMaster
        fields = ['plant_name', 'portco', 'is_delete']

    def __init__(self, data, *args, **kwargs):
        self.current_user = kwargs.get('current_user', None)
        del kwargs['current_user']
        if not data.get('is_delete'):
            data = data.copy()
            try:
                if data['is_delete'] == '':
                    data['is_delete'] = ''
                else:
                    data['is_delete'] = '0'
                if data['client'] == '':
                    data['client'] = ''
                else:
                    data['client'] = data['client']
            except:
                data['is_delete'] = '0'
                data['client'] = ''
        super(PlantFormFilter, self).__init__(data, *args, **kwargs)


class LshipFormFilter(django_filters.FilterSet):
    is_active = django_filters.ChoiceFilter(choices=active_tup, label='Status', initial='1')

    class Meta:
        model = Leadership_Representation
        fields = [ 'category_name','level_name']

    def __init__(self, data, *args, **kwargs):
        if not data.get('is_active'):
            data = data.copy()
            data['is_active'] = '1'
        super().__init__(data, *args, **kwargs)


class LshipCategoryFormFilter(django_filters.FilterSet):
    is_active = django_filters.ChoiceFilter(choices=active_tup, label='Status', initial='1')

    class Meta:
        model = Leadership_Category
        fields = ['is_active', 'category_name','category_description']

    def __init__(self, data, *args, **kwargs):
        if not data.get('is_active'):
            data = data.copy()
            data['is_active'] = '1'
        super().__init__(data, *args, **kwargs)


class LshipLevelFormFilter(django_filters.FilterSet):
    is_active = django_filters.ChoiceFilter(choices=active_tup, label='Status', initial='1')

    class Meta:
        model = Leadership_Level
        fields = ['is_active', 'level_name','level_description']

    def __init__(self, data, *args, **kwargs):
        if not data.get('is_active'):
            data = data.copy()
            data['is_active'] = '1'
        super().__init__(data, *args, **kwargs)


class TrainingFormFilter(django_filters.FilterSet):
    is_delete = django_filters.ChoiceFilter(choices=is_active, label='Status')

    class Meta:
        model = TrainingContentMaster
        fields = ['module_name','is_delete']

    def __init__(self, data, *args, **kwargs):
        if not data.get('is_delete'):
            data = data.copy()
            try:
                if data['is_delete'] == '':
                    data['is_delete'] = ''
                else:
                    data['is_delete'] = '0'
            except:
                data['is_delete'] = '0'
        super().__init__(data, *args, **kwargs)