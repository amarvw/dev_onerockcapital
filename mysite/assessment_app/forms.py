from django import forms
from django.forms import ModelForm, Select
from django.shortcuts import get_object_or_404

from .models import *


class DateInput(forms.DateInput):
    input_type = 'date'


class FormUserExtension(forms.ModelForm):
    class Meta:
        model = UserDetails
        fields = ['designation_title', 'is_orc_employee','is_delete','portco','user']


class FormPortco(forms.ModelForm):
    description = forms.CharField(widget=forms.Textarea)

    class Meta:
        model = PorfolioCompanyMaster
        fields = ['portco_name', 'hq_state','is_aquired', 'hq_country', 'primary_contact', 'primary_contact_mail', 'description', 'industry', 'added_on', 'is_delete']

    def __init__(self, *args, **kwargs):
        super(FormPortco, self).__init__(*args, **kwargs)
        self.fields['hq_state'].queryset = CountryStateMapping.objects.none()


class FormPortcoUpdate(forms.ModelForm):
    description = forms.CharField(widget=forms.Textarea)

    class Meta:
        model = PorfolioCompanyMaster
        fields = ['portco_name', 'hq_state','is_aquired', 'hq_country', 'primary_contact', 'primary_contact_mail', 'description', 'industry', 'added_on', 'is_delete']

    def __init__(self, country, *args, **kwargs):
        super(FormPortcoUpdate, self).__init__(*args, **kwargs)
        country = get_object_or_404(RegionCountryMapping, pk=country.id)
        self.fields['hq_state'].queryset = CountryStateMapping.objects.filter(country=country)
    
class FormPlant(forms.ModelForm):
    description = forms.CharField(widget=forms.Textarea)

    class Meta:
        model = PlantMaster
        fields = ['plant_name', 'region', 'state', 'country', 'portco', 'primary_contact', 'primary_contact_mail','description', 'added_on', 'is_delete']

    def __init__(self, current_user, *args, **kwargs):
        super(FormPlant, self).__init__(*args, **kwargs)
        self.fields['state'].queryset = CountryStateMapping.objects.none()
        self.fields['portco'].queryset = PorfolioCompanyMaster.objects.filter(is_delete=False)


class FormPlantUpdate(forms.ModelForm):
    description = forms.CharField(widget=forms.Textarea)

    class Meta:
        model = PlantMaster
        fields = ['plant_name', 'state', 'country', 'portco', 'primary_contact', 'primary_contact_mail','description', 'added_on', 'is_delete']

    def __init__(self, country, *args, **kwargs):
        super(FormPlantUpdate, self).__init__(*args, **kwargs)
        country = get_object_or_404(RegionCountryMapping, pk=country.id)
        self.fields['state'].queryset = CountryStateMapping.objects.filter(country=country)
        self.fields['portco'].queryset = PorfolioCompanyMaster.objects.filter(is_delete=False)


class FormElsCategory(forms.ModelForm):
    category_description = forms.CharField(widget=forms.Textarea)

    class Meta:
        model = ELSCategoryMaster
        fields = ['category_name', 'four_P_category','category_description', 'is_delete']


class FormAreaDetails(forms.ModelForm):
    category_description = forms.CharField(widget=forms.Textarea)

    class Meta:
        model = AreaDetails
        fields = ['area_name','description', 'is_delete']


class FormLshipCategory(forms.ModelForm):
    category_description = forms.CharField(widget=forms.Textarea)

    class Meta:
        model = Leadership_Category
        fields = ['category_name','category_description', 'is_active']


class FormLshipLevel(forms.ModelForm):
    level_description = forms.CharField(widget=forms.Textarea)

    class Meta:
        model = Leadership_Level
        fields = ['level_name','level_description', 'is_active']


class FormLshipRepresentationCategory(forms.ModelForm):
    status = (
        (True, 'Active'),
        (False, 'Inactive')
    )
    category_name = forms.CharField(max_length=100, disabled=True)
    category_description = forms.CharField(widget=forms.Textarea, disabled=True)
    level_description = forms.CharField(widget=forms.Textarea, disabled=True)
    level_name = forms.CharField(max_length=100, disabled=True)



    class Meta:
        model = Leadership_Representation
        fields = ['category_name', 'category_description', 'level_name', 'level_description']


class FormTraining(forms.ModelForm):
    module_description = forms.CharField(widget=forms.Textarea)

    class Meta:
        model = TrainingContentMaster
        fields = ['module_name', 'module_description', 'is_delete', 'document_name']

