# Generated by Django 3.0.8 on 2021-07-08 09:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('assessment_app', '0017_trainingcontentmaster'),
    ]

    operations = [
        migrations.AddField(
            model_name='trainingcontentmaster',
            name='document',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
    ]
