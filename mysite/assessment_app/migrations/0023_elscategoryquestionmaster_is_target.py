# Generated by Django 3.0.8 on 2021-07-25 17:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('assessment_app', '0022_elscategoryquestionmaster_elscategoryquestionresponse'),
    ]

    operations = [
        migrations.AddField(
            model_name='elscategoryquestionmaster',
            name='is_target',
            field=models.BooleanField(default=True),
        ),
    ]
