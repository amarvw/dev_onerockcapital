from django.apps import AppConfig


class AssessmentAppConfig(AppConfig):
    name = 'assessment_app'
