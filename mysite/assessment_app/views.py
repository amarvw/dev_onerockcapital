import io
from zipfile import ZipFile

import MySQLdb
from django.contrib.auth import authenticate, login, logout
from django.core.files.storage import FileSystemStorage
from django.db.models.functions import TruncDate, TruncMonth
from django.shortcuts import render
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.decorators import login_required
from openpyxl.styles.protection import Protection
from .forms import *
from django.conf import settings
from django.http import HttpResponse, Http404, FileResponse
from django.shortcuts import HttpResponseRedirect
from django.contrib import messages
import dateutil.relativedelta
from django.template import RequestContext
from .models import *
from .filters import *
# from .join import *
import json
from datetime import datetime
from django.contrib.auth.models import User
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.db.models import Sum, Avg, Max, Min, F, Count
import decimal
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.decorators import user_passes_test
import xlwt
from copy import copy
from xlrd import *
import openpyxl
from openpyxl.styles import Border, Side, PatternFill, Font, GradientFill, Alignment
from openpyxl.utils.cell import coordinate_from_string, column_index_from_string
from xlrd import open_workbook
import pandas as pd
import numpy as np
from django.db import connection
import decimal
from dateutil.relativedelta import relativedelta
import json
from django.contrib.auth.decorators import user_passes_test
import shutil
import plotly.graph_objects as go
import plotly.offline as pyo
# from plotly.offline import plot
import plotly.express as px


def is_portco_plant_selected(function):
    def wrap(request, *args, **kwargs):
        if ('global_portco' in request.session and 'global_plant' in request.session) and (
                request.session['global_portco'] is not None and request.session['global_plant'] is not None):
            return function(request, *args, **kwargs)
        else:
            request.session['selection'] = False
            return redirect('home')
    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap


def is_portco_selected(function):
    def wrap(request, *args, **kwargs):
        if ('global_portco' in request.session) and (
                request.session['global_portco'] is not None):
            return function(request, *args, **kwargs)
        else:
            return redirect('home')
    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap
# Create your views here.

def loginUser(request):
    if request.method == "POST":
        form = AuthenticationForm(request=request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            user_id = User.objects.get(username=username).pk
            is_delete = UserDetails.objects.get(user=user_id).is_delete

            if (user is not None) and (is_delete == False):

                if password == "Alpha@123":
                    login(request, user)
                    return redirect('change_password')
                else:
                    login(request, user)
                    return redirect('/')
            else:
                messages.error(request, 'Incorrect Username/Password')
                return redirect('login')
        else:
            messages.error(request, 'Incorrect Username/Password')
            return redirect('login')
    form = AuthenticationForm()
    return render(request=request, template_name="registration/login.html", context={"form": form})


def signUp(request):
    if request.method == "POST":
        try:
            user = User.objects.get(username=request.POST['email'])
            msg = 'User with Email ID already exist'
            form = FormUserExtension()
            return render(request=request, template_name="registration/sign-up.html",
                          context={"form": form, 'msg': msg})
        except User.DoesNotExist:
            if request.POST['is_orc_employee'] == 'True':
                is_orc_employee = True
            else:
                is_orc_employee = False
            # if request.POST['is_admin'] == '1':
            #     is_admin = True
            # else:
            #     is_admin = False
            user = User.objects.create_user(username=request.POST['email'], password=request.POST['password'],
                                            first_name=request.POST['fname'].title(),
                                            last_name=request.POST['lname'].title(), email=request.POST['email'],
                                            is_superuser=False)
            # new = UserDetails(designation=request.POST['designation_title'], added_by=request.user,
            #                   added_on=datetime.now(), modified_by=request.user, modified_on=datetime.now(), user=user)
            new = UserDetails(designation_title=request.POST['designation_title'], is_orc_employee=is_orc_employee, user=user)
            new.save()
            msg ='User request sent for approval. Please contact admin at One Rock to approve.'
            form = FormUserExtension()
            return render(request=request, template_name="registration/sign-up.html", context={"form": form, 'msg':msg})
    else:
        form = FormUserExtension()
        return render(request=request, template_name="registration/sign-up.html", context={"form": form})


def logoutUser(request):
    logout(request)
    messages.info(request, "Logged out successfully!")
    return HttpResponseRedirect("/login/")


def fetchSummaryTable(request, act=0):
    if act == 0:
        with connection.cursor() as cursor:
            cursor.execute("""select portco_id, plant_id, date_of_assessment_started, (count(els_category_id)/(select count(*) from assessment_app_elscategorymaster where is_delete = False))*100 as status, if(sum(score_y_n) >=0, True, False) as is_y_n, if(sum(score_id) >=0, True, False) as is_normal
                                from assessment_app_elsratings
                                where portco_id = %s and plant_id = %s
                                group by portco_id, plant_id, date_of_assessment_started
                                order by date_of_assessment_started desc;""", [request.session['global_portco'], request.session['global_plant']])
            columns = cursor.description
            summary_table = []
            for value in cursor.fetchall():
                tmp = {}
                for (index, column) in enumerate(value):
                    tmp[columns[index][0]] = column
                summary_table.append(tmp)
            summary_table = list(summary_table)
    elif act == 1:
        with connection.cursor() as cursor:
            cursor.execute("""select ar.portco_id, ar.plant_id, ar.area_id, ad.area_name, ar.date_of_assessment_started, (count(ar.els_category_id)/(select count(*) from assessment_app_elscategorymaster where is_delete = False))*100 as status
                                    from assessment_app_arearatings ar
                                    left join assessment_app_areadetails ad on ar.area_id = ad.id
                                    where ar.portco_id = %s and ar.plant_id = %s
                                    group by ar.portco_id, ar.plant_id, ar.area_id, ar.date_of_assessment_started
                                    order by ar.date_of_assessment_started desc;""", [request.session['global_portco'], request.session['global_plant']])
            columns = cursor.description
            summary_table = []
            for value in cursor.fetchall():
                tmp = {}
                for (index, column) in enumerate(value):
                    tmp[columns[index][0]] = column
                summary_table.append(tmp)
            summary_table = list(summary_table)
    elif act == 2:
        with connection.cursor() as cursor:
            cursor.execute("""select portco_id, null as plant_id,null as area_id, null as area_name, date_of_assessment_started, (count(lship_id_id)/(select count(*) from assessment_app_leadership_representation where is_active = True))*100 as status
                            from assessment_app_leadershipratings
                            where portco_id = %s
                            group by portco_id, plant_id, date_of_assessment_started
                            order by date_of_assessment_started desc;""", [request.session['global_portco']])
            columns = cursor.description
            summary_table = []
            for value in cursor.fetchall():
                tmp = {}
                for (index, column) in enumerate(value):
                    tmp[columns[index][0]] = column
                summary_table.append(tmp)
            summary_table = list(summary_table)
    # with connection.cursor() as cursor:
    #     cursor.execute("""(select *
    #                         from (select portco_id, plant_id,null as area_id, null as area_name, date_of_assessment_started, (count(els_category_id)/(select count(*) from assessment_app_elscategorymaster where is_delete = False))*100 as status, "ELS Assessment" as assessment_type
    #                         from assessment_app_elsratings
    #                         group by portco_id, plant_id, date_of_assessment_started
    #                         order by date_of_assessment_started desc) as abc)
    #                         union all
    #                         (select *
    #                         from (select ar.portco_id, ar.plant_id, ar.area_id, ad.area_name, ar.date_of_assessment_started, (count(ar.els_category_id)/(select count(*) from assessment_app_elscategorymaster where is_delete = False))*100 as status, "Area Assessment" as assessment_type
    #                             from assessment_app_arearatings ar
    #                             left join assessment_app_areadetails ad on ar.area_id = ad.id
    #                             group by ar.portco_id, ar.plant_id, ar.area_id, ar.date_of_assessment_started
    #                             order by ar.date_of_assessment_started desc) as xyz) order by date_of_assessment_started desc;""")
        # columns = cursor.description
        # summary_table = []
        # for value in cursor.fetchall():
        #     tmp = {}
        #     for (index, column) in enumerate(value):
        #         tmp[columns[index][0]] = column
        #     summary_table.append(tmp)
        # summary_table = list(summary_table)
    return summary_table


def checkIsAcquired(portco):
    is_ac = portco.is_aquired
    return is_ac


@login_required(login_url='login')
def indexView(request):
    if request.method == 'POST':
        request.session['global_portco'] = request.POST['portco']
        request.session['global_plant'] = request.POST['plant']
        msg = 'Portfolio Company and Plant is successfully selected'
        user_details = UserDetails.objects.get(user=request.user.id)
        if user_details.is_orc_employee == True:
            portco = PorfolioCompanyMaster.objects.filter(is_delete=False)
            ps = PorfolioCompanyMaster.objects.get(id=request.session['global_portco'])
            plant = PlantMaster.objects.filter(portco=ps, is_delete=False)
        else:
            portco = PorfolioCompanyMaster.objects.filter(id=user_details.portco.id, is_delete=False)
            p = PorfolioCompanyMaster.objects.get(id=user_details.portco.id)
            plant = PlantMaster.objects.filter(portco=p, is_delete=False)
        summary_table_els = fetchSummaryTable(request, act=0)
        summary_table_area = fetchSummaryTable(request, act=1)
        summary_table_lead = fetchSummaryTable(request, act=2)
        is_acquired = checkIsAcquired(selected_portco(request))
        return render(request, "admin/landing_page/landing_page.html", {'portco': portco, 'plant': plant, 'msg': msg, 'summary_list_els': summary_table_els, 'summary_list_area': summary_table_area, 'summary_list_lead': summary_table_lead,
                                                                        'portco_selected': request.session['global_portco'],
                                                                        'plant_selected': request.session['global_plant'], 'is_acquired': is_acquired,
                                                                        'selection':True})
    else:
        if 'global_portco' in request.session and 'global_plant' in request.session:
            user_details = UserDetails.objects.get(user=request.user.id)
            if user_details.is_orc_employee == True:
                portco = PorfolioCompanyMaster.objects.filter(is_delete=False)
                ps = PorfolioCompanyMaster.objects.get(id=request.session['global_portco'])
                plant = PlantMaster.objects.filter(portco=ps, is_delete=False)
            else:
                portco = PorfolioCompanyMaster.objects.filter(id=user_details.portco.id, is_delete=False)
                p = PorfolioCompanyMaster.objects.get(id=user_details.portco.id)
                plant = PlantMaster.objects.filter(portco=p, is_delete=False)
            summary_table_els = fetchSummaryTable(request, act=0)
            summary_table_area = fetchSummaryTable(request, act=1)
            summary_table_lead = fetchSummaryTable(request, act=2)
            is_acquired = checkIsAcquired(selected_portco(request))
            msg = ''
            if 'els_add' in request.session and request.session['els_add'] is not None:
                if request.session['els_add']:
                    msg = 'ELS Assessment successfully added'
                elif request.session['els_add'] == False:
                    msg = 'ELS Category already exist'
            if 'els_update' in request.session and request.session['els_update'] is not None:
                if request.session['els_update']:
                    msg = 'ELS Assessment successfully updated'
                elif request.session['els_update'] == False:
                    msg = 'ELS Assessment update failed. Please check the category name before updating.'
            if 'area_assessment_add' in request.session and request.session['area_assessment_add'] is not None:
                if request.session['area_assessment_add']:
                    msg = 'Area assessment successfully added'
                elif request.session['area_assessment_add'] == False:
                    msg = 'Area assessment already exist'
            if 'area_assessment_update' in request.session and request.session['area_assessment_update'] is not None:
                if request.session['area_assessment_update']:
                    msg = 'Area assessment successfully updated'
                elif request.session['area_assessment_update'] == False:
                    msg = 'Area assessment update failed. Please check the date before updating.'
            if 'lship_add' in request.session and request.session['lship_add'] is not None:
                if request.session['lship_add']:
                    msg = 'Leadership Assessment successfully added'
                elif request.session['lship_add'] == False:
                    msg = 'Leadership Assessment already exist'
            if 'lship_update' in request.session and request.session['lship_update'] is not None:
                if request.session['lship_update']:
                    msg = 'Leadership Assessment successfully updated'
                elif request.session['lship_update'] == False:
                    msg = 'Leadership Assessment update failed. Please check the details before updating.'
            request.session['els_add'] = None
            request.session['els_update'] = None
            request.session['area_assessment_add'] = None
            request.session['area_assessment_update'] = None
            request.session['lship_add'] = None
            request.session['lship_update'] = None
            return render(request, "admin/landing_page/landing_page.html",
                          {'portco': portco, 'plant': plant, 'msg': msg, 'summary_list_els': summary_table_els, 'summary_list_area': summary_table_area, 'summary_list_lead': summary_table_lead,
                           'portco_selected': request.session['global_portco'],
                           'plant_selected': request.session['global_plant'],  'is_acquired': is_acquired,
                           'selection': True})
        else:
            msg = ''
            if 'selection' in request.session and request.session['selection'] is not None:
                if request.session['selection'] == False:
                    msg = 'Please select a Portfolio Company and a Plant to access the requested section.'
            request.session['selection'] = None
            user_details = UserDetails.objects.get(user=request.user.id)
            if user_details.is_orc_employee == True:
                portco = PorfolioCompanyMaster.objects.filter(is_delete=False)
                is_acquired = False
            else:
                portco = PorfolioCompanyMaster.objects.filter(id=user_details.portco.id, is_delete=False)
                is_acquired = checkIsAcquired(PorfolioCompanyMaster.objects.get(id=user_details.portco.id))
            return render(request, "admin/landing_page/landing_page.html", {'portco':portco,'msg':msg,  'is_acquired': is_acquired})


#################################################### Employee Section #####################-----Mani's code
@user_passes_test(lambda u: u.is_superuser)
@login_required(login_url='login')
def employeeList(request):
    if 'global_portco' in request.session:
        is_acquired = checkIsAcquired(selected_portco(request))
    else:
        is_acquired = False
    user_details = UserDetails.objects.all()
    filter = UserFormFilter(request.GET, queryset=user_details)
    user_details = filter.qs
    msg = ''
    if 'user_add' in request.session:
        if request.session['user_add']:
            msg = 'Employee successfully added'
        elif request.session['user_add'] == False:
            msg = 'Employee with employee code already exist'
    if 'user_update' in request.session:
        if request.session['user_update']:
            msg = 'Employee successfully updated'
        elif request.session['user_update'] == False:
            msg = 'Employee update failed. Please check the Id before updating.'
    if 'password_reset' in request.session and request.session['password_reset'] is not None:
        if request.session['password_reset']:
            msg = 'Password successfully reset'
    context = {'employee_list': user_details,'myFilter':filter, 'msg':msg, 'is_acquired': is_acquired}
    request.session['user_add'] = None
    request.session['user_update'] = None
    request.session['password_reset'] = None
    return render(request, "admin/employee_section/employee_list_page.html", context)
#
#
# @user_passes_test(lambda u: u.is_superuser)
# @login_required(login_url='login')
# def save_employee_form(request, form, template_name):
#     data = dict()
#     if request.method == 'POST':
#         if form.is_valid():
#             form.save()
#             data['form_is_valid'] = True
#             client = ClientMaster.objects.all().filter(is_delete=False)
#             data['html_client_list'] = render_to_string('partial_client_list.html', {'client_list': client})
#         else:
#             data['form_is_valid'] = False
#     context = {'form': form}
#     data['html_form'] = render_to_string(template_name, context, request=request)
#     return JsonResponse(data)

@user_passes_test(lambda u: u.is_superuser)
@login_required(login_url='login')
def employeeCreate(request):
    if 'global_portco' in request.session:
        is_acquired = checkIsAcquired(selected_portco(request))
    else:
        is_acquired = False
    if request.method == 'POST':

        if request.POST['is_orc_employee'] == 'True':
            is_orc_employee = True
            portco = None
            if request.POST['is_superuser'] == 'True':
                is_admin = True
            else:
                is_admin = False
        else:
            is_orc_employee = False
            is_admin = False
            portco = get_object_or_404(PorfolioCompanyMaster, pk=request.POST['portco'])

        try:
            user = User.objects.get(username=request.POST['username'])
            form = FormUserExtension()
            request.session['user_add'] = False
            return redirect('employee_list')
            # return render(request, 'admin/employee_section/employee_list_page.html', {'employee_list': UserDetails.objects.all(), 'error': "Username Exist"})
        except User.DoesNotExist:

            user = User.objects.create_user(username=request.POST['username'], password="Alpha@123",first_name=request.POST['fname'].title(),last_name=request.POST['lname'].title() , email=request.POST['email'], is_superuser=is_admin)
            # new = UserDetails(designation_title=request.POST['designation_title'],added_by=request.user, added_on=datetime.now(),modified_by=request.user ,modified_on=datetime.now(), user=user)




            new = UserDetails(designation_title=request.POST['designation_title'],is_orc_employee=is_orc_employee, portco = portco,user=user)
            new.save()
            request.session['user_add'] = True
            return redirect('employee_list')
    else:
        form = FormUserExtension()
        # return save_employee_form(request, form, 'admin/employee_section/employee_create.html')
        return render(request, "admin/employee_section/employee_create.html", {'form': form, 'is_acquired': is_acquired})
#

@user_passes_test(lambda u: u.is_superuser)
@login_required(login_url='login')
def employeeUpdate(request, pk):
    if 'global_portco' in request.session:
        is_acquired = checkIsAcquired(selected_portco(request))
    else:
        is_acquired = False
    a = UserDetails.objects.filter(id=pk).values_list('user', flat=True)[0]
    user = get_object_or_404(User, pk=a)
    userdetails = get_object_or_404(UserDetails, pk=pk)
    # request.session['user_update'] = False
    # user = User.objects.filter(pk=pk).update(is_delete=True)
    if request.method == 'POST':
        if request.POST['is_orc_employee'] == 'True':
            is_orc_employee = True
            portco = None
            if request.POST['is_superuser'] == 'True':
                is_admin = True
            else:
                is_admin = False
        else:
            is_orc_employee = False
            is_admin = False
            portco = get_object_or_404(PorfolioCompanyMaster, pk=request.POST['portco'])

        try:
            User.objects.filter(pk=a).update(username=request.POST['username'], first_name=request.POST['fname'].title(), last_name=request.POST['lname'].title(), email=request.POST['email'], is_superuser=is_admin)
            UserDetails.objects.filter(pk=pk).update( designation_title=request.POST['designation_title'], is_delete=request.POST['is_delete'], is_orc_employee = is_orc_employee,portco = portco)
            form = FormUserExtension(request.POST, instance=userdetails)
            request.session['user_update'] = True
        except:

            request.session['user_update'] = False
        return redirect('employee_list')
    else:
        form = FormUserExtension(instance=userdetails)
        is_superuser = form.initial['user']
        is_superuser = User.objects.get(id=is_superuser)
        is_superuser = is_superuser.is_superuser


    context = {'form': form, 'user_selected': user,"is_superuser" : is_superuser, 'is_acquired': is_acquired}
    return render(request, "admin/employee_section/employee_update.html", context)


@user_passes_test(lambda u: u.is_superuser)
@login_required(login_url='login')
def resetPassword(request,id):
    a = UserDetails.objects.filter(id=id).values_list('user', flat=True)[0]
    user = get_object_or_404(User, pk=a)
    user.set_password('Alpha@123')
    user.save()
    request.session['password_reset'] = True
    if user.pk == request.user.pk:
        return redirect('change_password')
    else:
        return redirect('employee_list')


@login_required(login_url='login')
def changePassword(request):
    if request.method == 'POST':
        if request.POST['new_password1'] != request.POST['new_password2']:
            messages.error(request, 'New password mismatch')
            form = PasswordChangeForm(request.user)
        else:
            form = PasswordChangeForm(request.user, request.POST)
            if form.is_valid():
                user = form.save()
                update_session_auth_hash(request, user)  # Important!
                messages.success(request, 'Your password was successfully updated!')
                return redirect('home')
            else:
                messages.error(request, 'Incorrect old password')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'registration/change_password.html', {
        'form': form
    })


@login_required(login_url='login')
def selected_portco(request):
    portco = PorfolioCompanyMaster.objects.get(id=request.session['global_portco'])
    return portco


@login_required(login_url='login')
def selected_plant(request):
    plant = PlantMaster.objects.get(id=request.session['global_plant'])
    return plant


@login_required(login_url='login')
def loadPlant(request):
    portco = request.GET.get('portco')
    plant = PlantMaster.objects.filter(portco=portco, is_delete=False)
    return render(request, 'admin/plant_dropdown_list_options.html', {'plant': plant})


@user_passes_test(lambda u: u.is_superuser)
@login_required(login_url='login')
def signupRequestList(request):
    if request.method == 'GET':
        tr = UserDetails.objects.filter(is_confirm=False)
        msg = ''
        if 'approve_all' in request.session and request.session['approve_all'] is not None:
            if request.session['approve_all']:
                msg = 'All delete transaction request were successfully approved.'
        if 'approve' in request.session and request.session['approve'] is not None:
            if request.session['approve']:
                msg = 'Delete transaction successfully approved.'
        if 'reject' in request.session and request.session['reject'] is not None:
            if request.session['reject']:
                msg = 'User Signup request successfully rejected.'
        request.session['approve_all'] = None
        request.session['approve'] = None
        request.session['reject'] = None
        context = {'tr': tr, 'msg': msg}
        return render(request, "admin/signup_request/signup_request_list_page.html", context)


@user_passes_test(lambda u: u.is_superuser)
@login_required(login_url='login')
def signupRequestApproval(request, id, act):
    if id=='0':
        # delete_tr = DeleteTransactionRequest.objects.filter(transaction__portco=portco, transaction__plant=plant).values()
        # if delete_tr.count() > 0:
        #     for del_tr in delete_tr:
        #         id = del_tr['id']
        #         pk = del_tr['transaction_id']
        #         transaction = get_object_or_404(TransactionMaster, pk=pk)
        #         ## act = 0 means reject and act = 1 approve
        #         if act == '0':
        #             DeleteTransactionRequest.objects.filter(id=id).delete()
        #             TransactionMaster.objects.filter(id=pk).update(is_delete=False)
        #             return redirect('delete_list')
        #         elif act == '1':
        #             tr = transaction.operation_type
        #             quantity = transaction.quantity
        #             # security = SecurityMaster.objects.get(id=transaction.security)
        #             security = transaction.security
        #             # currency = ForeignExchangeMaster.objects.get(id=transaction.currency)
        #             currency = transaction.currency
        #             # lot = tr[0]['lot']
        #             quantity_change = (decimal.Decimal(quantity) * -1)
        #
        #             if tr == 'Sell':
        #                 TransactionMaster.objects.filter(pk=pk).delete()
        #             elif tr == 'Buy':
        #                 lot = transaction.lot
        #                 TransactionMaster.objects.filter(portco=portco, plant=plant, security=security, currency=currency
        #                                                  , lot=lot).delete()
        #             DeleteTransactionRequest.objects.filter(id=id).delete()
        #             TransactionMaster.objects.filter(transaction_date__gte=transaction.transaction_date,
        #                                              operation_type='mtm',
        #                                              portco=portco, plant=plant,
        #                                              security=security, currency=currency).update(
        #                 quantity=F('quantity') + quantity_change)
        #             TransactionMaster.objects.filter(portco=portco, plant=plant, security=security, operation_type='mtm',
        #                                              quantity=0).delete()
        #     if act == '1':
        #         with connection.cursor() as cursor:
        #             var_portco = portco.id
        #             var_plant = plant.id
        #             var_security = security.id
        #             var_currency = currency.id
        #             # cursor.callproc('sp_alter_restore_transaction_v_3',
        #             #                 [lot, var_portco, var_plant, var_security, var_currency, 'transaction'])
        #             cursor.callproc('sp_alter_restore_transaction_v_6',
        #                             [-1, var_portco, var_plant, var_security, var_currency, 'mtm'])
        #         request.session['approve_all'] = True
        return redirect('delete_list')
    else:
        userdetails = get_object_or_404(UserDetails, pk=id)
        ## act = 0 means reject and act = 1 approve
        if act == '0':
            user_id = userdetails.user.id
            UserDetails.objects.filter(id=id).delete()
            User.objects.filter(id=user_id).delete()
            request.session['reject'] = True
            return redirect('signup_request_list')
        elif act == '1':
            request.session['approve'] = True
            return redirect('signup_request_list')


############################################### Client Section ###########################
@user_passes_test(lambda u: u.is_superuser)
@login_required(login_url='login')
def portcoList(request):
    if 'global_portco' in request.session:
        is_acquired = checkIsAcquired(selected_portco(request))
    else:
        is_acquired = False
    portco = PorfolioCompanyMaster.objects.filter()
    if 'is_delete' in request.GET:
        if request.GET['is_delete'] == '':
            filter = PortcoFormFilter(request.GET, queryset=portco)
        else:
            filter = PortcoFormFilter(request.GET, queryset=portco)
            portco = filter.qs
    else:
        filter = PortcoFormFilter(request.GET, queryset=portco)
        portco = filter.qs
    msg = ''
    if 'portco_add' in request.session and request.session['portco_add'] is not None:
        if request.session['portco_add']:
            msg = 'Portfolio Company successfully added'
        elif request.session['portco_add'] == False:
            msg = 'Portfolio Company already exist'
    if 'portco_update' in request.session and request.session['portco_update'] is not None:
        if request.session['portco_update']:
            msg = 'Portfolio Company successfully updated'
        elif request.session['portco_update'] == False:
            msg = 'Portfolio Company update failed. Please check the company name before updating.'
    context = {'portco_list': portco,'myFilter':filter, 'msg': msg, 'is_acquired': is_acquired}
    request.session['portco_add'] = None
    request.session['portco_update'] = None
    return render(request, "admin/portco_section/portco_list_page.html", context)

# @user_passes_test(lambda u: u.is_superuser)
# @login_required(login_url='login')
# def save_portco_form(request, context_form, template_name):
#     data = dict()
#     if request.method == 'POST':
#         if context_form.is_valid():
#             portco_new = PorfolioCompanyMaster(portco_name=request.POST['portco_name'],state=request.POST['state'],country=request.POST['country'],industry=request.POST['industry'],primary_contact=request.POST['primary_contact'],primary_contact_mail=request.POST['primary_contact_mail'],description=request.POST['description'],added_by=request.user, added_on=datetime.now(),modified_by=request.user ,modified_on=datetime.now())
#             portco_new.save()
#             # return redirect('portco_list')
#             data['form_is_valid'] = True
#             portco = PorfolioCompanyMaster.objects.all()
#             data['html_portco_list'] = render_to_string('admin/portco_section/portco_list.html', {'portco_list': portco})
#         else:
#             data['form_is_valid'] = False
#     # context = {'form': form}
#     form = FormClient()
#     # form_portco_broker = FormClientBrokerMapping()
#     context_form = {'form': form}
#     data['html_form'] = render_to_string(template_name, context_form, request=request)
#     return JsonResponse(data)
#
@user_passes_test(lambda u: u.is_superuser)
@login_required(login_url='login')
def portcoCreate(request):
    if 'global_portco' in request.session:
        is_acquired = checkIsAcquired(selected_portco(request))
    else:
        is_acquired = False
    if request.method == 'POST':
        try:
            portco = PorfolioCompanyMaster.objects.get(portco_name=request.POST['portco_name'])
            request.session['portco_add'] = False
            return redirect('portco_list')
            # return render(request, 'admin/portco_section/portco_list_page.html', {'portco_list': PorfolioCompanyMaster.objects.all(), 'error': "Client Exist"})
        except PorfolioCompanyMaster.DoesNotExist:
            form = FormPortco(request.POST)
            try:
                state = get_object_or_404(CountryStateMapping, state=request.POST['hq_state'])
            except:
                state = get_object_or_404(CountryStateMapping, pk=request.POST['hq_state'])
            country = get_object_or_404(RegionCountryMapping, pk=state.country.id)
            region = get_object_or_404(GlobalRegionMaster, pk=state.region.id)
            portco_new = PorfolioCompanyMaster(portco_name=request.POST['portco_name'], hq_state=state, hq_region=region,
                                      hq_country=country, industry=request.POST['industry'],
                                      primary_contact=request.POST['primary_contact'], primary_contact_mail=request.POST['primary_contact_mail'],
                                      description=request.POST['description'], added_by=request.user,
                                      added_on=datetime.now(), modified_by=request.user, modified_on=datetime.now())
            portco_new.save()
            request.session['portco_add'] = True
            return redirect('portco_list')
    else:
        form = FormPortco()
        # form_portco_broker = FormClientBrokerMapping()
        context_form = {'form': form, 'is_acquired': is_acquired}
    return render(request, "admin/portco_section/portco_create.html", context_form)


@user_passes_test(lambda u: u.is_superuser)
@login_required(login_url='login')
def portcoUpdate(request, pk):
    if 'global_portco' in request.session:
        is_acquired = checkIsAcquired(selected_portco(request))
    else:
        is_acquired = False
    portco = get_object_or_404(PorfolioCompanyMaster, pk=pk)
    if request.method == 'POST':
        if portco.portco_name == request.POST['portco_name']:
            form = FormPortco(request.POST)
            try:
                state = get_object_or_404(CountryStateMapping, state=request.POST['hq_state'])
            except:
                state = get_object_or_404(CountryStateMapping, pk=request.POST['hq_state'])
            country = get_object_or_404(RegionCountryMapping, pk=state.country.id)
            region = get_object_or_404(GlobalRegionMaster, pk=state.region.id)
            if request.POST['is_aquired'] == 'True':
                is_acquired = True
            else:
                is_acquired = False
            PorfolioCompanyMaster.objects.filter(pk=pk).update(portco_name=request.POST['portco_name'], hq_state=state,
                                                               hq_region=region, is_aquired=is_acquired,
                                                               hq_country=country, industry=request.POST['industry'],
                                                               primary_contact=request.POST['primary_contact'],
                                                               primary_contact_mail=request.POST[
                                                                   'primary_contact_mail'],
                                                               description=request.POST['description'],
                                                               is_delete=request.POST['is_delete'],
                                                               modified_by=request.user, modified_on=datetime.now())
            form = FormPortco(request.POST)
            request.session['portco_update'] = True
        else:
            try:
                portco = PorfolioCompanyMaster.objects.get(portco_name=request.POST['portco_name'])
                request.session['portco_update'] = False
            except PorfolioCompanyMaster.DoesNotExist:
                form = FormPortco(request.POST)
                state = get_object_or_404(CountryStateMapping, pk=request.POST['hq_state'])
                country = get_object_or_404(RegionCountryMapping, pk=state.country.id)
                region = get_object_or_404(GlobalRegionMaster, pk=state.region.id)
                if request.POST['is_aquired'] == 'True':
                    is_acquired = True
                else:
                    is_acquired = False
                PorfolioCompanyMaster.objects.filter(pk=pk).update(portco_name=request.POST['portco_name'], hq_state=state, hq_region=region,
                                          hq_country=country, industry=request.POST['industry'], is_aquired=is_acquired,
                                                                   primary_contact=request.POST['primary_contact'],
                                                                   primary_contact_mail=request.POST[
                                                                       'primary_contact_mail'],
                                                                   description=request.POST['description'],
                                                                   is_delete=request.POST['is_delete'],
                                                                   modified_by=request.user, modified_on=datetime.now())
                form = FormPortco(request.POST)
                request.session['portco_update'] = True
        form = FormPortco(request.POST, instance=portco)
        return redirect('portco_list')
    else:
        form = FormPortcoUpdate(instance=portco, country=portco.hq_country)
    context = {'form': form, 'is_acquired': is_acquired}
    return render(request, "admin/portco_section/portco_update.html", context)


@user_passes_test(lambda u: u.is_superuser)
@login_required(login_url='login')
def plantList(request):
    if 'global_portco' in request.session:
        is_acquired = checkIsAcquired(selected_portco(request))
    else:
        is_acquired = False
    portco = PorfolioCompanyMaster.objects.filter(is_delete=False)
    plant = PlantMaster.objects.filter(portco__in=portco)
    if 'is_delete' in request.GET:
        if request.GET['is_delete'] == '':
            filter = PlantFormFilter(request.GET, queryset=plant, current_user=request.user)
            plant = filter.qs
        else:
            filter = PlantFormFilter(request.GET, queryset=plant, current_user=request.user)
            plant = filter.qs
    else:
        filter = PlantFormFilter(request.GET, queryset=plant, current_user=request.user)
        plant = filter.qs
    msg = ''
    if 'plant_add' in request.session and request.session['plant_add'] is not None:
        if request.session['plant_add']:
            msg = 'Plant successfully added'
        elif request.session['plant_add'] == False:
            msg = 'Plant already exist'
    if 'plant_update' in request.session and request.session['plant_update'] is not None:
        if request.session['plant_update']:
            msg = 'Plant successfully updated'
        elif request.session['plant_update'] == False:
            msg = 'Plant update failed. Please check the Plant name before updating.'
    context = {'plant_list': plant,'myFilter':filter, 'msg':msg, 'is_acquired': is_acquired}
    request.session['plant_add'] = None
    request.session['plant_update'] = None
    return render(request, "admin/plant_section/plant_list_page.html", context)


@user_passes_test(lambda u: u.is_superuser)
@login_required(login_url='login')
def plantCreate(request):
    if 'global_portco' in request.session:
        is_acquired = checkIsAcquired(selected_portco(request))
    else:
        is_acquired = False
    if request.method == 'POST':
        try:
            portco = PorfolioCompanyMaster.objects.get(id=request.POST['portco'])
            plant = PlantMaster.objects.get(portco=portco, plant_name=request.POST['plant_name'])
            request.session['plant_add'] = False
            return redirect('plant_list')
            # return render(request, 'admin/plant_section/plant_list_page.html', {'plant_list': plant, 'error': "Fund Exist"})
        except PlantMaster.DoesNotExist:
            # form = FormFund(request.POST)
            portco = PorfolioCompanyMaster.objects.get(id=request.POST['portco'])
            country = get_object_or_404(RegionCountryMapping, pk=request.POST['country'])
            state = get_object_or_404(CountryStateMapping, state=request.POST['state'], country=country)
            region = get_object_or_404(GlobalRegionMaster, pk=state.region.id)
            plant_new = PlantMaster(portco=portco, plant_name=request.POST['plant_name'], state=state, region=region,
                                          country=country, description=request.POST['description'],
                                    primary_contact=request.POST['primary_contact'], primary_contact_mail=request.POST['primary_contact_mail'], added_by=request.user,
                                      added_on=datetime.now(), modified_by=request.user, modified_on=datetime.now())
            plant_new.save()
            request.session['plant_add'] = True
            return redirect('plant_list')
    else:
        form = FormPlant(current_user=request.user)
        context = {'form':form, 'is_acquired': is_acquired}
    return render(request, "admin/plant_section/plant_create.html", context)


@user_passes_test(lambda u: u.is_superuser)
@login_required(login_url='login')
def plantUpdate(request, pk):
    if 'global_portco' in request.session:
        is_acquired = checkIsAcquired(selected_portco(request))
    else:
        is_acquired = False
    plant = get_object_or_404(PlantMaster, pk=pk)
    if request.method == 'POST':
        try:
            state = get_object_or_404(CountryStateMapping, state=request.POST['state'])
        except:
            state = get_object_or_404(CountryStateMapping, pk=request.POST['state'])
        country = get_object_or_404(RegionCountryMapping, id=state.country.id)
        # state = get_object_or_404(CountryStateMapping, id=request.POST['state'], country=country)
        region = get_object_or_404(GlobalRegionMaster, pk=state.region.id)
        if plant.plant_name == request.POST['plant_name']:
            PlantMaster.objects.filter(pk=pk).update(plant_name=request.POST['plant_name'], state=state, region=region,
                                          country=country,
                                                     description=request.POST['description'],
                                                     primary_contact=request.POST['primary_contact'],
                                                     primary_contact_mail=request.POST['primary_contact_mail'],
                                                     modified_by=request.user, modified_on=datetime.now())
            request.session['plant_update'] = True
        else:
            try:
                portco = PorfolioCompanyMaster.objects.get(id=request.POST['portco'])
                plant = PlantMaster.objects.get(portco=portco, plant_name=request.POST['plant_name'])
                request.session['plant_update'] = False
                return redirect('plant_list')
            except PlantMaster.DoesNotExist:
                PlantMaster.objects.filter(pk=pk).update(plant_name=request.POST['plant_name'], state=state, region=region,
                                          country=country,
                                                         description=request.POST['description'],
                                                         primary_contact=request.POST['primary_contact'],
                                                         primary_contact_mail=request.POST['primary_contact_mail'],
                                                         modified_by=request.user, modified_on=datetime.now())
                request.session['plant_update'] = True
        form = FormPlant(instance=plant, current_user=request.user)
        return redirect('plant_list')
    else:
        form = FormPlantUpdate(instance=plant, country=plant.country)
    context = {'form': form, 'is_acquired': is_acquired}
    return render(request,'admin/plant_section/plant_update.html', context)


@user_passes_test(lambda u: u.is_superuser)
@login_required(login_url='login')
def loadState(request):
    country = request.GET.get('country')
    country = get_object_or_404(RegionCountryMapping, pk=country)
    state = CountryStateMapping.objects.filter(country=country)
    return render(request, 'region_country_state_selection/state_dropdown_list_options.html', {'state_list': state})


@user_passes_test(lambda u: u.is_superuser)
@login_required(login_url='login')
def elsList(request):
    if 'global_portco' in request.session:
        is_acquired = checkIsAcquired(selected_portco(request))
    else:
        is_acquired = False
    els = ELSCategoryMaster.objects.filter(is_delete=False)
    # portco = PorfolioCompanyMaster.objects.filter(is_delete=False)
    # plant = PlantMaster.objects.filter(portco__in=portco)
    # if 'is_delete' in request.GET:
    #     if request.GET['is_delete'] == '':
    #         filter = PlantFormFilter(request.GET, queryset=plant, current_user=request.user)
    #         plant = filter.qs
    #     else:
    #         filter = PlantFormFilter(request.GET, queryset=plant, current_user=request.user)
    #         plant = filter.qs
    # else:
    #     filter = PlantFormFilter(request.GET, queryset=plant, current_user=request.user)
    #     plant = filter.qs
    msg = ''
    if 'els_add' in request.session and request.session['els_add'] is not None:
        if request.session['els_add']:
            msg = 'ELS Category successfully added'
        elif request.session['els_add'] == False:
            msg = 'ELS Category already exist'
    if 'els_update' in request.session and request.session['els_update'] is not None:
        if request.session['els_update']:
            msg = 'ELS Category successfully updated'
        elif request.session['els_update'] == False:
            msg = 'ELS Category update failed. Please check the category name before updating.'
    # context = {'plant_list': plant,'myFilter':filter, 'msg':msg}
    context = {'els_list': els, 'msg':msg, 'is_acquired': is_acquired}
    request.session['els_add'] = None
    request.session['els_update'] = None
    return render(request, "admin/els_section/els_list_page.html", context)


@user_passes_test(lambda u: u.is_superuser)
@login_required(login_url='login')
def elsCreate(request):
    if 'global_portco' in request.session:
        is_acquired = checkIsAcquired(selected_portco(request))
    else:
        is_acquired = False
    if request.method == 'POST':
        try:
            els = ELSCategoryMaster.objects.get(category_name=request.POST['category_name'])
            request.session['els_add'] = False
            return redirect('els_list')
            # return render(request, 'admin/plant_section/plant_list_page.html', {'plant_list': plant, 'error': "Fund Exist"})
        except ELSCategoryMaster.DoesNotExist:
            four_p = FourPMaster.objects.get(id=request.POST['four_P_category'])
            els_new = ELSCategoryMaster(category_name=request.POST['category_name'], category_description=request.POST['category_description'], four_P_category=four_p, added_by=request.user,
                                      added_on=datetime.now(), modified_by=request.user, modified_on=datetime.now())
            els_new.save()
            score_list = request.POST.getlist('scorelist')
            score = [v for k, v in request.POST.items() if k.startswith('score_')]
            j=0
            for i in score_list:
                if score_list[j] != '':
                    x = i.split("_")[1]
                    els_rating_new = ELSCategoryRatingMaster(els_category=els_new, score=x, description=score[j], added_by=request.user,
                                              added_on=datetime.now(), modified_by=request.user, modified_on=datetime.now())
                    els_rating_new.save()
                    j=j+1

            request.session['els_add'] = True
            return redirect('els_list')
    else:
        form = FormElsCategory()
        context = {'form':form, 'is_acquired': is_acquired}
    return render(request, "admin/els_section/els_create.html", context)


@user_passes_test(lambda u: u.is_superuser)
@login_required(login_url='login')
def elsUpdate(request, pk):
    if 'global_portco' in request.session:
        is_acquired = checkIsAcquired(selected_portco(request))
    else:
        is_acquired = False
    els = get_object_or_404(ELSCategoryMaster, pk=pk)
    rating = ELSCategoryRatingMaster.objects.filter(els_category=els).values('score', 'id', 'description').order_by('score')
    if request.method == 'POST':
        four_p = get_object_or_404(FourPMaster, id=request.POST['four_P_category'])
        if els.category_name == request.POST['category_name']:
            ELSCategoryMaster.objects.filter(pk=pk).update(category_name=request.POST['category_name'], category_description=request.POST['category_description'], four_P_category=four_p,
                                                     modified_by=request.user, modified_on=datetime.now(), is_delete=request.POST['is_delete'])
            score_list = request.POST.getlist('scorelist')
            score_list_id = request.POST.getlist('scorelistid')
            score = [v for k, v in request.POST.items() if k.startswith('score_')]
            j = 0
            for i in score_list:
                if score_list[j] != '':
                    x = i.split("_")[1]
                    y = score_list_id[j].split("_")[1]
                    els_rating_new = ELSCategoryRatingMaster.objects.filter(pk=y).update(els_category=els, score=x, description=score[j], modified_by=request.user,
                                                             modified_on=datetime.now())
                    j = j + 1
            request.session['els_update'] = True
        else:
            try:
                els = ELSCategoryMaster.objects.get(category_name=request.POST['category_name'])
                request.session['els_update'] = False
                return redirect('els_list')
            except ELSCategoryMaster.DoesNotExist:
                ELSCategoryMaster.objects.filter(pk=pk).update(category_name=request.POST['category_name'],
                                                               category_description=request.POST[
                                                                   'category_description'], four_P_category=four_p,
                                                               modified_by=request.user, modified_on=datetime.now(), is_delete=request.POST['is_delete'])
                score_list = request.POST.getlist('scorelist')
                score_list_id = request.POST.getlist('scorelistid')
                score = [v for k, v in request.POST.items() if k.startswith('score_')]
                j = 0
                for i in score_list:
                    if score_list[j] != '':
                        x = i.split("_")[1]
                        y = score_list_id[j].split("_")[1]
                        els_rating_new = ELSCategoryRatingMaster.objects.filter(pk=y).update(els_category=els, score=x,
                                                                                             description=score[j],
                                                                                             modified_by=request.user,
                                                                                             modified_on=datetime.now())
                        j = j + 1
                request.session['els_update'] = True
        return redirect('els_list')
    else:
        form = FormElsCategory(instance=els)
    context = {'form': form, 'ratings':rating, 'is_acquired': is_acquired}
    return render(request,'admin/els_section/els_update.html', context)


@is_portco_plant_selected
@login_required(login_url='login')
def areaList(request):
    is_acquired = checkIsAcquired(selected_portco(request))
    portco = PorfolioCompanyMaster.objects.get(id=request.session['global_portco'])
    plant = PlantMaster.objects.get(id=request.session['global_plant'])
    area = AreaDetails.objects.filter(portco=portco, plant=plant, is_delete=False)
    # portco = PorfolioCompanyMaster.objects.filter(is_delete=False)
    # plant = PlantMaster.objects.filter(portco__in=portco)
    # if 'is_delete' in request.GET:
    #     if request.GET['is_delete'] == '':
    #         filter = PlantFormFilter(request.GET, queryset=plant, current_user=request.user)
    #         plant = filter.qs
    #     else:
    #         filter = PlantFormFilter(request.GET, queryset=plant, current_user=request.user)
    #         plant = filter.qs
    # else:
    #     filter = PlantFormFilter(request.GET, queryset=plant, current_user=request.user)
    #     plant = filter.qs
    msg = ''
    if 'area_add' in request.session and request.session['area_add'] is not None:
        if request.session['area_add']:
            msg = 'New area of oppurtunity successfully added'
        elif request.session['area_add'] == False:
            msg = 'The area of oppurtunity already exist'
    if 'area_update' in request.session and request.session['area_update'] is not None:
        if request.session['area_update']:
            msg = 'The area of oppurtunity successfully updated'
        elif request.session['area_update'] == False:
            msg = 'The area of oppurtunity update failed. Please check the area name before updating.'
    # context = {'plant_list': plant,'myFilter':filter, 'msg':msg}
    context = {'area_list': area, 'msg':msg, 'portco':portco.portco_name, 'plant':plant.plant_name, 'is_acquired': is_acquired}
    request.session['area_add'] = None
    request.session['area_update'] = None
    return render(request, "assessment_section/area_section/area_list_page.html", context)


@is_portco_plant_selected
@login_required(login_url='login')
def areaCreate(request):
    is_acquired = checkIsAcquired(selected_portco(request))
    if request.method == 'POST':
        area_list = request.POST.getlist('arealist')
        area = [v for k, v in request.POST.items() if k.startswith('area_name_')]
        description_list = request.POST.getlist('descriptionlist')
        description = [v for k, v in request.POST.items() if k.startswith('description_')]
        portco = selected_portco(request)
        plant = selected_plant(request)
        j=0
        for i in area_list:
            # x = i.split("_")[1]
            try:
                areaa = AreaDetails.objects.get(portco=portco, plant=plant, area_name=area[j])
                j=j+1
                pass
            except AreaDetails.DoesNotExist:
                area_new = AreaDetails(portco=portco, plant=plant, area_name=area[j], description=description[j], added_by=request.user,
                                          added_on=datetime.now(), modified_by=request.user, modified_on=datetime.now())
                area_new.save()
                j = j + 1
                request.session['area_add'] = True
        return redirect('area_list')
    else:
        form = FormAreaDetails()
        portco = selected_portco(request)
        plant = selected_plant(request)
        context = {'form':form, 'portco':portco.portco_name, 'plant':plant.plant_name, 'is_acquired': is_acquired}
    return render(request, "assessment_section/area_section/area_create.html", context)


@is_portco_plant_selected
@login_required(login_url='login')
def areaUpdate(request, pk):
    is_acquired = checkIsAcquired(selected_portco(request))
    area = get_object_or_404(AreaDetails, pk=pk)
    portco = selected_portco(request)
    plant = selected_plant(request)
    if request.method == 'POST':
        if request.POST['is_delete'] == 'True':
            is_delete= True
        else:
            is_delete = False
        if area.area_name == request.POST['area_name']:
            AreaDetails.objects.filter(pk=pk).update(area_name=request.POST['area_name'], description=request.POST['description'], is_delete=is_delete,
                                                     modified_by=request.user, modified_on=datetime.now())
            request.session['els_update'] = True
        else:
            try:
                areaa = AreaDetails.objects.get(portco=portco, plant=plant, area_name=request.POST['area_name'])
                request.session['area_update'] = False
                return redirect('area_list')
            except AreaDetails.DoesNotExist:
                AreaDetails.objects.filter(pk=pk).update(area_name=request.POST['area_name'], is_delete=is_delete,
                                                               description=request.POST[
                                                                   'description'],
                                                               modified_by=request.user, modified_on=datetime.now())
                request.session['area_update'] = True
        return redirect('area_list')
    else:
        form = FormAreaDetails(instance=area)
        portco = selected_portco(request)
        plant = selected_plant(request)
        context = {'form':form, 'portco':portco.portco_name, 'plant':plant.plant_name, 'is_acquired': is_acquired}
        return render(request,'assessment_section/area_section/area_update.html', context)


@is_portco_plant_selected
@login_required(login_url='login')
def elsRating(request, act='0', assessment_date='', area=''):
    is_acquired = checkIsAcquired(selected_portco(request))
    if act=='0': #New Assessment
        portco = PorfolioCompanyMaster.objects.get(id=request.session['global_portco'])
        plant = PlantMaster.objects.get(id=request.session['global_plant'])
        els = ELSCategoryMaster.objects.filter(is_delete=False).values('id', 'category_name', 'four_P_category', 'four_P_category__description','category_description','priority_rank').order_by('priority_rank')
        msg = ''
        if 'els_add' in request.session and request.session['els_add'] is not None:
            if request.session['els_add']:
                msg = 'ELS Assessment successfully added'
            elif request.session['els_add'] == False:
                msg = 'ELS Category already exist'
        if 'els_update' in request.session and request.session['els_update'] is not None:
            if request.session['els_update']:
                msg = 'ELS Assessment successfully updated'
            elif request.session['els_update'] == False:
                msg = 'ELS Assessment update failed. Please check the category name before updating.'
        # context = {'plant_list': plant,'myFilter':filter, 'msg':msg}
        els_rating = ELSCategoryRatingMaster.objects.filter()
        if els is not None:
            df_els = pd.DataFrame(list(els))
            els_rating = ELSCategoryRatingMaster.objects.filter().values('id', 'score', 'description', 'els_category_id')
            df_els_rating = pd.DataFrame(list(els_rating))
            df_els_rating = df_els_rating.pivot(index='els_category_id', columns='score', values='description') \
                .add_suffix('_value') \
                .reset_index()
            df = pd.merge(df_els, df_els_rating, how='left',
                          left_on=['id'],
                          right_on=['els_category_id'])
            df.rename({'id': 'id_x'}, axis=1, inplace=True)
            json_records = df.reset_index().to_json(orient='records')
            els = []
            els = json.loads(json_records)
        context = {'els_list': els, 'msg':msg, 'portco':portco.portco_name, 'plant':plant.plant_name, 'is_acquired': is_acquired}
        request.session['els_add'] = None
        request.session['els_update'] = None
        return render(request, "assessment_section/els_assessment/els_assessment.html", context)
    elif act=='1': #Old Assessment
        portco = PorfolioCompanyMaster.objects.get(id=request.session['global_portco'])
        plant = PlantMaster.objects.get(id=request.session['global_plant'])
        if area != 'abc' and area != '':
            areaa = AreaDetails.objects.get(id=area)
            area = AreaDetails.objects.filter(id=area, is_delete=False).values('id', 'area_name')
            els = ELSCategoryMaster.objects.filter(is_delete=False).values('id', 'category_name', 'four_P_category',
                                                                           'four_P_category__description',
                                                                           'category_description','priority_rank').order_by('priority_rank')
            els_rating = ELSCategoryRatingMaster.objects.filter()
            if els is not None:
                df_els = pd.DataFrame(list(els))
                els_rating = ELSCategoryRatingMaster.objects.filter().values('id', 'score', 'description',
                                                                             'els_category_id')
                df_els_rating = pd.DataFrame(list(els_rating))
                els_rating_submitted = AreaRatings.objects.filter(portco=portco, plant=plant, area=areaa,
                                                                 date_of_assessment_started=assessment_date, ).values(
                    'id', 'score__score', 'els_category__id', 'description', 'is_evidence')
                df_els_rating = df_els_rating.pivot(index='els_category_id', columns='score', values='description') \
                    .add_suffix('_value') \
                    .reset_index()
                if els_rating_submitted is not None:
                    els_rating_submitted = pd.DataFrame(list(els_rating_submitted))
                    df_els_rating = pd.merge(df_els_rating, els_rating_submitted, how='left',
                                             left_on=['els_category_id'],
                                             right_on=['els_category__id'])
                df = pd.merge(df_els, df_els_rating, how='left',
                              left_on=['id'],
                              right_on=['els_category_id'])
                json_records = df.reset_index().to_json(orient='records')
                els = []
                els = json.loads(json_records)
        else:
            areaa = None
            els = ELSCategoryMaster.objects.filter(is_delete=False).values('id', 'category_name', 'four_P_category',
                                                                           'four_P_category__description',
                                                                           'category_description','priority_rank').order_by('priority_rank')
            els_rating = ELSCategoryRatingMaster.objects.filter()
            if els is not None:
                df_els = pd.DataFrame(list(els))
                els_rating = ELSCategoryRatingMaster.objects.filter().values('id', 'score', 'description',
                                                                             'els_category_id')
                df_els_rating = pd.DataFrame(list(els_rating))
                els_rating_submitted = ELSRatings.objects.filter(portco=portco, plant=plant,
                                                                 date_of_assessment_started=assessment_date).values('id', 'score__score', 'els_category__id', 'description', 'is_evidence')
                df_els_rating = df_els_rating.pivot(index='els_category_id', columns='score', values='description') \
                    .add_suffix('_value') \
                    .reset_index()
                if els_rating_submitted is not None:
                    els_rating_submitted = pd.DataFrame(list(els_rating_submitted))
                    df_els_rating = pd.merge(df_els_rating, els_rating_submitted, how='left',
                                  left_on=['els_category_id'],
                                  right_on=['els_category__id'])
                df = pd.merge(df_els, df_els_rating, how='left',
                              left_on=['id'],
                              right_on=['els_category_id'])
                json_records = df.reset_index().to_json(orient='records')
                els = []
                els = json.loads(json_records)
        context = {'els_list': els, 'portco': portco.portco_name, 'plant': plant.plant_name, 'area':areaa,
                   'assessment_date':assessment_date, 'is_acquired': is_acquired}
        request.session['els_add'] = None
        request.session['els_update'] = None
        return render(request, "assessment_section/els_assessment/els_assessment.html", context)
    elif act=='2':
        portco = PorfolioCompanyMaster.objects.get(id=request.session['global_portco'])
        plant = PlantMaster.objects.get(id=request.session['global_plant'])
        area = AreaDetails.objects.get(id=area)
        els = ELSCategoryMaster.objects.filter(is_delete=False).values('id', 'category_name', 'four_P_category',
                                                                       'four_P_category__description',
                                                                       'category_description','priority_rank').order_by('priority_rank')
        msg = ''
        if 'els_add' in request.session and request.session['els_add'] is not None:
            if request.session['els_add']:
                msg = 'ELS Assessment successfully added'
            elif request.session['els_add'] == False:
                msg = 'ELS Category already exist'
        if 'els_update' in request.session and request.session['els_update'] is not None:
            if request.session['els_update']:
                msg = 'ELS Assessment successfully updated'
            elif request.session['els_update'] == False:
                msg = 'ELS Assessment update failed. Please check the category name before updating.'
        # context = {'plant_list': plant,'myFilter':filter, 'msg':msg}
        els_rating = ELSCategoryRatingMaster.objects.filter()
        if els is not None:
            df_els = pd.DataFrame(list(els))
            els_rating = ELSCategoryRatingMaster.objects.filter().values('id', 'score', 'description',
                                                                         'els_category_id')
            df_els_rating = pd.DataFrame(list(els_rating))
            df_els_rating = df_els_rating.pivot(index='els_category_id', columns='score', values='description') \
                .add_suffix('_value') \
                .reset_index()
            df = pd.merge(df_els, df_els_rating, how='left',
                          left_on=['id'],
                          right_on=['els_category_id'])
            df.rename({'id': 'id_x'}, axis=1, inplace=True)
            json_records = df.reset_index().to_json(orient='records')
            els = []
            els = json.loads(json_records)
        context = {'els_list': els, 'msg': msg, 'portco': portco.portco_name, 'plant': plant.plant_name, 'area':area, 'is_acquired': is_acquired}
        request.session['els_add'] = None
        request.session['els_update'] = None
        return render(request, "assessment_section/els_assessment/els_assessment.html", context)


@is_portco_plant_selected
@login_required(login_url='login')
def elsRatingSubmmit(request):
    is_acquired = checkIsAcquired(selected_portco(request))
    assessment_date = request.POST['assessment_date']
    # area_list = request.POST.getlist('arealist')
    area = [v for k, v in request.POST.items() if k.startswith('marks_')]
    description_list = request.POST.getlist('descriptionlist')
    description = [v for k, v in request.POST.items() if k.startswith('description_')]
    description = [i for i in description if i != 'None']
    portco = selected_portco(request)
    plant = selected_plant(request)

    j = 0
    for i in area:
        els_cat_id = i.split("_")[0]
        els_cat_id = ELSCategoryMaster.objects.get(id=els_cat_id)

        score = i.split("_")[1]
        if score == '0':
            score = ELSCategoryRatingMaster.objects.get(score=int(score), els_category=None)
        else:
            score = ELSCategoryRatingMaster.objects.get(score=int(score), els_category=els_cat_id)

        if ("image_" + str(els_cat_id.id)) in request.FILES:
            upload_file1 = request.FILES.getlist("image_" + str(els_cat_id.id))
            for upload_file in upload_file1:
                fs = FileSystemStorage()
                file_path = os.path.join(settings.BASE_DIR,
                                         'assessment_app/static/assessment_app/images/evidence/' + str(portco.portco_name) + '/' + str(plant.plant_name)
                                         + '/' + 'ELS_Assessment/' + str(assessment_date) + '/' + str(els_cat_id.id) + '/' +
                                         upload_file.name).replace("\\", "/")
                if os.path.exists(file_path):
                    os.remove(file_path)
                file = fs.save(file_path, upload_file)
                fileurl = fs.url(file)
            output_filename = os.path.join(settings.BASE_DIR,
                                         'assessment_app/static/assessment_app/images/evidence/' + str(portco.portco_name) + '/' + str(plant.plant_name)
                                         + '/' + 'ELS_Assessment/' + str(assessment_date) + '/' + str(portco.portco_name)+'_'+str(plant.plant_name)+'_' + str(assessment_date).replace("-", "")+'_'+str(els_cat_id.id)).replace("\\", "/")
            dir_name = os.path.join(settings.BASE_DIR,
                                         'assessment_app/static/assessment_app/images/evidence/' + str(portco.portco_name) + '/' + str(plant.plant_name)
                                         + '/' + 'ELS_Assessment/' + str(assessment_date) + '/' + str(els_cat_id.id)).replace("\\", "/")
            shutil.make_archive(output_filename, 'zip', dir_name)
            shutil.rmtree(dir_name)
            evidence = True
        else:
            file_path = os.path.join(settings.BASE_DIR,
                                     'assessment_app/static/assessment_app/images/evidence/' + str(
                                         portco.portco_name) + '/' + str(plant.plant_name)
                                     + '/' + 'ELS_Assessment/' + str(assessment_date) + '/' + str(els_cat_id.id) + '.zip').replace("\\", "/")
            if os.path.exists(file_path):
                evidence = True
            else:
                evidence = False


        try:
            description_post = description[j]
        except:
            description_post = None
        try:
            elss = ELSRatings.objects.get(portco=portco, plant=plant, date_of_assessment_started=assessment_date, els_category=els_cat_id)
            ELSRatings.objects.filter(pk=elss.id).update(portco=portco, plant=plant, date_of_assessment_started=assessment_date, date_of_assessment=assessment_date,
                                  score=score, els_category=els_cat_id, description=description_post, modified_by=request.user, modified_on=datetime.now(), is_evidence=evidence)
            j = j + 1
        except ELSRatings.DoesNotExist:
            els_rating = ELSRatings(portco=portco, plant=plant, date_of_assessment_started=assessment_date, date_of_assessment=assessment_date,
                                  score=score, els_category=els_cat_id, description=description_post, is_evidence=evidence,
                                   added_by=request.user,
                                   added_on=datetime.now(), modified_by=request.user, modified_on=datetime.now())
            els_rating.save()
            j = j + 1
        request.session['els_add'] = True
    # context = {'els_list': els, 'msg':msg, 'portco':portco.portco_name, 'plant':plant.plant_name}
    # request.session['els_add'] = None
    # request.session['els_update'] = None
    return redirect('home')


@is_portco_plant_selected
@login_required(login_url='login')
def isElsAssessmentPresent(request):
    is_acquired = checkIsAcquired(selected_portco(request))
    assessment_date = request.GET.get('assessment_date')
    portco = selected_portco(request)
    plant = selected_plant(request)
    if request.GET.get('area') != '0':
        area = AreaDetails.objects.get(id=request.GET.get('area'))
        elss = AreaRatings.objects.filter(portco=portco, plant=plant, area=area, date_of_assessment_started=assessment_date)
        act = 1
    else:
        elss = ELSRatings.objects.filter(portco=portco, plant=plant, date_of_assessment_started=assessment_date)
        act = 0
    if elss.count() > 0:
        request.session['assessment_date'] = assessment_date
        if act == 1:
            request.session['area_id'] = area.id
        else:
            request.session['area_id'] = 'abc'
        data = {
            'is_assessment_present': True,
            'assessment_date': assessment_date
        }
    else:
        data = {
            'is_assessment_present': False
        }
    return JsonResponse(data)


@is_portco_plant_selected
@login_required(login_url='login')
def elsAreaRedirection(request):
    return redirect('els_assessment_xyz', act=1, assessment_date=request.session['assessment_date'], area=request.session['area_id'])


@is_portco_plant_selected
@login_required(login_url='login')
def elsAreaRatingSubmmit(request):
    is_acquired = checkIsAcquired(selected_portco(request))
    assessment_date = request.POST['assessment_date']
    area_selected = request.POST['area_selected']
    area_selected = AreaDetails.objects.get(id=area_selected)
    # area_list = request.POST.getlist('arealist')
    area = [v for k, v in request.POST.items() if k.startswith('marks_')]
    description_list = request.POST.getlist('descriptionlist')
    description = [v for k, v in request.POST.items() if k.startswith('description_')]
    description = [i for i in description if i != 'None']
    portco = selected_portco(request)
    plant = selected_plant(request)
    j = 0
    for i in area:
        els_cat_id = i.split("_")[0]
        els_cat_id = ELSCategoryMaster.objects.get(id=els_cat_id)
        score = i.split("_")[1]
        if score == '0':
            score = ELSCategoryRatingMaster.objects.get(score=int(score), els_category=None)
        else:
            score = ELSCategoryRatingMaster.objects.get(score=int(score), els_category=els_cat_id)

        if ("image_" + str(els_cat_id.id)) in request.FILES:
            upload_file1 = request.FILES.getlist("image_" + str(els_cat_id.id))
            for upload_file in upload_file1:
                fs = FileSystemStorage()
                file_path = os.path.join(settings.BASE_DIR,
                                         'assessment_app/static/assessment_app/images/evidence/' + str(portco.portco_name) + '/' + str(plant.plant_name)
                                         + '/' + 'Area_Assessment/' + str(area_selected.id) + '/' + str(assessment_date) + '/' + str(els_cat_id.id) + '/' +
                                         upload_file.name).replace("\\", "/")
                if os.path.exists(file_path):
                    os.remove(file_path)
                file = fs.save(file_path, upload_file)
                fileurl = fs.url(file)
            output_filename = os.path.join(settings.BASE_DIR,
                                         'assessment_app/static/assessment_app/images/evidence/' + str(portco.portco_name) + '/' + str(plant.plant_name)
                                         + '/' + 'Area_Assessment/' + str(area_selected.id) + '/' + str(assessment_date) + '/' + str(portco.portco_name)+'_'+str(plant.plant_name)+'_' + str(assessment_date).replace("-", "")+'_'+str(area_selected.id)+'_'+str(els_cat_id.id)).replace("\\", "/")
            dir_name = os.path.join(settings.BASE_DIR,
                                         'assessment_app/static/assessment_app/images/evidence/' + str(portco.portco_name) + '/' + str(plant.plant_name)
                                         + '/' + 'Area_Assessment/' + str(area_selected.id) + '/' + str(assessment_date) + '/' + str(els_cat_id.id)).replace("\\", "/")
            shutil.make_archive(output_filename, 'zip', dir_name)
            shutil.rmtree(dir_name)
            evidence = True
        else:
            file_path = os.path.join(settings.BASE_DIR,
                                     'assessment_app/static/assessment_app/images/evidence/' + str(
                                         portco.portco_name) + '/' + str(plant.plant_name)
                                     + '/' + 'Area_Assessment/' + str(area_selected.id) + '/' + str(assessment_date) + '/' + str(els_cat_id.id) + '.zip').replace("\\", "/")
            if os.path.exists(file_path):
                evidence = True
            else:
                evidence = False

        try:
            description_post = description[j]
        except:
            description_post = None
        try:
            areaa = AreaRatings.objects.get(portco=portco, plant=plant, date_of_assessment_started=assessment_date,
                                           area=area_selected, els_category=els_cat_id)
            AreaRatings.objects.filter(pk=areaa.id).update(portco=portco, plant=plant, date_of_assessment_started=assessment_date, is_evidence=evidence,
                                  score=score, els_category=els_cat_id, description=description_post, modified_by=request.user, modified_on=datetime.now())
            j = j + 1
        except AreaRatings.DoesNotExist:
            area_rating = AreaRatings(portco=portco, plant=plant, date_of_assessment_started=assessment_date,
                                  score=score,area=area_selected, els_category=els_cat_id, description=description_post,
                                   added_by=request.user, is_evidence=evidence,
                                   added_on=datetime.now(), modified_by=request.user, modified_on=datetime.now())
            area_rating.save()
            j = j + 1
        request.session['area_assessment_add'] = True
    # context = {'els_list': els, 'msg':msg, 'portco':portco.portco_name, 'plant':plant.plant_name}
    # request.session['els_add'] = None
    # request.session['els_update'] = None
    return redirect('home')


@user_passes_test(lambda u: u.is_superuser)
@login_required(login_url='login')
def lshipList(request):
    if 'global_portco' in request.session:
        is_acquired = checkIsAcquired(selected_portco(request))
    else:
        is_acquired = False
    # lship = Leadership_Representation.objects.filter()

    lship = Leadership_Representation.objects.order_by('category_name','level_name')
    filter = LshipFormFilter(request.GET, queryset=lship)
    lship = filter.qs

    # portco = PorfolioCompanyMaster.objects.filter(is_delete=False)
    # plant = PlantMaster.objects.filter(portco__in=portco)
    # if 'is_delete' in request.GET:
    #     if request.GET['is_delete'] == '':
    #         filter = PlantFormFilter(request.GET, queryset=plant, current_user=request.user)
    #         plant = filter.qs
    #     else:
    #         filter = PlantFormFilter(request.GET, queryset=plant, current_user=request.user)
    #         plant = filter.qs
    # else:
    #     filter = PlantFormFilter(request.GET, queryset=plant, current_user=request.user)
    #     plant = filter.qs
    msg = ''
    if 'lship_add' in request.session and request.session['lship_add'] is not None:
        if request.session['lship_add']:
            msg = 'Leadership Category successfully added'
        elif request.session['lship_add'] == False:
            msg = 'Leadership Category already exist'
    if 'lship_add_level' in request.session and request.session['lship_add_level'] is not None:
        if request.session['lship_add_level']:
            msg = 'Leadership Level successfully added'
        elif request.session['lship_add_level'] == False:
            msg = 'Leadership Level already exist'
    if 'lship_update' in request.session and request.session['lship_update'] is not None:
        if request.session['lship_update']:
            msg = 'Leadership Ratings successfully updated'
        elif request.session['lship_update'] == False:
            msg = 'Leadership Ratings update failed. Please check the category name before updating.'
    # context = {'plant_list': plant,'myFilter':filter, 'msg':msg}
    context = {'lship_list': lship, 'myFilter':filter, 'msg':msg, 'is_acquired': is_acquired}
    request.session['lship_add'] = None
    request.session['lship_update'] = None
    request.session['lship_add_level'] = None
    return render(request, "admin/leadership_section/lship_list_page.html", context)

@user_passes_test(lambda u: u.is_superuser)
@login_required(login_url='login')
def lshipCreateCategory(request):
    if 'global_portco' in request.session:
        is_acquired = checkIsAcquired(selected_portco(request))
    else:
        is_acquired = False
    if request.method == 'POST':
        try:
            lship = Leadership_Category.objects.get(category_name=request.POST['category_name'])
            request.session['lship_add'] = False
            return redirect('lship_list')
            # return render(request, 'admin/plant_section/plant_list_page.html', {'plant_list': plant, 'error': "Fund Exist"})
        except Leadership_Category.DoesNotExist:
            # four_p = FourPMaster.objects.get(id=request.POST['four_P_category'])
            lship_new = Leadership_Category(category_name=request.POST['category_name'], category_description=request.POST['category_description'],added_by=request.user,
                                      added_on=datetime.now(), modified_by=request.user, modified_on=datetime.now())
            lship_new.save()

            leadership_category_tbl = Leadership_Category.objects.all()
            leadership_level_tbl = Leadership_Level.objects.all()
            for i in leadership_category_tbl:
                for j in leadership_level_tbl:
                    temp = Leadership_Representation.objects.filter(category_name=i.category_name, level_name=j.level_name)
                    if not temp:
                        p = Leadership_Representation(category_name=i.category_name,
                                                      category_description=i.category_description,
                                                      level_name=j.level_name,
                                                      level_description=j.level_description, added_by=request.user,
                                                      added_on=datetime.now(), modified_by=request.user,
                                                      modified_on=datetime.now())
                        p.save()

                        for k in range(1, 6):
                            z = LshipCategoryRatingMaster(lship_representation=p, score=k, added_by=request.user,
                                                          added_on=datetime.now(), modified_by=request.user,
                                                          modified_on=datetime.now())
                            z.save()
                    else:
                        pass


            request.session['lship_add'] = True
            return redirect('lship_list')
    else:
        form = FormLshipCategory()
        context = {'form':form, 'is_acquired': is_acquired}
    return render(request, "admin/leadership_section/lship_create_category.html", context)


@user_passes_test(lambda u: u.is_superuser)
@login_required(login_url='login')
def lshipCreateLevel(request):
    if 'global_portco' in request.session:
        is_acquired = checkIsAcquired(selected_portco(request))
    else:
        is_acquired = False
    if request.method == 'POST':
        try:
            lship = Leadership_Level.objects.get(level_name=request.POST['level_name'])
            request.session['lship_add_level'] = False
            return redirect('lship_list')
            # return render(request, 'admin/plant_section/plant_list_page.html', {'plant_list': plant, 'error': "Fund Exist"})
        except Leadership_Level.DoesNotExist:

            # four_p = FourPMaster.objects.get(id=request.POST['four_P_category'])
            lship_new = Leadership_Level(level_name=request.POST['level_name'], level_description=request.POST['level_description'],added_by=request.user,
                                      added_on=datetime.now(), modified_by=request.user, modified_on=datetime.now())
            lship_new.save()

            leadership_category_tbl = Leadership_Category.objects.all()
            leadership_level_tbl = Leadership_Level.objects.all()
            for i in leadership_category_tbl:
                for j in leadership_level_tbl:
                    temp = Leadership_Representation.objects.filter(category_name = i.category_name,level_name = j.level_name)
                    if not temp:
                        p = Leadership_Representation(category_name=i.category_name,
                                                      category_description=i.category_description, level_name=j.level_name,
                                                      level_description=j.level_description,added_by=request.user,
                                          added_on=datetime.now(), modified_by=request.user, modified_on=datetime.now())
                        p.save()



                        for k in range(1,6):

                            z = LshipCategoryRatingMaster(lship_representation = p, score = k, added_by=request.user,
                                          added_on=datetime.now(), modified_by=request.user, modified_on=datetime.now())
                            z.save()
                    else:
                        pass

            request.session['lship_add_level'] = True
            return redirect('lship_list')
    else:
        form = FormLshipLevel()
        context = {'form':form, 'is_acquired': is_acquired}
    return render(request, "admin/leadership_section/lship_create_level.html", context)


@user_passes_test(lambda u: u.is_superuser)
@login_required(login_url='login')
def lshipUpdate(request, pk):
    if 'global_portco' in request.session:
        is_acquired = checkIsAcquired(selected_portco(request))
    else:
        is_acquired = False
    lship = get_object_or_404(Leadership_Representation, pk=pk)
    rating = LshipCategoryRatingMaster.objects.filter(lship_representation=lship).values('score', 'id', 'score_representation').order_by('score')
    if request.method == 'POST':
        score_list = request.POST.getlist('scorelist')
        score_list_id = request.POST.getlist('scorelistid')
        score = [v for k, v in request.POST.items() if k.startswith('score_')]
        j = 0
        for i in score_list:
            if score_list[j] != '':
                x = i.split("_")[1]
                y = score_list_id[j].split("_")[1]
                lship_rating_new = LshipCategoryRatingMaster.objects.filter(pk=y).update(lship_representation=lship, score=x,
                                                                                     score_representation=score[j],
                                                                                     modified_by=request.user,
                                                                                     modified_on=datetime.now())
                j = j + 1
        request.session['lship_update'] = True
        return redirect('lship_list')
    else:
        form = FormLshipRepresentationCategory(instance=lship)
    context = {'form': form, 'ratings':rating, 'is_acquired': is_acquired}
    return render(request,'admin/leadership_section/lship_update.html', context)



@user_passes_test(lambda u: u.is_superuser)
@login_required(login_url='login')
def lshipListCategory(request):
    if 'global_portco' in request.session:
        is_acquired = checkIsAcquired(selected_portco(request))
    else:
        is_acquired = False
    lship = Leadership_Category.objects.filter()
    filter = LshipCategoryFormFilter(request.GET, queryset=lship)
    lship = filter.qs
    # portco = PorfolioCompanyMaster.objects.filter(is_delete=False)
    # plant = PlantMaster.objects.filter(portco__in=portco)
    # if 'is_delete' in request.GET:
    #     if request.GET['is_delete'] == '':
    #         filter = PlantFormFilter(request.GET, queryset=plant, current_user=request.user)
    #         plant = filter.qs
    #     else:
    #         filter = PlantFormFilter(request.GET, queryset=plant, current_user=request.user)
    #         plant = filter.qs
    # else:
    #     filter = PlantFormFilter(request.GET, queryset=plant, current_user=request.user)
    #     plant = filter.qs
    msg = ''
    if 'lship_update_category' in request.session and request.session['lship_update_category'] is not None:
        if request.session['lship_update_category']:
            msg = 'Leadership Category successfully updated'
        elif request.session['lship_update'] == False:
            msg = 'Leadership Category update failed. Please check the category name before updating.'
    # context = {'plant_list': plant,'myFilter':filter, 'msg':msg}
    context = {'lship_list_category': lship,'myFilter':filter, 'msg':msg, 'is_acquired': is_acquired}
    request.session['lship_update_category'] = None

    return render(request, "admin/leadership_section/lship_list_page_category.html", context)


@user_passes_test(lambda u: u.is_superuser)
@login_required(login_url='login')
def lshipUpdateCategory(request, pk):
    if 'global_portco' in request.session:
        is_acquired = checkIsAcquired(selected_portco(request))
    else:
        is_acquired = False
    lship = get_object_or_404(Leadership_Category, pk=pk)

    if request.method == 'POST':
        if request.POST['is_active'] == 'True':
            is_active = True
        else:
            is_active = False
        if lship.category_name == request.POST['category_name'] and lship.category_description == request.POST['category_description'] and lship.is_active == request.POST['is_active']:
            Leadership_Category.objects.filter(pk=pk).update(category_name=request.POST['category_name'], category_description=request.POST['category_description'], is_active=request.POST['is_active'],
                                                     modified_by=request.user, modified_on=datetime.now())

            request.session['lship_update_category'] = True
        else:
            try:
                lship = Leadership_Category.objects.get(category_name=request.POST['category_name'],category_description=request.POST['category_description'],is_active=request.POST['is_active'])
                request.session['lship_update_category'] = False
                return redirect('lship_list_category')
            except Leadership_Category.DoesNotExist:

                pre_name = lship.category_name
                new_name = request.POST['category_name']

                pre_desc = lship.category_description
                new_desc = request.POST['category_description']

                pre_status = lship.is_active
                new_status = request.POST['is_active']

                Leadership_Category.objects.filter(pk=pk).update(category_name=request.POST['category_name'],
                                                                       category_description=request.POST[
                                                                           'category_description'],
                                                                       is_active=request.POST['is_active'],
                                                                       modified_by=request.user,
                                                                       modified_on=datetime.now())

                leadership_representation_tbl = Leadership_Representation.objects.all()
                for i in leadership_representation_tbl:
                    if i.category_name == pre_name or i.category_description == pre_desc or (i.category_name == pre_name and i.category_description == pre_desc and i.is_active == pre_status):
                        Leadership_Representation.objects.filter(category_name=pre_name,category_description=pre_desc,is_active = pre_status).update(category_name=request.POST['category_name'],
                                                                         category_description=request.POST[
                                                                             'category_description'],
                                                                         is_active = request.POST['is_active'],
                                                                         modified_by=request.user,
                                                                         modified_on=datetime.now())


                request.session['lship_update_category'] = True
        return redirect('lship_list_category')
    else:
        form = FormLshipCategory(instance=lship)
    context = {'form': form, 'is_acquired': is_acquired}
    return render(request,'admin/leadership_section/lship_update_category.html', context)



@user_passes_test(lambda u: u.is_superuser)
@login_required(login_url='login')
def lshipListLevel(request):
    if 'global_portco' in request.session:
        is_acquired = checkIsAcquired(selected_portco(request))
    else:
        is_acquired = False
    lship = Leadership_Level.objects.filter()
    filter = LshipLevelFormFilter(request.GET, queryset=lship)
    lship = filter.qs
    # portco = PorfolioCompanyMaster.objects.filter(is_delete=False)
    # plant = PlantMaster.objects.filter(portco__in=portco)
    # if 'is_delete' in request.GET:
    #     if request.GET['is_delete'] == '':
    #         filter = PlantFormFilter(request.GET, queryset=plant, current_user=request.user)
    #         plant = filter.qs
    #     else:
    #         filter = PlantFormFilter(request.GET, queryset=plant, current_user=request.user)
    #         plant = filter.qs
    # else:
    #     filter = PlantFormFilter(request.GET, queryset=plant, current_user=request.user)
    #     plant = filter.qs
    msg = ''
    if 'lship_update_level' in request.session and request.session['lship_update_level'] is not None:
        if request.session['lship_update_level']:
            msg = 'Leadership Level successfully updated'
        elif request.session['lship_update_level'] == False:
            msg = 'Leadership Level update failed. Please check the level name before updating.'
    # context = {'plant_list': plant,'myFilter':filter, 'msg':msg}
    context = {'lship_list_level': lship,'myFilter':filter, 'msg':msg, 'is_acquired': is_acquired}
    request.session['lship_update_level'] = None

    return render(request, "admin/leadership_section/lship_list_page_level.html", context)


@user_passes_test(lambda u: u.is_superuser)
@login_required(login_url='login')
def lshipUpdateLevel(request, pk):
    if 'global_portco' in request.session:
        is_acquired = checkIsAcquired(selected_portco(request))
    else:
        is_acquired = False
    lship = get_object_or_404(Leadership_Level, pk=pk)

    if request.method == 'POST':
        if request.POST['is_active'] == 'True':
            is_active = True
        else:
            is_active = False
        if lship.level_name == request.POST['level_name'] and lship.level_description == request.POST['level_description'] and lship.is_active == request.POST['is_active']:
            Leadership_Level.objects.filter(pk=pk).update(level_name=request.POST['level_name'], level_description=request.POST['level_description'], is_active=request.POST['is_active'],
                                                     modified_by=request.user, modified_on=datetime.now())

            request.session['lship_update_level'] = True
        else:
            try:
                lship = Leadership_Level.objects.get(level_name=request.POST['level_name'],level_description=request.POST['level_description'],is_active=request.POST['is_active'])
                request.session['lship_update_level'] = False
                return redirect('lship_list_level')
            except Leadership_Level.DoesNotExist:

                pre_name = lship.level_name
                new_name = request.POST['level_name']

                pre_desc = lship.level_description
                new_desc = request.POST['level_description']

                pre_status = lship.is_active
                new_status = request.POST['is_active']

                Leadership_Level.objects.filter(pk=pk).update(level_name=request.POST['level_name'],
                                                                       level_description=request.POST[
                                                                           'level_description'],
                                                                       is_active=request.POST['is_active'],
                                                                       modified_by=request.user,
                                                                       modified_on=datetime.now())

                leadership_representation_tbl = Leadership_Representation.objects.all()
                for i in leadership_representation_tbl:
                    if i.level_name == pre_name or i.level_description == pre_desc or (i.level_name == pre_name and i.level_description == pre_desc and i.is_active == pre_status):
                        Leadership_Representation.objects.filter(level_name=pre_name,level_description=pre_desc,is_active=pre_status).update(level_name=request.POST['level_name'],
                                                                         level_description=request.POST[
                                                                             'level_description'],
                                                                         is_active = request.POST['is_active'],
                                                                         modified_by=request.user,
                                                                         modified_on=datetime.now())


                request.session['lship_update_level'] = True
        return redirect('lship_list_level')
    else:
        form = FormLshipLevel(instance=lship)
    context = {'form': form, 'is_acquired': is_acquired}
    return render(request,'admin/leadership_section/lship_update_level.html', context)


def tuple_to_df(worksheet):
    excel_data = list()
    # iterating over the rows and
    # getting value from each cell in row
    for row in range(len(worksheet)):
        row = worksheet[row]
        # for cell in row:
        excel_data.append(str(row[0].value))
        # excel_data.append(row_data)
    # df = pd.DataFrame(excel_data)
    # headers = df.iloc[0]
    # df = pd.DataFrame(df.values[1:], columns=headers)
    excel_data = [i for i in excel_data if i != 'None']
    if len(excel_data) > 0:
        excel_data = excel_data[0]
    else:
        excel_data = 0
    return excel_data


def ratingValidationCheck(worksheet):
    excel_data = list()
    # iterating over the rows and
    # getting value from each cell in row
    for row in range(len(worksheet)):
        row = worksheet[row]
        # for cell in row:
        excel_data.append(str(row[0].value))
        # excel_data.append(row_data)
    # df = pd.DataFrame(excel_data)
    # headers = df.iloc[0]
    # df = pd.DataFrame(df.values[1:], columns=headers)
    excel_data = [i for i in excel_data if i != 'None']
    if len(excel_data) > 0:
        excel_data = len(excel_data)
    else:
        excel_data = 0
    return excel_data


def uploadValidation(wb, date_of_assessment, request):
    first_sheet = wb.get_sheet_names()[0]
    worksheet = wb.get_sheet_by_name(first_sheet)
    portco_sheet = worksheet['D2'].value
    portco_sheet = PorfolioCompanyMaster.objects.get(portco_name=portco_sheet)
    assessment_type = worksheet['E2'].value
    if assessment_type == 'ELS Assessment':
        plant_sheet = worksheet['D3'].value
        plant_sheet = PlantMaster.objects.get(portco=portco_sheet, plant_name=plant_sheet)
        portco = selected_portco(request)
        plant = selected_plant(request)
        if portco_sheet.id != portco.id or plant_sheet.id != plant.id:
            return 0
        else:
            els = ELSCategoryMaster.objects.filter(is_delete=False)
            iterator = 6
            for i in range(els.count()):
                markings = worksheet['E' + str(iterator + 1) + ':E' + str(iterator + 6)]
                num_rate = ratingValidationCheck(markings)
                if num_rate == 0:
                    iterator = iterator + 9
                    pass
                elif num_rate > 1:
                    return 0
                elif num_rate == 1:
                    mark = tuple_to_df(markings)
                    if 0 <= int(mark) <= 5:
                        els_pass = True
                        desc = worksheet['F' + str(iterator + 1) + ':F' + str(iterator + 6)]
                        num_rate = ratingValidationCheck(desc)
                        if num_rate == 0:
                            iterator = iterator + 9
                            pass
                        elif num_rate > 1:
                            return 0
                        elif num_rate == 1:
                            return 1
                        if els_pass:
                            return 1
                    else:
                        return 0
    elif assessment_type == 'Area Assessment':
        plant_sheet = worksheet['D3'].value
        plant_sheet = PlantMaster.objects.get(portco=portco_sheet, plant_name=plant_sheet)
        portco = selected_portco(request)
        plant = selected_plant(request)
        if portco_sheet.id != portco.id or plant_sheet.id != plant.id:
            return 0
        else:
            els = ELSCategoryMaster.objects.filter(is_delete=False)
            iterator = 6
            for i in range(els.count()):
                markings = worksheet['E' + str(iterator + 1) + ':E' + str(iterator + 6)]
                num_rate = ratingValidationCheck(markings)
                if num_rate == 0:
                    iterator = iterator + 9
                    pass
                elif num_rate > 1:
                    return 0
                elif num_rate == 1:
                    mark = tuple_to_df(markings)
                    if 0 <= int(mark) <= 5:
                        els_pass = True
                        desc = worksheet['F' + str(iterator + 1) + ':F' + str(iterator + 6)]
                        num_rate = ratingValidationCheck(desc)
                        if num_rate == 0:
                            iterator = iterator + 9
                            pass
                        elif num_rate > 1:
                            return 0
                        elif num_rate == 1:
                            pass
                    else:
                        return 0
            area = AreaDetails.objects.filter(portco=portco_sheet, plant=plant_sheet, is_delete=False)
            for j in range(area.count()):
                sheet = wb.get_sheet_names()[j + 1]
                worksheet = wb.get_sheet_by_name(sheet)
                area_category_sheet = worksheet['D4'].value
                area_category_sheet = AreaDetails.objects.get(portco=portco_sheet, plant=plant_sheet,
                                                              area_name=area_category_sheet,
                                                              is_delete=False)
                iterator = 7
                for i in range(els.count()):
                    markings = worksheet['E' + str(iterator + 1) + ':E' + str(iterator + 6)]
                    num_rate = ratingValidationCheck(markings)
                    if num_rate == 0:
                        iterator = iterator + 9
                        pass
                    elif num_rate > 1:
                        return 0
                    elif num_rate == 1:
                        mark = tuple_to_df(markings)
                        if 0 <= int(mark) <= 5:
                            desc = worksheet['F' + str(iterator + 1) + ':F' + str(iterator + 6)]
                            num_rate = ratingValidationCheck(desc)
                            if num_rate == 0:
                                iterator = iterator + 9
                                pass
                            elif num_rate > 1:
                                return 0
                            elif num_rate == 1:
                                pass
                        else:
                            return 0
            return 2
    elif assessment_type == 'Leadership Assessment':
        portco = selected_portco(request)
        if portco_sheet.id != portco.id:
            return 0
        else:
            lship_repr = Leadership_Representation.objects.filter()
            iterator = 5
            for i in range(lship_repr.count()):
                markings = worksheet['E' + str(iterator + 2) + ':E' + str(iterator + 7)]
                num_rate = ratingValidationCheck(markings)
                if num_rate == 0:
                    iterator = iterator + 11
                    pass
                elif num_rate > 1:
                    return 0
                elif num_rate == 1:
                    mark = tuple_to_df(markings)
                    if 0 <= int(mark) <= 5:
                        els_pass = True
                        desc = worksheet['F' + str(iterator + 2) + ':F' + str(iterator + 7)]
                        num_rate = ratingValidationCheck(desc)
                        if num_rate == 0:
                            iterator = iterator + 11
                            pass
                        elif num_rate > 1:
                            return 0
                        elif num_rate == 1:
                            pass
                    else:
                        return 0
            return 3
    else:
        return 0


@is_portco_plant_selected
@login_required(login_url='login')
def bulkUpload(request):
    is_acquired = checkIsAcquired(selected_portco(request))
    if request.method == 'POST':
        portco = PorfolioCompanyMaster.objects.get(id=request.session['global_portco'])
        plant = PlantMaster.objects.get(id=request.session['global_plant'])
        if 'upload_template' in request.POST:
            excel_file = request.FILES["excel_file"]
            date_of_assessment = request.POST['assessment_date']
            wb = openpyxl.load_workbook(excel_file, data_only=True, keep_vba=True)
            is_validate = uploadValidation(wb, date_of_assessment, request)
            if is_validate == 0:
                request.session['bulk_upload_successful'] = False
                return redirect('bulk_upload')
            elif is_validate == 1:
                wb = openpyxl.load_workbook(excel_file, data_only=True, keep_vba=True)
                first_sheet = wb.get_sheet_names()[0]
                worksheet = wb.get_sheet_by_name(first_sheet)
                portco_sheet = worksheet['D2'].value
                plant_sheet = worksheet['D3'].value
                portco_sheet = PorfolioCompanyMaster.objects.get(portco_name=portco_sheet)
                plant_sheet = PlantMaster.objects.get(portco=portco_sheet, plant_name=plant_sheet)
                els = ELSCategoryMaster.objects.filter(is_delete=False)
                iterator = 6
                for i in range(els.count()):
                    els_category = worksheet['D' + str(iterator)].value
                    markings = worksheet['E' + str(iterator + 1) + ':E' + str(iterator + 6)]
                    marks = tuple_to_df(markings)
                    desc = worksheet['F' + str(iterator + 1) + ':F' + str(iterator + 6)]
                    desc = tuple_to_df(desc)
                    if desc == 0:
                        desc=None
                    else:
                        desc = desc
                    if marks == 0:
                        pass
                    else:
                        els_cat = ELSCategoryMaster.objects.get(category_name=els_category, is_delete=False)

                        if marks == '0':
                            score = ELSCategoryRatingMaster.objects.get(score=int(marks), els_category=None)
                        else:
                            score = ELSCategoryRatingMaster.objects.get(score=int(marks), els_category=els_cat)


                        try:
                            elss = ELSRatings.objects.get(portco=portco_sheet, plant=plant_sheet,
                                                          date_of_assessment_started=date_of_assessment,
                                                          els_category=els_cat)
                            ELSRatings.objects.filter(pk=elss.id).update(portco=portco_sheet, plant=plant_sheet,
                                                                         date_of_assessment_started=date_of_assessment,
                                                                         date_of_assessment=date_of_assessment,
                                                                         score=score, els_category=els_cat,
                                                                         description=desc,
                                                                         modified_by=request.user,
                                                                         modified_on=datetime.now())
                        except ELSRatings.DoesNotExist:
                            els_rating = ELSRatings(portco=portco_sheet, plant=plant_sheet,
                                                    date_of_assessment_started=date_of_assessment,
                                                    date_of_assessment=date_of_assessment,
                                                    score=score, els_category=els_cat, description=desc,
                                                    added_by=request.user,
                                                    added_on=datetime.now(), modified_by=request.user,
                                                    modified_on=datetime.now())
                            els_rating.save()
                    iterator = iterator + 9
                request.session['bulk_upload_successful'] = True
                return redirect('bulk_upload')
            elif is_validate == 2:
                wb = openpyxl.load_workbook(excel_file, data_only=True, keep_vba=True)
                first_sheet = wb.get_sheet_names()[0]
                worksheet = wb.get_sheet_by_name(first_sheet)
                portco_sheet = worksheet['D2'].value
                plant_sheet = worksheet['D3'].value
                portco_sheet = PorfolioCompanyMaster.objects.get(portco_name=portco_sheet)
                plant_sheet = PlantMaster.objects.get(portco=portco_sheet, plant_name=plant_sheet)
                els = ELSCategoryMaster.objects.filter(is_delete=False)
                iterator = 6
                for i in range(els.count()):
                    els_category = worksheet['D' + str(iterator)].value
                    markings = worksheet['E' + str(iterator + 1) + ':E' + str(iterator + 6)]
                    marks = tuple_to_df(markings)
                    desc = worksheet['F' + str(iterator + 1) + ':F' + str(iterator + 6)]
                    desc = tuple_to_df(desc)
                    if desc == 0:
                        desc = None
                    else:
                        desc = desc
                    if marks == 0:
                        pass
                    else:
                        els_cat = ELSCategoryMaster.objects.get(category_name=els_category, is_delete=False)

                        if marks == '0':
                            score = ELSCategoryRatingMaster.objects.get(score=int(marks), els_category=None)
                        else:
                            score = ELSCategoryRatingMaster.objects.get(score=int(marks), els_category=els_cat)

#                        score = ELSCategoryRatingMaster.objects.get(els_category=els_cat, score=marks)
                        try:
                            elss = ELSRatings.objects.get(portco=portco_sheet, plant=plant_sheet,
                                                          date_of_assessment_started=date_of_assessment,
                                                          els_category=els_cat)
                            ELSRatings.objects.filter(pk=elss.id).update(portco=portco_sheet, plant=plant_sheet,
                                                                         date_of_assessment_started=date_of_assessment,
                                                                         date_of_assessment=date_of_assessment,
                                                                         score=score, els_category=els_cat,
                                                                         description=desc,
                                                                         modified_by=request.user,
                                                                         modified_on=datetime.now())
                        except ELSRatings.DoesNotExist:
                            els_rating = ELSRatings(portco=portco_sheet, plant=plant_sheet,
                                                    date_of_assessment_started=date_of_assessment,
                                                    date_of_assessment=date_of_assessment,
                                                    score=score, els_category=els_cat, description=desc,
                                                    added_by=request.user,
                                                    added_on=datetime.now(), modified_by=request.user,
                                                    modified_on=datetime.now())
                            els_rating.save()
                    iterator = iterator + 9
                area = AreaDetails.objects.filter(portco=portco_sheet, plant=plant_sheet, is_delete=False)
                for j in range(area.count()):
                    sheet = wb.get_sheet_names()[j + 1]
                    worksheet = wb.get_sheet_by_name(sheet)
                    area_category_sheet = worksheet['D4'].value
                    area_category_sheet = AreaDetails.objects.get(portco=portco_sheet, plant=plant_sheet,
                                                                  area_name=area_category_sheet,
                                                                  is_delete=False)
                    iterator = 7
                    for i in range(els.count()):
                        els_category = worksheet['D' + str(iterator)].value
                        markings = worksheet['E' + str(iterator + 1) + ':E' + str(iterator + 6)]
                        marks = tuple_to_df(markings)
                        desc = worksheet['F' + str(iterator + 1) + ':F' + str(iterator + 6)]
                        desc = tuple_to_df(desc)
                        if desc == 0:
                            desc = None
                        else:
                            desc = desc
                        if marks == 0:
                            pass
                        else:
                            els_cat = ELSCategoryMaster.objects.get(category_name=els_category, is_delete=False)
                            if marks == '0':
                                score = ELSCategoryRatingMaster.objects.get(score=int(marks), els_category=None)
                            else:
                                score = ELSCategoryRatingMaster.objects.get(score=int(marks), els_category=els_cat)

#                            score = ELSCategoryRatingMaster.objects.get(els_category=els_cat, score=marks)
                            try:
                                areaa = AreaRatings.objects.get(portco=portco_sheet, plant=plant_sheet,
                                                                date_of_assessment_started=date_of_assessment,
                                                                area=area_category_sheet, els_category=els_cat)
                                AreaRatings.objects.filter(pk=areaa.id).update(portco=portco_sheet, plant=plant_sheet,
                                                                               date_of_assessment_started=date_of_assessment,
                                                                               score=score, els_category=els_cat,
                                                                               description=desc,
                                                                               modified_by=request.user,
                                                                               modified_on=datetime.now())
                                j = j + 1
                            except AreaRatings.DoesNotExist:
                                area_rating = AreaRatings(portco=portco_sheet, plant=plant_sheet,
                                                          date_of_assessment_started=date_of_assessment,
                                                          score=score, area=area_category_sheet, els_category=els_cat,
                                                          description=desc,
                                                          added_by=request.user,
                                                          added_on=datetime.now(), modified_by=request.user,
                                                          modified_on=datetime.now())
                                area_rating.save()
                        iterator = iterator + 9
                request.session['bulk_upload_successful'] = True
                return redirect('bulk_upload')
            elif is_validate == 3:
                wb = openpyxl.load_workbook(excel_file, data_only=True, keep_vba=True)
                first_sheet = wb.get_sheet_names()[0]
                worksheet = wb.get_sheet_by_name(first_sheet)
                portco_sheet = worksheet['D2'].value
                portco_sheet = PorfolioCompanyMaster.objects.get(portco_name=portco_sheet)
                lship_repr = Leadership_Representation.objects.filter()
                iterator = 5
                for i in range(lship_repr.count()):
                    level = worksheet['D' + str(iterator)].value
                    category = worksheet['D' + str(iterator + 1)].value
                    markings = worksheet['E' + str(iterator + 2) + ':E' + str(iterator + 7)]
                    marks = tuple_to_df(markings)
                    desc = worksheet['F' + str(iterator + 2) + ':F' + str(iterator + 7)]
                    desc = tuple_to_df(desc)
                    if desc == 0:
                        desc = None
                    if marks == 0:
                        pass
                    else:
                        a = 1
                        lship_cat_id = Leadership_Representation.objects.get(level_name=level, category_name=category)

                        if marks == '0':
                            score = LshipCategoryRatingMaster.objects.get(score=int(marks), lship_representation=None)
                        else:
                            score = LshipCategoryRatingMaster.objects.get(score=int(marks),
                                                                          lship_representation=lship_cat_id)
                        #score = LshipCategoryRatingMaster.objects.get(score=int(marks),
                                                                    #lship_representation=lship_cat_id)
                        try:
                            lshipp = LeadershipRatings.objects.get(portco=portco,
                                                                   date_of_assessment_started=date_of_assessment,
                                                                   lship_id=lship_cat_id)
                            LeadershipRatings.objects.filter(pk=lshipp.id).update(portco=portco,
                                                                                  date_of_assessment_started=date_of_assessment,
                                                                                  date_of_assessment=date_of_assessment,
                                                                                  score=score, lship_id=lship_cat_id,
                                                                                  description=desc,
                                                                                  modified_by=request.user,
                                                                                  modified_on=datetime.now())
                            # j = j + 1
                        except LeadershipRatings.DoesNotExist:
                            lship_rating = LeadershipRatings(portco=portco, date_of_assessment_started=date_of_assessment,
                                                             date_of_assessment=date_of_assessment,
                                                             score=score, lship_id=lship_cat_id,
                                                             description=desc,
                                                             added_by=request.user,
                                                             added_on=datetime.now(), modified_by=request.user,
                                                             modified_on=datetime.now())
                            lship_rating.save()
                    if iterator>=15:
                        iterator = iterator + 11
                    else:
                        iterator = iterator + 10
                request.session['bulk_upload_successful'] = True
                return redirect('bulk_upload')
            # if excel_file._name.startswith('els'):
            #     wb = openpyxl.load_workbook(excel_file, data_only=True, keep_vba=True)
            #     first_sheet = wb.get_sheet_names()[0]
            #     worksheet = wb.get_sheet_by_name(first_sheet)
            #     portco_sheet = worksheet['D2'].value
            #     plant_sheet = worksheet['D3'].value
            #     portco_sheet = PorfolioCompanyMaster.objects.get(portco_name=portco_sheet)
            #     plant_sheet = PlantMaster.objects.get(plant_name=plant_sheet)
            #     els = ELSCategoryMaster.objects.filter(is_delete=False)
            #     iterator = 6
            #     for i in range(els.count()):
            #         els_category = worksheet['D' + str(iterator)].value
            #         markings = worksheet['E' + str(iterator+1) + ':E' +str(iterator+5)]
            #         marks = tuple_to_df(markings)
            #         if marks==0:
            #             pass
            #         else:
            #             els_cat = ELSCategoryMaster.objects.get(category_name=els_category, is_delete=False)
            #             score = ELSCategoryRatingMaster.objects.get(els_category=els_cat, score=marks)
            #             try:
            #                 elss = ELSRatings.objects.get(portco=portco_sheet, plant=plant_sheet,
            #                                               date_of_assessment_started=date_of_assessment,
            #                                               els_category=els_cat)
            #                 ELSRatings.objects.filter(pk=elss.id).update(portco=portco_sheet, plant=plant_sheet,
            #                                                              date_of_assessment_started=date_of_assessment,
            #                                                              date_of_assessment=date_of_assessment,
            #                                                              score=score, els_category=els_cat,
            #                                                              description=None,
            #                                                              modified_by=request.user,
            #                                                              modified_on=datetime.now())
            #             except ELSRatings.DoesNotExist:
            #                 els_rating = ELSRatings(portco=portco_sheet, plant=plant_sheet, date_of_assessment_started=date_of_assessment,
            #                                         date_of_assessment=date_of_assessment,
            #                                         score=score, els_category=els_cat, description=None,
            #                                         added_by=request.user,
            #                                         added_on=datetime.now(), modified_by=request.user,
            #                                         modified_on=datetime.now())
            #                 els_rating.save()
            #         iterator = iterator + 8
            # elif excel_file._name.startswith('area'):
            #     wb = openpyxl.load_workbook(excel_file, data_only=True, keep_vba=True)
            #     first_sheet = wb.get_sheet_names()[0]
            #     worksheet = wb.get_sheet_by_name(first_sheet)
            #     portco_sheet = worksheet['D2'].value
            #     plant_sheet = worksheet['D3'].value
            #     portco_sheet = PorfolioCompanyMaster.objects.get(portco_name=portco_sheet)
            #     plant_sheet = PlantMaster.objects.get(plant_name=plant_sheet)
            #     els = ELSCategoryMaster.objects.filter(is_delete=False)
            #     iterator = 6
            #     for i in range(els.count()):
            #         els_category = worksheet['D' + str(iterator)].value
            #         markings = worksheet['E' + str(iterator + 1) + ':E' + str(iterator + 5)]
            #         marks = tuple_to_df(markings)
            #         if marks == 0:
            #             pass
            #         else:
            #             els_cat = ELSCategoryMaster.objects.get(category_name=els_category, is_delete=False)
            #             score = ELSCategoryRatingMaster.objects.get(els_category=els_cat, score=marks)
            #             try:
            #                 elss = ELSRatings.objects.get(portco=portco_sheet, plant=plant_sheet,
            #                                               date_of_assessment_started=date_of_assessment,
            #                                               els_category=els_cat)
            #                 ELSRatings.objects.filter(pk=elss.id).update(portco=portco_sheet, plant=plant_sheet,
            #                                                              date_of_assessment_started=date_of_assessment,
            #                                                              date_of_assessment=date_of_assessment,
            #                                                              score=score, els_category=els_cat,
            #                                                              description=None,
            #                                                              modified_by=request.user,
            #                                                              modified_on=datetime.now())
            #             except ELSRatings.DoesNotExist:
            #                 els_rating = ELSRatings(portco=portco_sheet, plant=plant_sheet,
            #                                         date_of_assessment_started=date_of_assessment,
            #                                         date_of_assessment=date_of_assessment,
            #                                         score=score, els_category=els_cat, description=None,
            #                                         added_by=request.user,
            #                                         added_on=datetime.now(), modified_by=request.user,
            #                                         modified_on=datetime.now())
            #                 els_rating.save()
            #         iterator = iterator + 8
            #     area = AreaDetails.objects.filter(portco=portco_sheet, plant=plant_sheet, is_delete=False)
            #     for j in range(area.count()):
            #         sheet = wb.get_sheet_names()[j+1]
            #         worksheet = wb.get_sheet_by_name(sheet)
            #         area_category_sheet = worksheet['D4'].value
            #         area_category_sheet = AreaDetails.objects.get(portco=portco_sheet, plant=plant_sheet, area_name=area_category_sheet,
            #                                                       is_delete=False)
            #         iterator = 7
            #         for i in range(els.count()):
            #             els_category = worksheet['D' + str(iterator)].value
            #             markings = worksheet['E' + str(iterator + 1) + ':E' + str(iterator + 5)]
            #             marks = tuple_to_df(markings)
            #             if marks == 0:
            #                 pass
            #             else:
            #                 els_cat = ELSCategoryMaster.objects.get(category_name=els_category, is_delete=False)
            #                 score = ELSCategoryRatingMaster.objects.get(els_category=els_cat, score=marks)
            #                 try:
            #                     areaa = AreaRatings.objects.get(portco=portco_sheet, plant=plant_sheet,
            #                                                     date_of_assessment_started=date_of_assessment,
            #                                                     area=area_category_sheet, els_category=els_cat)
            #                     AreaRatings.objects.filter(pk=areaa.id).update(portco=portco_sheet, plant=plant_sheet,
            #                                                                    date_of_assessment_started=date_of_assessment,
            #                                                                    score=score, els_category=els_cat,
            #                                                                    description=None,
            #                                                                    modified_by=request.user,
            #                                                                    modified_on=datetime.now())
            #                     j = j + 1
            #                 except AreaRatings.DoesNotExist:
            #                     area_rating = AreaRatings(portco=portco_sheet, plant=plant_sheet,
            #                                                                    date_of_assessment_started=date_of_assessment,
            #                                               score=score, area=area_category_sheet, els_category=els_cat,
            #                                               description=None,
            #                                               added_by=request.user,
            #                                               added_on=datetime.now(), modified_by=request.user,
            #                                               modified_on=datetime.now())
            #                     area_rating.save()
            #             iterator = iterator + 8
            # elif excel_file._name.startswith('leadership'):
            #     wb = openpyxl.load_workbook(excel_file, data_only=True, keep_vba=True)
            #     first_sheet = wb.get_sheet_names()[0]
            #     worksheet = wb.get_sheet_by_name(first_sheet)
            #     portco_sheet = worksheet['D2'].value
            #     portco_sheet = PorfolioCompanyMaster.objects.get(portco_name=portco_sheet)
            #     lship_repr = Leadership_Representation.objects.filter()
            #     iterator = 5
            #     for i in range(lship_repr.count()):
            #         level = worksheet['D' + str(iterator)].value
            #         category = worksheet['D' + str(iterator+1)].value
            #         markings = worksheet['E' + str(iterator + 2) + ':E' + str(iterator + 11)]
            #         marks = tuple_to_df(markings)
            #         markings = worksheet['F' + str(iterator + 2) + ':F' + str(iterator + 11)]
            #         desc = tuple_to_df(markings)
            #         if desc == 0:
            #             desc = None
            #         if marks == 0:
            #             pass
            #         else:
            #             a = 1
            #             # els_cat = ELSCategoryMaster.objects.get(category_name=els_category, is_delete=False)
            #             # score = ELSCategoryRatingMaster.objects.get(els_category=els_cat, score=marks)
            #             # try:
            #             #     lshipp = LeadershipRatings.objects.get(portco=portco,
            #             #                                            date_of_assessment_started=date_of_assessment,
            #             #                                            lship_id=lship_cat_id)
            #             #     LeadershipRatings.objects.filter(pk=lshipp.id).update(portco=portco,
            #             #                                                           date_of_assessment_started=date_of_assessment,
            #             #                                                           date_of_assessment=date_of_assessment,
            #             #                                                           score=score, lship_id=lship_cat_id,
            #             #                                                           description=description_post,
            #             #                                                           modified_by=request.user,
            #             #                                                           modified_on=datetime.now())
            #             #     j = j + 1
            #             # except LeadershipRatings.DoesNotExist:
            #             #     lship_rating = LeadershipRatings(portco=portco, date_of_assessment_started=assessment_date,
            #             #                                      date_of_assessment=assessment_date,
            #             #                                      score=score, lship_id=lship_cat_id,
            #             #                                      description=description_post,
            #             #                                      added_by=request.user,
            #             #                                      added_on=datetime.now(), modified_by=request.user,
            #             #                                      modified_on=datetime.now())
            #             #     lship_rating.save()
            #         iterator = iterator + 8
    else:
        if 'global_portco' in request.session and 'global_plant' in request.session:
            user_details = UserDetails.objects.get(user=request.user.id)
            if user_details.is_orc_employee == True:
                portco = PorfolioCompanyMaster.objects.filter(is_delete=False)
                plant = PlantMaster.objects.filter(is_delete=False)
            else:
                portco = PorfolioCompanyMaster.objects.filter(id=user_details.portco.id, is_delete=False)
                p = PorfolioCompanyMaster.objects.get(id=user_details.portco.id)
                plant = PlantMaster.objects.filter(portco=p, is_delete=False)
        portco_selected = selected_portco(request)
        plant_selected = selected_plant(request)
    portco_selected = selected_portco(request)
    if portco_selected.is_aquired == True:
        aquired = True
    else:
        aquired = False
    msg = ''
    if 'bulk_upload_successful' in request.session and request.session['bulk_upload_successful'] is not None:
        if request.session['bulk_upload_successful']:
            msg = 'Uploading template successfully!!'
        elif request.session['bulk_upload_successful'] == False:
            msg = 'Template uploading fail. Please check the file before uploading'
            # There is error in the excel template formatting. Below could be the reasons\n\t1. Multiple rating in one category\n\t2. Portfolio company and Plant selected does match with template '
    request.session['bulk_upload_successful'] = None
    return render(request, "assessment_section/Bulk_Upload/bulk_upload.html", {'portco': portco, 'plant': plant, 'msg': msg, 'aquired':aquired,
                   'portco_selected': request.session['global_portco'],
                   'plant_selected': request.session['global_plant'],
                   'selection': True, 'is_acquired': is_acquired})


def cell_format(new_cell, cell):
    if cell.has_style:
        new_cell.font = copy(cell.font)
        new_cell.border = copy(cell.border)
        new_cell.fill = copy(cell.fill)
        new_cell.number_format = copy(cell.number_format)
        new_cell.protection = copy(cell.protection)
        new_cell.alignment = Alignment(horizontal = 'left', vertical = 'top',wrapText=True)
        #new_cell.alignment = copy(cell.alignment)


@is_portco_plant_selected
@login_required(login_url='login')
def bulkUploadELS(request, act=''):
    is_acquired = checkIsAcquired(selected_portco(request))
    portco = PorfolioCompanyMaster.objects.get(id=request.session['global_portco'])
    plant = PlantMaster.objects.get(id=request.session['global_plant'])
    if act=='0':
        file_path = os.path.join(settings.BASE_DIR, 'assessment_app/static/assessment_app/docs/els_assessment.xlsm').replace("\\","/")
        if os.path.exists(file_path):
            fh = openpyxl.load_workbook(file_path, read_only=False, keep_vba=True)
            response = HttpResponse(content_type="application/vnd.ms-excel.sheet.binary.macroenabled.12; version='2007 onwards'")
            response['Content-Disposition'] = 'inline; filename=' + str(portco.portco_name) + '_' + str(plant.plant_name) + '_' + os.path.basename(file_path)
            wb = fh
            first_sheet = wb.get_sheet_names()[0]
            ws = wb.get_sheet_by_name(first_sheet)
            ws['D2'] = str(portco.portco_name)
            ws['D3'] = str(plant.plant_name)
            els = ELSCategoryRatingMaster.objects.filter(els_category__is_delete=False).order_by('els_category_priority', 'score')
            iterator = 6
            j = 0
            for i in els:
                if i.els_category.is_delete == False:
                    if j == 0:
                        ws['D' + str(iterator)] = i.els_category.category_name
                        ws['C' + str(iterator)] = i.els_category_priority.id
                        iterator = iterator + 1
                        ws['D' + str(iterator+1)] = i.description

                        iterator = iterator + 1
                        j = j + 1
                    elif j == 4:
                        ws['D' + str(iterator+1)] = i.description
                        iterator = iterator + 4
                        temp = iterator
                        if els[len(els)-1].els_category.category_name != i.els_category.category_name:
                            for i in range(6, 14):
                                for j in range(3, 8):
                                    ws.cell(row=temp, column=j).value = ws.cell(row=i, column=j).value
                                    cell_format(ws.cell(row=temp, column=j), ws.cell(row=i, column=j))
                                temp = temp + 1
                            j = 0
                    elif j > 0 and j < 4:
                        ws['D' + str(iterator+1)] = i.description
                        iterator = iterator + 1
                        j = j + 1
            for z in range(len(wb.get_sheet_names())):
                sheet = wb.get_sheet_names()[z]
                ws = wb.get_sheet_by_name(sheet)
                ws.protection.sheet = True
                ws.protection.set_password('bulkupload@dig')
                ws.protection.enable()
                ws.protection.selectLockedCells = True
                ws.protection.selectUnlockedCells = True
            for z in range(len(wb.get_sheet_names())):
                sheet = wb.get_sheet_names()[z]
                ws = wb.get_sheet_by_name(sheet)
                ws_max_row = ws.max_row
                ws.protection.selectUnlockedCells = False
                cell = ws['E7:G' + str(ws_max_row)]
                for row in range(len(cell)):
                    row = cell[row]
                    for cell1 in row:
                        cell1.protection = Protection(locked=False, hidden=False)
            wb.save(response)
            return response
        raise Http404
    elif act=='1':
        file_path = os.path.join(settings.BASE_DIR, 'assessment_app/static/assessment_app/docs/area_els_assessment.xlsm').replace("\\","/")
        if os.path.exists(file_path):
            fh = openpyxl.load_workbook(file_path, read_only=False, keep_vba=True)
            response = HttpResponse(content_type="application/vnd.ms-excel.sheet.binary.macroenabled.12; version='2007 onwards'")
            response['Content-Disposition'] = 'inline; filename=' + str(portco.portco_name) + '_' + str(plant.plant_name) + '_' + os.path.basename(file_path)
            wb = fh
            first_sheet = wb.get_sheet_names()[0]
            ws = wb.get_sheet_by_name(first_sheet)
            ws['D2'] = str(portco.portco_name)
            ws['D3'] = str(plant.plant_name)
            els = ELSCategoryRatingMaster.objects.filter(els_category__is_delete=False).order_by('els_category_priority', 'score')
            iterator = 6
            k = 0
            for d in els:
                if d.els_category.is_delete == False:
                    if k == 0:
                        ws['D' + str(iterator)] = d.els_category.category_name
                        ws['C' + str(iterator)] = d.els_category_priority.id
                        iterator = iterator + 1
                        ws['D' + str(iterator + 1)] = d.description
                        iterator = iterator + 1
                        k = k + 1
                    elif k == 4:
                        ws['D' + str(iterator+1)] = d.description
                        iterator = iterator + 4
                        temp = iterator
                        if els[len(els) - 1].els_category.category_name != d.els_category.category_name:
                            for m in range(6, 14):
                                for l in range(3, 8):
                                    ws.cell(row=temp, column=l).value = ws.cell(row=m, column=l).value
                                    cell_format(ws.cell(row=temp, column=l), ws.cell(row=m, column=l))
                                temp = temp + 1
                        k = 0
                    elif k > 0 and k < 4:
                        ws['D' + str(iterator+1)] = d.description
                        iterator = iterator + 1
                        k = k + 1
            area = AreaDetails.objects.filter(portco=portco, plant=plant, is_delete=False).values('id', 'area_name').order_by('id')
            if area.count() > 0:
                j = 0
                area_name = list(AreaDetails.objects.filter(portco=portco, plant=plant, is_delete=False).values_list('area_name',
                                                                                                    flat=True).order_by('id'))
                for i in area_name:
                    sheet = wb.get_sheet_names()[j+1]
                    ws = wb.get_sheet_by_name(sheet)
                    ws.title = i + ' ' + 'Assessment'
                    ws['D2'] = str(portco.portco_name)
                    ws['D3'] = str(plant.plant_name)
                    ws['D4'] = str(i)
                    els = ELSCategoryRatingMaster.objects.filter(els_category__is_delete=False).order_by('els_category', 'score')
                    iterator = 7
                    k = 0
                    for d in els:
                        if d.els_category.is_delete == False:
                            if k == 0:
                                ws['D' + str(iterator)] = d.els_category.category_name
                                ws['C' + str(iterator)] = d.els_category.id
                                ws['D' + str(iterator + 2)] = d.description
                                iterator = iterator + 3
                                k = k + 1
                            elif k == 4:
                                ws['D' + str(iterator)] = d.description
                                iterator = iterator + 3
                                temp = iterator
                                if els[len(els) - 1].els_category.category_name != d.els_category.category_name:
                                    for m in range(7, 14):
                                        for l in range(3, 8):
                                            ws.cell(row=temp, column=l).value = ws.cell(row=m, column=l).value
                                            cell_format(ws.cell(row=temp, column=l), ws.cell(row=m, column=l))
                                        temp = temp + 1
                                k = 0
                            elif k > 0 and k < 4:
                                ws['D' + str(iterator)] = d.description
                                iterator = iterator + 1
                                k = k + 1
                    if j < area.count()-1:
                        target = wb[i + ' ' + 'Assessment']
                        wb.copy_worksheet(target)
                    j = j + 1
            for z in range(len(wb.get_sheet_names())):
                sheet = wb.get_sheet_names()[z]
                ws = wb.get_sheet_by_name(sheet)
                ws.protection.sheet = True
                ws.protection.set_password('bulkupload@dig')
                ws.protection.enable()
                ws.protection.selectLockedCells = True
                ws.protection.selectUnlockedCells = True
            for z in range(len(wb.get_sheet_names())):
                sheet = wb.get_sheet_names()[z]
                ws = wb.get_sheet_by_name(sheet)
                ws_max_row = ws.max_row
                ws.protection.selectUnlockedCells = False
                cell = ws['E7:G' + str(ws_max_row)]
                for row in range(len(cell)):
                    row = cell[row]
                    for cell1 in row:
                        cell1.protection = Protection(locked=False, hidden=False)
            wb.save(response)
            return response
        raise Http404
    elif act == '2':
        file_path = os.path.join(settings.BASE_DIR,
                                 'assessment_app/static/assessment_app/docs/leadership_assessment.xlsm').replace("\\", "/")
        if os.path.exists(file_path):
            fh = openpyxl.load_workbook(file_path, read_only=False, keep_vba=True)
            response = HttpResponse(
                content_type="application/vnd.ms-excel.sheet.binary.macroenabled.12; version='2007 onwards'")
            response['Content-Disposition'] = 'inline; filename=' + str(portco.portco_name) + '_' + os.path.basename(file_path)
            wb = fh
            first_sheet = wb.get_sheet_names()[0]
            ws = wb.get_sheet_by_name(first_sheet)
            ws['D2'] = str(portco.portco_name)
            lship_cat = Leadership_Representation.objects.filter(is_active=True).order_by('level_name', 'category_name')
            lship = LshipCategoryRatingMaster.objects.all().order_by('lship_representation', 'score')
            iterator = 5
            k = 0
            a = 1
            for d in lship_cat:
                if k == 0:
                    ws['D' + str(iterator)] = d.level_name
                    ws['D' + str(iterator+1)] = d.category_name
                    ws['C' + str(iterator+1)] = a
                    iterator = iterator + 3
                    lship = LshipCategoryRatingMaster.objects.filter(lship_representation=d).order_by('score')
                    for l in lship:
                        ws['D' + str(iterator)] = l.score_representation
                        if lship[len(lship)-1].id != l.id:
                            iterator = iterator + 1
                        k = k + 1
                    a = a + 1
                elif k == 5:
                    iterator = iterator + 3
                    temp = iterator
                    for m in range(5, 13):
                        for l in range(3, 7):
                            ws.cell(row=temp, column=l).value = ws.cell(row=m, column=l).value
                            cell_format(ws.cell(row=temp, column=l), ws.cell(row=m, column=l))
                        temp = temp + 1
                    k = 0
                    ws['D' + str(iterator)] = d.level_name
                    ws['D' + str(iterator + 1)] = d.category_name
                    ws['C' + str(iterator + 1)] = a
                    iterator = iterator + 3
                    lship = LshipCategoryRatingMaster.objects.filter(lship_representation=d).order_by('score')
                    for l in lship:
                        ws['D' + str(iterator)] = l.score_representation
                        iterator = iterator + 1
                        k = k + 1
                    a = a + 1
            for z in range(len(wb.get_sheet_names())):
                sheet = wb.get_sheet_names()[z]
                ws = wb.get_sheet_by_name(sheet)
                ws.protection.sheet = True
                ws.protection.set_password('bulkupload@dig')
                ws.protection.enable()
                ws.protection.selectLockedCells = True
                ws.protection.selectUnlockedCells = True
            for z in range(len(wb.get_sheet_names())):
                sheet = wb.get_sheet_names()[z]
                ws = wb.get_sheet_by_name(sheet)
                ws_max_row = ws.max_row
                ws.protection.selectUnlockedCells = False
                cell = ws['E7:F' + str(ws_max_row)]
                for row in range(len(cell)):
                    row = cell[row]
                    for cell1 in row:
                        cell1.protection = Protection(locked=False, hidden=False)
            wb.save(response)
            return response


@is_portco_plant_selected
@login_required(login_url='login')
def lshipRating(request, act='0', assessment_date='',):
    is_acquired = checkIsAcquired(selected_portco(request))
    if act=='0': #New Assessment
        portco = PorfolioCompanyMaster.objects.get(id=request.session['global_portco'])
        # plant = PlantMaster.objects.get(id=request.session['global_plant'])
        lship = Leadership_Representation.objects.filter(is_active=True).values('id', 'category_name', 'category_description', 'level_name','level_description').order_by('level_name')
        msg = ''
        if 'lship_add' in request.session and request.session['lship_add'] is not None:
            if request.session['lship_add']:
                msg = 'Leadership Assessment successfully added'
            elif request.session['lship_add'] == False:
                msg = 'Leadership Assessment already exist'
        if 'lship_update' in request.session and request.session['lship_update'] is not None:
            if request.session['lship_update']:
                msg = 'Leadership Assessment successfully updated'
            elif request.session['lship_update'] == False:
                msg = 'Leadership Assessment update failed. Please check the details before updating.'
        # context = {'plant_list': plant,'myFilter':filter, 'msg':msg}
        lship_rating = LshipCategoryRatingMaster.objects.filter()
        if lship is not None:
            df_lship = pd.DataFrame(list(lship))
            lship_rating = LshipCategoryRatingMaster.objects.filter().values('id', 'score', 'score_representation', 'lship_representation')
            df_lship_rating = pd.DataFrame(list(lship_rating))
            df_lship_rating = df_lship_rating.pivot(index='lship_representation', columns='score', values='score_representation') \
                .add_suffix('_value') \
                .reset_index()
            df = pd.merge(df_lship, df_lship_rating, how='left',
                          left_on=['id'],
                          right_on=['lship_representation'])
            df.rename({'id': 'id_x'}, axis=1, inplace=True)
            json_records = df.reset_index().to_json(orient='records')
            lship = []
            lship = json.loads(json_records)
        context = {'lship_list': lship, 'msg':msg, 'portco':portco.portco_name, 'is_acquired': is_acquired}
        request.session['lship_add'] = None
        request.session['lship_update'] = None
        return render(request, "assessment_section/leadership_assessment/lship_assessment.html", context)
    else: #Old Assessment
        portco = PorfolioCompanyMaster.objects.get(id=request.session['global_portco'])
        # plant = PlantMaster.objects.get(id=request.session['global_plant'])
        # areaa = None
        lship = Leadership_Representation.objects.filter(is_active=True).values('id', 'category_name', 'category_description', 'level_name','level_description')
        lship_rating = LshipCategoryRatingMaster.objects.filter().values('id', 'score', 'score_representation', 'lship_representation')
        if lship is not None:
            df_lship = pd.DataFrame(list(lship))
            lship_rating = LshipCategoryRatingMaster.objects.filter().values('id', 'score', 'score_representation', 'lship_representation')
            df_lship_rating = pd.DataFrame(list(lship_rating))

            msg = ''
            if 'lship_add' in request.session and request.session['lship_add'] is not None:
                if request.session['lship_add']:
                    msg = 'Leadership Assessment successfully added'
                elif request.session['lship_add'] == False:
                    msg = 'Leadership Assessment already exist'
            if 'lship_update' in request.session and request.session['lship_update'] is not None:
                if request.session['lship_update']:
                    msg = 'Leadership Assessment successfully updated'
                elif request.session['lship_update'] == False:
                    msg = 'Leadership Assessment update failed. Please check the details before updating.'

            lship_rating_submitted = LeadershipRatings.objects.filter(portco=portco,
                                                             date_of_assessment_started=assessment_date).values('id', 'score__score', 'lship_id_id', 'description')
            df_lship_rating = df_lship_rating.pivot(index='lship_representation', columns='score',
                                                values='score_representation') \
            .add_suffix('_value') \
            .reset_index()

            if lship_rating_submitted is not None:
                lship_rating_submitted = pd.DataFrame(list(lship_rating_submitted))
                df_lship_rating = pd.merge(df_lship_rating, lship_rating_submitted, how='left',
                              left_on=['lship_representation'],
                              right_on=['lship_id_id'])
            df = pd.merge(df_lship, df_lship_rating, how='left',
                          left_on=['id'],
                          right_on=['lship_representation'])
            json_records = df.reset_index().to_json(orient='records')
            lship = []
            lship = json.loads(json_records)
        context = {'lship_list': lship, 'portco': portco.portco_name,
                   'assessment_date':assessment_date, 'msg':msg, 'is_acquired': is_acquired}
        request.session['lship_add'] = None
        request.session['lship_update'] = None
        return render(request, "assessment_section/leadership_assessment/lship_assessment.html", context)

@is_portco_plant_selected
@login_required(login_url='login')
def lshipRatingSubmmit(request):
    is_acquired = checkIsAcquired(selected_portco(request))
    assessment_date = request.POST['assessment_date']
    # area_list = request.POST.getlist('arealist')
    area = [v for k, v in request.POST.items() if k.startswith('marks_')]
    description_list = request.POST.getlist('descriptionlist')
    description = [v for k, v in request.POST.items() if k.startswith('description_')]
    portco = selected_portco(request)
    # plant = selected_plant(request)
    # j = 0
    for i in area:
        lship_cat_id = i.split("_")[0]
        lship_cat_id = Leadership_Representation.objects.get(id=lship_cat_id)
        score = i.split("_")[1]
        if score == '0':
            score = LshipCategoryRatingMaster.objects.get(score=int(score), lship_representation=None)
        else:
            score = LshipCategoryRatingMaster.objects.get(score=int(score), lship_representation=lship_cat_id)
        try:
            j = int(i.split("_")[0])
            description_post = description[j-1]
        except:
            description_post = None
        try:
            lshipp = LeadershipRatings.objects.get(portco=portco, date_of_assessment_started=assessment_date, lship_id=lship_cat_id)
            LeadershipRatings.objects.filter(pk=lshipp.id).update(portco=portco, date_of_assessment_started=assessment_date, date_of_assessment=assessment_date,
                                  score=score, lship_id=lship_cat_id, description=description_post, modified_by=request.user, modified_on=datetime.now())
            # j = j + 1
        except LeadershipRatings.DoesNotExist:
            lship_rating = LeadershipRatings(portco=portco, date_of_assessment_started=assessment_date, date_of_assessment=assessment_date,
                                  score=score, lship_id=lship_cat_id, description=description_post,
                                   added_by=request.user,
                                   added_on=datetime.now(), modified_by=request.user, modified_on=datetime.now())
            lship_rating.save()
            # j = j + 1
        request.session['lship_add'] = True
    # context = {'els_list': els, 'msg':msg, 'portco':portco.portco_name, 'plant':plant.plant_name}
    # request.session['els_add'] = None
    # request.session['els_update'] = None
    return redirect('home')

@is_portco_plant_selected
@login_required(login_url='login')
def isLshipAssessmentPresent(request):
    assessment_date = request.GET.get('assessment_date')
    portco = selected_portco(request)
    # plant = selected_plant(request)
    lshipp = LeadershipRatings.objects.filter(portco=portco, date_of_assessment_started=assessment_date)
        # act = 0
    if lshipp.count() > 0:
        request.session['assessment_date'] = assessment_date
        # request.session['area_id'] = 'abc'
        data = {
            'is_assessment_present': True,
            'assessment_date': assessment_date
        }
    else:
        data = {
            'is_assessment_present': False
        }
    return JsonResponse(data)


@is_portco_plant_selected
@login_required(login_url='login')
def LshipRedirection(request):
    return redirect('lship_assessment_xyz', act=1, assessment_date=request.session['assessment_date'])


@login_required(login_url='login')
def LshipCharts(request):
    if 'global_portco' in request.session:
        is_acquired = checkIsAcquired(selected_portco(request))
    else:
        is_acquired = False
    if request.method == 'POST':
        user_details = UserDetails.objects.get(user=request.user.id)

        if user_details.is_orc_employee == True:
            portco = PorfolioCompanyMaster.objects.filter(id = request.POST['portco'], is_delete=False)
            lship_doa = LeadershipRatings.objects.filter(portco__in=portco).values('date_of_assessment').distinct()
        else:
            portco = PorfolioCompanyMaster.objects.filter(id=user_details.portco, is_delete=False)
            lship_doa = LeadershipRatings.objects.filter(portco__in=portco).values('date_of_assessment').distinct()
        lship_doa = lship_doa.order_by('-date_of_assessment')
        portco_selected = request.POST['portco']
        portco_selected_obj = PorfolioCompanyMaster.objects.get(id=portco_selected)

        lship_doa1 = LeadershipRatings.objects.filter(portco = portco_selected_obj, date_of_assessment = request.POST['doa1']).values('lship_id__category_name','lship_id__level_name','score__score')
        lship_doa2 = LeadershipRatings.objects.filter(portco = portco_selected_obj, date_of_assessment = request.POST['doa2']).values('lship_id__category_name','lship_id__level_name','score__score')

        df_lship1 = pd.DataFrame(list(lship_doa1))
        df_lship2 = pd.DataFrame(list(lship_doa1))

        lship_doa1 = LeadershipRatings.objects.filter(portco=portco_selected_obj, date_of_assessment=request.POST['doa1']).values(
            'lship_id__category_name', 'lship_id__level_name', 'score__score')
        lship_doa2 = LeadershipRatings.objects.filter(portco=portco_selected_obj, date_of_assessment=request.POST['doa2']).values(
            'lship_id__category_name', 'lship_id__level_name', 'score__score')

        categories = Leadership_Category.objects.filter(is_active=True).values('category_name')
        levels = Leadership_Level.objects.filter(is_active=True).values('level_name')

        main_df_category = pd.DataFrame(list(categories))
        main_df_level = pd.DataFrame(list(levels))
        df_lship1 = pd.DataFrame(list(lship_doa1))
        df_lship2 = pd.DataFrame(list(lship_doa2))
        main_df_category.columns = ['category']
        main_df_level.columns = ['level']
        df_lship1.columns = ['category', 'level', 'score']
        df_lship2.columns = ['category', 'level', 'score']

        temp_dict_df1 = {}
        for i in range(len(main_df_category)):
            sample_df = pd.merge(main_df_level, df_lship1[df_lship1['category'] == main_df_category.loc[i, 'category']],
                                 on='level', how='left')
            sample_df['score'] = sample_df['score'].fillna(0)
            levels = list(sample_df.level)
            scores = list(sample_df.score)
            ##################
            # data = [go.Scatterpolar(r=scores, theta=levels, fill='toself', name = main_df_category.loc[i, 'category'])]
            # # plot_div = pyo.plot(data, output_type='div',show_link=False,link_text="")
            # layout = {
            #     'height': 420,
            #     'width': 560,
            #
            # }
            #
            # plot_div = pyo.plot({'data': data, 'layout': layout},
            #                 output_type='div', show_link=False, link_text="",config=dict(
            #         displayModeBar=False
            #     ))
            ###############
            j = 0
            for m in levels:
                if m.count(' ') >= 2:
                    levels[j] = '<br>'.join(' '.join(s) for s in zip(*[iter(m.split())] * 2))
                elif len(m) >= 12:
                    levels[j] = m.replace('/', '/<br>')
                    levels[j] = levels[j].replace(' ', '<br>')
                j = j + 1
            fig = go.Figure(
                data=[go.Scatterpolar(mode= "markers+lines",r=scores, theta=levels, fill='toself', name = main_df_category.loc[i, 'category'],line={'color':'#70001E', 'width':4},opacity=0.5, fillcolor= '#808080')],
                layout=go.Layout(
                    polar={'radialaxis': {'visible': True, 'range': [0, 5]},
                           'angularaxis': {'tickfont': {'size': 12, 'color' : 'black'}}}, width=580,
                    height=560
                )
            )
            plot_div = pyo.plot(fig,
                                 output_type='div', show_link=False, link_text="", config=dict(
                    displayModeBar=False
                ))

            temp_dict_df1[main_df_category.loc[i, 'category']] = plot_div

        temp_dict_df2 = {}
        for i in range(len(main_df_category)):
            sample_df = pd.merge(main_df_level, df_lship2[df_lship2['category'] == main_df_category.loc[i, 'category']],
                                 on='level', how='left')
            sample_df['score'] = sample_df['score'].fillna(0)
            sample_df['score'] = sample_df['score'].apply(np.int64)
            levels = list(sample_df.level)
            scores = list(sample_df.score)

            ########################
            # data = [go.Scatterpolar(r=scores, theta=levels, fill='toself', name = main_df_category.loc[i, 'category'])]
            # # plot_div = pyo.plot(data, output_type='div', show_link=False, link_text="")
            # layout = {
            #     'height': 420,
            #     'width': 560,
            # }
            #
            # plot_div = pyo.plot({'data': data, 'layout': layout},
            #                 output_type='div', show_link=False, link_text="",config=dict(
            #         displayModeBar=False
            #     ))
            ####################
            j = 0
            for m in levels:
                if m.count(' ') >= 2:
                    levels[j] = '<br>'.join(' '.join(s) for s in zip(*[iter(m.split())] * 2))
                elif len(m) >= 12:
                    levels[j] = m.replace('/', '/<br>')
                    levels[j] = levels[j].replace(' ', '<br>')
                j = j + 1
            fig = go.Figure(
                data=[go.Scatterpolar(mode= "markers+lines",r=scores, theta=levels, fill='toself', name=main_df_category.loc[i, 'category'],line={'color':'#70001E', 'width':4},opacity=0.5, fillcolor= '#808080')],
                layout=go.Layout(
                    polar={'radialaxis': {'visible': True, 'range': [0, 5]},
                           'angularaxis': {'tickfont': {'size': 12, 'color' : 'black'}}}, width=580,
                    height=560
                )
            )
            plot_div = pyo.plot(fig,
                                output_type='div', show_link=False, link_text="", config=dict(
                    displayModeBar=False
                ))

            temp_dict_df2[main_df_category.loc[i, 'category']] = plot_div



        category_list = list(main_df_category.category)
        temp_dict = {}
        for i in category_list:

            res1 = ""
            for j in i:
                if j.isalpha():
                    res1 = "".join([res1, j])

            temp_dict[i] = res1

        doa1_s = datetime.strptime(request.POST['doa1'], '%Y-%m-%d').strftime('%B %d, %Y')
        doa2_s = datetime.strptime(request.POST['doa2'], '%Y-%m-%d').strftime('%B %d, %Y')
        return render(request, "charts_section/leadership_charts.html", {'temp_dict_df1':temp_dict_df1, 'is_acquired': is_acquired,
                                                                         'temp_dict_df2':temp_dict_df2,'plot_div': plot_div,
                                                                         'temp_dict':temp_dict, 'portco': portco,
                                                                         'portco_selected': portco_selected,
                                                                         'lship_doa': lship_doa, 'selection':True,
                                                                         'doa1_selected':request.POST['doa1'],
                                                                         'doa2_selected':request.POST['doa2'], 'doa1_s': doa1_s, 'doa2_s': doa2_s, })
    else:
        user_details = UserDetails.objects.get(user=request.user.id)
        if user_details.is_orc_employee == True:
            portco = PorfolioCompanyMaster.objects.filter(is_delete=False)
        else:
            portco = PorfolioCompanyMaster.objects.filter(id=user_details.portco, is_delete=False)
        return render(request, "charts_section/leadership_charts.html", {'portco': portco, 'is_acquired': is_acquired})


@login_required(login_url='login')
def loadlshipdoa(request):
    portco = request.GET.get('portco')
    lship_doa = LeadershipRatings.objects.filter(portco=portco).values('date_of_assessment').distinct()
    lship_doa = lship_doa.order_by('-date_of_assessment')
    return render(request, 'charts_section/lship_doa_dropdown_list_options.html', {'lship_doa': lship_doa})

@login_required(login_url='login')
def loadelsdoa(request):
    plant = request.GET.get('plant')
    plant = PlantMaster.objects.get(id = plant)
    portco = plant.portco
    els_doa = ELSRatings.objects.filter(portco=portco,plant=plant).values('date_of_assessment').distinct()
    els_doa = els_doa.order_by('-date_of_assessment')
    return render(request, 'charts_section/els_doa_dropdown_list_options.html', {'els_doa': els_doa})

@login_required(login_url='login')
def loadareadoa(request):
    plant = request.GET.get('plant')
    plant = PlantMaster.objects.get(id = plant)
    portco = plant.portco
    els_doa = AreaRatings.objects.filter(portco=portco,plant=plant).values('date_of_assessment_started').distinct()
    els_doa = els_doa.order_by('-date_of_assessment_started')
    return render(request, 'charts_section/area_doa_dropdown_list_options.html', {'els_doa': els_doa})


def nth_repl_all(s, sub, repl, nth):
    find = s.find(sub)
    # loop util we find no match
    i = 1
    while find != -1:
        # if i  is equal to nth we found nth matches so replace
        if i == nth:
            s = s[:find]+repl+s[find + len(sub):]
            i = 0
        # find + len(sub) + 1 means we start after the last match
        find = s.find(sub, find + len(sub) + 1)
        i += 1
    return s


@login_required(login_url='login')
def ELSCharts(request):
    if 'global_portco' in request.session:
        is_acquired = checkIsAcquired(selected_portco(request))
    else:
        is_acquired = False
    if request.method == 'POST':
        user_details = UserDetails.objects.get(user=request.user.id)
        if user_details.is_orc_employee == True:
            portco = PorfolioCompanyMaster.objects.filter(id=request.POST['portco'],is_delete=False)
            plant = PlantMaster.objects.filter(id=request.POST['plant'], portco__in=portco)
            els_doa = ELSRatings.objects.filter(portco__in=portco, plant__in=plant).values('date_of_assessment').distinct()
        else:
            portco = PorfolioCompanyMaster.objects.filter(id=user_details.portco, is_delete=False)
            plant = PlantMaster.objects.filter(id=request.POST['plant'], portco__in=portco)
            els_doa = ELSRatings.objects.filter(portco__in=portco, plant__in=plant).values(
                'date_of_assessment').distinct()
        els_doa = els_doa.order_by('-date_of_assessment')

        portco_selected = request.POST['portco']
        portco_selected_obj = PorfolioCompanyMaster.objects.get(id=portco_selected)
        plant_selected = request.POST['plant']
        plant_selected_obj = PlantMaster.objects.get(id=plant_selected)

        els_doa1 = ELSRatings.objects.filter(portco = portco_selected_obj, plant = plant_selected_obj, date_of_assessment = request.POST['doa1']).values('els_category__category_name', 'score__score')
        els_doa2 = ELSRatings.objects.filter(portco = portco_selected_obj, plant = plant_selected_obj, date_of_assessment = request.POST['doa2']).values('els_category__category_name', 'score__score')

        df_els1 = pd.DataFrame(list(els_doa1))
        df_els2 = pd.DataFrame(list(els_doa2))
        categories = ELSCategoryMaster.objects.filter(is_delete=False).values('category_name')
        main_df_category = pd.DataFrame(list(categories))
        main_df_category.columns = ['category']
        df_els1.columns = ['category', 'score']
        df_els2.columns = ['category', 'score']

        #first chart
        sample_df1 = pd.merge(main_df_category, df_els1,
                             on='category', how='left')
        sample_df1['score'] = sample_df1['score'].fillna(0)
        sample_df1['score'] = sample_df1['score'].apply(np.int64)
        categories = list(sample_df1.category)
        scores = list(sample_df1.score)
        # data = [go.Scatterpolar(r=scores, theta=categories, fill='toself', name="ELS Radar")]
        # layout = {
        #     'height': 750,
        #     'width': 750,
        # }
        j = 0
        for m in categories:
            if m == 'Continuous Improvement Plan':
                categories[j] = categories[j].replace(' ', '<br>')
            elif m.count(' ') >= 2:
                categories[j] = m.replace('/', '/<br>')
                categories[j] = nth_repl_all(categories[j], ' ', '<br>', 2)
                # categories[j] = '<br>'.join(' '.join(s) for s in zip(*[iter(m.split())] * 2))
            elif len(m) >= 15:
                categories[j] = m.replace('/', '/<br>')
                categories[j] = categories[j].replace(' ', '<br>')
            j = j + 1


        # converter = lambda x: x.replace(' ', '<br>')
        # categories = list(map(converter, categories))
        fig = go.Figure(
            data=[go.Scatterpolar(mode= "markers+lines",r=scores, theta=categories, fill='toself', name="ELS Radar",line={'color':'#70001E', 'width':4},opacity=0.5, fillcolor= '#808080')],
            layout=go.Layout(
                polar={'radialaxis': {'visible': True, 'range': [0,5]},
                       'angularaxis' : {'tickfont' : {'size' : 9, 'color' : 'black'}, 'direction':'counterclockwise'}}, width=620,
        height=650
            )
        )
        plot_div1 = pyo.plot(fig,
                             output_type='div', show_link=False, link_text="", config=dict(
                displayModeBar=False
            ))


        # plot_div1 = pyo.plot({'data': data, 'layout': layout},
        #                     output_type='div', show_link=False, link_text="", config=dict(
        #         displayModeBar=False
        #     ))

        # second chart
        sample_df2 = pd.merge(main_df_category, df_els2,
                              on='category', how='left')
        sample_df2['score'] = sample_df2['score'].fillna(0)
        sample_df2['score'] = sample_df2['score'].apply(np.int64)
        categories = list(sample_df2.category)
        scores = list(sample_df2.score)
        # data = [go.Scatterpolar(r=scores, theta=categories, fill='toself', name="ELS Radar")]
        # layout = {
        #     'height': 750,
        #     'width': 750
        # }
        # plot_div2 = pyo.plot({'data': data, 'layout': layout},
        #                      output_type='div', show_link=False, link_text="", config=dict(
        #         displayModeBar=False
        #     ))
        j = 0
        for m in categories:
            if m == 'Continuous Improvement Plan':
                categories[j] = categories[j].replace(' ', '<br>')
            elif m.count(' ') >= 2:
                categories[j] = m.replace('/', '/<br>')
                categories[j] = nth_repl_all(categories[j], ' ', '<br>', 2)
                # categories[j] = '<br>'.join(' '.join(s) for s in zip(*[iter(m.split())] * 2))
            elif len(m) >= 15:
                categories[j] = m.replace('/', '/<br>')
                categories[j] = categories[j].replace(' ', '<br>')
            j = j + 1


        # converter = lambda x: x.replace(' ', '<br>')
        # categories = list(map(converter, categories))
        fig2 = go.Figure(
            data=[go.Scatterpolar(mode= "markers+lines", r=scores, theta=categories, fill='toself', name="ELS Radar",line={'color':'#70001E', 'width':4},opacity=0.5, fillcolor= '#808080')],
            layout=go.Layout(
                polar={'radialaxis': {'visible': True, 'range': [0, 5]},
                       'angularaxis': {'tickfont': {'size': 9, 'color' : 'black'}}}, width=620,
                height=650
            )
        )
        plot_div2 = pyo.plot(fig2,
                             output_type='div', show_link=False, link_text="", config=dict(
                displayModeBar=False
            ))

        doa1_s = datetime.strptime(request.POST['doa1'], '%Y-%m-%d').strftime('%B %d, %Y')
        doa2_s = datetime.strptime(request.POST['doa2'], '%Y-%m-%d').strftime('%B %d, %Y')


        return render(request, "charts_section/els_charts.html", {'plot_div1':plot_div1, 'is_acquired': is_acquired,
                                                                  'plot_div2':plot_div2, 'portco': portco,
                                                                  'portco_selected': portco_selected, 'plant': plant,
                                                                  'plant_selected': plant_selected,
                                                                  'doa1_selected':request.POST['doa1'], 'doa1_s': doa1_s, 'doa2_s': doa2_s,
                                                                  'doa2_selected':request.POST['doa2'], 'els_doa':els_doa,
                                                                  'selection':True})
    else:
        user_details = UserDetails.objects.get(user=request.user.id)
        if user_details.is_orc_employee == True:
            portco = PorfolioCompanyMaster.objects.filter(is_delete=False)
        else:
            portco = PorfolioCompanyMaster.objects.filter(id=user_details.portco, is_delete=False)
        return render(request, "charts_section/els_charts.html", {'portco': portco, 'is_acquired': is_acquired})


@login_required(login_url='login')
def BaselineCharts(request):
    if 'global_portco' in request.session:
        is_acquired = checkIsAcquired(selected_portco(request))
    else:
        is_acquired = False
    if request.method == 'POST':
        user_details = UserDetails.objects.get(user=request.user.id)
        if user_details.is_orc_employee == True:
            portco = PorfolioCompanyMaster.objects.filter(id=request.POST['portco'], is_delete=False)
            plant = PlantMaster.objects.filter(id=request.POST['plant'], portco__in=portco)
            els_doa = AreaRatings.objects.filter(portco__in=portco, plant__in=plant).values('date_of_assessment_started').distinct()
        else:
            portco = PorfolioCompanyMaster.objects.filter(id=user_details.portco, is_delete=False)
            plant = PlantMaster.objects.filter(id=request.POST['plant'], portco__in=portco)
            els_doa = AreaRatings.objects.filter(portco__in=portco, plant__in=plant).values(
                'date_of_assessment_started').distinct()
        els_doa = els_doa.order_by('-date_of_assessment_started')
        portco_selected = request.POST['portco']
        portco_selected_obj = PorfolioCompanyMaster.objects.get(id=portco_selected)
        plant_selected = request.POST['plant']
        plant_selected_obj = PlantMaster.objects.get(id=plant_selected)

        els_doa1 = AreaRatings.objects.filter(portco = portco_selected_obj, plant = plant_selected_obj, date_of_assessment_started = request.POST['doa1']).values('els_category__category_name', 'area__area_name', 'score__score')
        els_doa2 = AreaRatings.objects.filter(portco = portco_selected_obj, plant = plant_selected_obj, date_of_assessment_started = request.POST['doa2']).values('els_category__category_name', 'area__area_name', 'score__score')

        df_area1 = pd.DataFrame(list(els_doa1))
        df_area2 = pd.DataFrame(list(els_doa2))
        categories = ELSCategoryMaster.objects.filter().values('category_name')
        main_df_category = pd.DataFrame(list(categories))
        main_df_category.columns = ['category']
        df_area1.columns = ['category', 'area', 'score']
        df_area2.columns = ['category', 'area', 'score']

        sample_df1 = None
        sample_df2 = None

        sample_df1 = pd.merge(main_df_category, df_area1, on='category', how='left')
        sample_df1 = sample_df1.pivot(index='category', columns='area', values='score')
        columns = [x for x in list(sample_df1.columns) if str(x) != 'nan']
        sample_df1 = sample_df1[columns]
        sample_df1.columns = [x + ' ' for x in list(sample_df1.columns)]
        sample_df1['Average '] = sample_df1.mean(axis=1)
        sample_df1 = sample_df1.fillna(0)
        sample_df1 = sample_df1.round(decimals=1)

        sample_df2 = pd.merge(main_df_category, df_area2, on='category', how='left')
        sample_df2 = sample_df2.pivot(index='category', columns='area', values='score')
        columns = [x for x in list(sample_df2.columns) if str(x) != 'nan']
        sample_df2 = sample_df2[columns]
        sample_df2.columns = [x + '  ' for x in list(sample_df2.columns)]
        sample_df2['Average  '] = sample_df2.mean(axis=1)
        sample_df2 = sample_df2.fillna(0)
        sample_df2 = sample_df2.round(decimals=1)
        columns = list(sample_df2.columns)
        columns.insert(0, 'Category')
        sample_df2['Category'] = sample_df2.index
        sample_df2 = sample_df2[columns]

        final_df = pd.concat([sample_df1, sample_df2], axis=1)
        final_df.reset_index(inplace=True)
        del final_df['category']


        for i in list(final_df.columns):
            if i not in ['Category', 'Average ', 'Average  ']:
                final_df[i] = final_df[i].apply(np.int64)

        column_names = list(final_df.columns)
        column_names_dict = {x: x for x in column_names}


        json_records = final_df.reset_index().to_json(orient='records')
        data = []
        data = json.loads(json_records)

        return render(request, "charts_section/baseline_chart.html", {'column_names_dict':column_names_dict, 'column_names':column_names, 'data': data, 'portco': portco, 'portco_selected': portco_selected, 'plant': plant, 'plant_selected': plant_selected, 'doa1_selected':request.POST['doa1'], 'doa2_selected':request.POST['doa2'], 'els_doa':els_doa, 'selection':True, 'is_acquired': is_acquired})
    else:
        user_details = UserDetails.objects.get(user=request.user.id)
        if user_details.is_orc_employee == True:
            portco = PorfolioCompanyMaster.objects.filter(is_delete=False)
        else:
            portco = PorfolioCompanyMaster.objects.filter(id=user_details.portco, is_delete=False)
        return render(request, "charts_section/baseline_chart.html", {'portco': portco, 'is_acquired': is_acquired})


@login_required(login_url='login')
def trainingContentList(request):
    if 'global_portco' in request.session:
        is_acquired = checkIsAcquired(selected_portco(request))
    else:
        is_acquired = False
    training = TrainingContentMaster.objects.filter()
    if 'is_delete' in request.GET:
        if request.GET['is_delete'] == '':
            filter = TrainingFormFilter(request.GET, queryset=training)
        else:
            filter = TrainingFormFilter(request.GET, queryset=training)
            training = filter.qs
    else:
        filter = TrainingFormFilter(request.GET, queryset=training)
        training = filter.qs
    msg = ''
    if 'training_add' in request.session and request.session['training_add'] is not None:
        if request.session['training_add']:
            msg = 'Training module successfully added'
        elif request.session['training_add'] == False:
            msg = 'Training module already exist'
    if 'training_update' in request.session and request.session['training_update'] is not None:
        if request.session['training_update']:
            msg = 'Training module successfully updated'
        elif request.session['training_update'] == False:
            msg = 'Training module update failed. Please check the company name before updating.'
    user_details = UserDetails.objects.get(user=request.user.id)
    if request.user.is_superuser == True:
        admin = 'True'
    else:
        admin = 'False'
    context = {'training_list': training,'myFilter':filter, 'msg': msg, 'is_acquired': is_acquired, 'admin':admin}
    request.session['training_add'] = None
    request.session['training_update'] = None
    return render(request, "training_module/training_module_list_page.html", context)


@user_passes_test(lambda u: u.is_superuser)
@login_required(login_url='login')
def trainingModuleCreate(request):
    if 'global_portco' in request.session:
        is_acquired = checkIsAcquired(selected_portco(request))
    else:
        is_acquired = False
    if request.method == 'POST':
        try:
            portco = TrainingContentMaster.objects.get(module_name=request.POST['module_name'])
            request.session['training_add'] = False
            return redirect('training_content_list')
            # return render(request, 'admin/portco_section/portco_list_page.html', {'portco_list': PorfolioCompanyMaster.objects.all(), 'error': "Client Exist"})
        except TrainingContentMaster.DoesNotExist:
            if 'upload_file' in request.FILES:
                upload_file = request.FILES["upload_file"]
                fs = FileSystemStorage()
                file_path = os.path.join(settings.BASE_DIR,
                                         'assessment_app/static/assessment_app/training_docs/' + request.POST['module_name'] + '.' + upload_file.name.split('.')[1]).replace("\\", "/")
                file = fs.save(file_path, upload_file)
                fileurl = fs.url(file)
                document = True
                training_new = TrainingContentMaster(module_name=request.POST['module_name'],
                                                     module_description=request.POST['module_description'],
                                                     added_by=request.user, document=document,
                                                     document_name=request.POST['module_name'] + '.' +
                                                                   upload_file.name.split('.')[1],
                                                     added_on=datetime.now(), modified_by=request.user,
                                                     modified_on=datetime.now())
                training_new.save()
            else:
                document = False
                training_new = TrainingContentMaster(module_name=request.POST['module_name'],
                                                     module_description=request.POST['module_description'],
                                                     added_by=request.user, document=document,
                                                     document_name=None,
                                                     added_on=datetime.now(), modified_by=request.user,
                                                     modified_on=datetime.now())
                training_new.save()


            request.session['training_add'] = True
            return redirect('training_content_list')
    else:
        form = FormTraining()
        # form_portco_broker = FormClientBrokerMapping()
        context_form = {'form': form, 'is_acquired': is_acquired}
    return render(request, "training_module/training_module_create.html", context_form)


@user_passes_test(lambda u: u.is_superuser)
@login_required(login_url='login')
def trainingUpdate(request, pk):
    if 'global_portco' in request.session:
        is_acquired = checkIsAcquired(selected_portco(request))
    else:
        is_acquired = False
    tr = get_object_or_404(TrainingContentMaster, pk=pk)
    if request.method == 'POST':
        if tr.module_name == request.POST['module_name']:
            form = FormTraining(request.POST)
            if 'upload_file' in request.FILES:
                upload_file = request.FILES["upload_file"]
                fs = FileSystemStorage()
                file_path = os.path.join(settings.BASE_DIR,
                                         'assessment_app/static/assessment_app/training_docs/' + request.POST['module_name'] + '.' + upload_file.name.split('.')[1]).replace("\\", "/")
                if os.path.exists(file_path):
                    os.remove(file_path)
                file = fs.save(file_path, upload_file)
                fileurl = fs.url(file)
                document = True
                TrainingContentMaster.objects.filter(pk=pk).update(module_name=request.POST['module_name'],
                                                                   module_description=request.POST['module_description']
                                                                   , is_delete=request.POST['is_delete'], document=document, document_name=request.POST['module_name'] + '.' + upload_file.name.split('.')[1],
                                                                   modified_by=request.user, modified_on=datetime.now())
            else:
                document = False
                TrainingContentMaster.objects.filter(pk=pk).update(module_name=request.POST['module_name'],
                                                                   module_description=request.POST['module_description']
                                                                   , is_delete=request.POST['is_delete'],
                                                                   modified_by=request.user, modified_on=datetime.now())
            form = FormTraining(request.POST)
            request.session['training_update'] = True
        else:
            try:
                tr = TrainingContentMaster.objects.get(module_name=request.POST['module_name'])
                request.session['training_update'] = False
            except TrainingContentMaster.DoesNotExist:
                form = FormTraining(request.POST)
                if 'upload_file' in request.FILES:
                    upload_file = request.FILES["upload_file"]
                    fs = FileSystemStorage()
                    file_path = os.path.join(settings.BASE_DIR,
                                             'assessment_app/static/assessment_app/training_docs/' + request.POST['module_name'] + '.' + upload_file.name.split('.')[1]).replace("\\", "/")
                    if os.path.exists(file_path):
                        os.remove(file_path)
                    file = fs.save(file_path, upload_file)
                    fileurl = fs.url(file)
                    document = True
                    TrainingContentMaster.objects.filter(pk=pk).update(module_name=request.POST['module_name'],
                                                                       module_description=request.POST[
                                                                           'module_description']
                                                                       , is_delete=request.POST['is_delete'], document=document, document_name=request.POST['module_name'] + '.' + upload_file.name.split('.')[1],
                                                                       modified_by=request.user,
                                                                       modified_on=datetime.now())
                else:
                    document = False
                    TrainingContentMaster.objects.filter(pk=pk).update(module_name=request.POST['module_name'],
                                                                       module_description=request.POST[
                                                                           'module_description']
                                                                       , is_delete=request.POST['is_delete'],
                                                                       modified_by=request.user,
                                                                       modified_on=datetime.now())

                form = FormTraining(request.POST)
                request.session['training_update'] = True
        return redirect('training_content_list')
    else:
        form = FormTraining(instance=tr)
    context = {'form': form, 'is_acquired': is_acquired}
    return render(request, "training_module/training_module_update.html", context)


@login_required(login_url='login')
def downloadTrainingContent(request, id=''):
    if 'global_portco' in request.session:
        is_acquired = checkIsAcquired(selected_portco(request))
    else:
        is_acquired = False
    tr = TrainingContentMaster.objects.get(id=id)
    filename = tr.document_name
    file_path = os.path.join(settings.BASE_DIR, 'assessment_app/static/assessment_app/training_docs/' + filename).replace("\\","/")
    if os.path.exists(file_path):
        response = FileResponse(open(file_path, 'rb'))
        return response


@login_required(login_url='login')
def downloadELSEVidence(request, assessment_date='', id='', act='', area_id=''):
    if 'global_portco' in request.session:
        is_acquired = checkIsAcquired(selected_portco(request))
    else:
        is_acquired = False
    els = ELSCategoryMaster.objects.get(id=id)
    portco=selected_portco(request)
    plant=selected_plant(request)

    if act=='els':
        file_path = os.path.join(settings.BASE_DIR,
                                 'assessment_app/static/assessment_app/images/evidence/' + str(
                                     portco.portco_name) + '/' + str(plant.plant_name)
                                 + '/' + 'ELS_Assessment/' + str(assessment_date) + '/' + str(portco.portco_name)+'_'+str(plant.plant_name)+'_'+str(assessment_date).replace('-','')+"_"+str(id) + '.zip').replace("\\", "/")
        if os.path.exists(file_path):
            # response = HttpResponse(open(file_path, 'rb'), content_type='application/force-download')
            # response['Content-Disposition'] = 'attachment; filename="{}"'.format(open(file_path, 'rb'))
            # return response
            response = FileResponse(open(file_path, 'rb'))
            return response
    if act=='area':
        area = area_id
        file_path = os.path.join(settings.BASE_DIR,
                                 'assessment_app/static/assessment_app/images/evidence/' + str(
                                     portco.portco_name) + '/' + str(plant.plant_name)
                                 + '/' + 'Area_Assessment/' + str(area) + '/' + str(assessment_date) + '/' + str(portco.portco_name)+'_'+str(plant.plant_name)+'_'+str(assessment_date).replace('-','')+"_"+str(area)+'_'+str(id) + '.zip').replace(
            "\\", "/")
        if os.path.exists(file_path):
            response = FileResponse(open(file_path, 'rb'))
            return response


@is_portco_plant_selected
@login_required(login_url='login')
def elsRatingYesNO(request, act='0', assessment_date='', area=''):
    is_acquired = checkIsAcquired(selected_portco(request))
    if act=='0': #New Assessment
        portco = PorfolioCompanyMaster.objects.get(id=request.session['global_portco'])
        plant = PlantMaster.objects.get(id=request.session['global_plant'])
        els = ELSCategoryMaster.objects.filter(is_delete=False).values('id', 'category_name', 'four_P_category', 'four_P_category__description','category_description')
        msg = ''
        if 'els_add' in request.session and request.session['els_add'] is not None:
            if request.session['els_add']:
                msg = 'ELS Assessment successfully added'
            elif request.session['els_add'] == False:
                msg = 'ELS Category already exist'
        if 'els_update' in request.session and request.session['els_update'] is not None:
            if request.session['els_update']:
                msg = 'ELS Assessment successfully updated'
            elif request.session['els_update'] == False:
                msg = 'ELS Assessment update failed. Please check the category name before updating.'
        els_rating = ELSCategoryQuestionMaster.objects.filter()
        els_dict = {}
        if els is not None:
            df_els = pd.DataFrame(list(els))

            for i in els:
                els_rating = ELSCategoryQuestionMaster.objects.filter(els_category=i['id']).values('id', 'els_category', 'is_target',
                                                                               'weight_y', 'weight_n', 'description')
                els_dict[i['id']] = els_rating

            els_rating = ELSCategoryRatingMaster.objects.filter().values('id', 'score', 'description', 'els_category_id')
            df_els_rating = pd.DataFrame(list(els_rating))
            df_els_rating = df_els_rating.pivot(index='els_category_id', columns='score', values='description') \
                .add_suffix('_value') \
                .reset_index()
            df = pd.merge(df_els, df_els_rating, how='left',
                          left_on=['id'],
                          right_on=['els_category_id'])
            df.rename({'id': 'id_x'}, axis=1, inplace=True)
            json_records = df.reset_index().to_json(orient='records')
            els = []
            els = json.loads(json_records)
        context = {'els_list': els, 'els_dict':els_dict, 'msg':msg, 'portco':portco.portco_name, 'plant':plant.plant_name, 'is_acquired': is_acquired}
        request.session['els_add'] = None
        request.session['els_update'] = None
        return render(request, "assessment_section/els_assessment_yes_no/els_assessment.html", context)
    elif act=='1': #Old Assessment
        portco = PorfolioCompanyMaster.objects.get(id=request.session['global_portco'])
        plant = PlantMaster.objects.get(id=request.session['global_plant'])
        if area != 'abc' and area != '':
            areaa = AreaDetails.objects.get(id=area)
            area = AreaDetails.objects.filter(id=area, is_delete=False).values('id', 'area_name')
            els = ELSCategoryMaster.objects.filter(is_delete=False).values('id', 'category_name', 'four_P_category',
                                                                           'four_P_category__description',
                                                                           'category_description')
            els_rating = ELSCategoryRatingMaster.objects.filter()
            if els is not None:
                df_els = pd.DataFrame(list(els))
                els_rating = ELSCategoryRatingMaster.objects.filter().values('id', 'score', 'description',
                                                                             'els_category_id')
                df_els_rating = pd.DataFrame(list(els_rating))
                els_rating_submitted = AreaRatings.objects.filter(portco=portco, plant=plant, area=areaa,
                                                                 date_of_assessment_started=assessment_date, ).values(
                    'id', 'score__score', 'els_category__id', 'description', 'is_evidence')
                df_els_rating = df_els_rating.pivot(index='els_category_id', columns='score', values='description') \
                    .add_suffix('_value') \
                    .reset_index()
                if els_rating_submitted is not None:
                    els_rating_submitted = pd.DataFrame(list(els_rating_submitted))
                    df_els_rating = pd.merge(df_els_rating, els_rating_submitted, how='left',
                                             left_on=['els_category_id'],
                                             right_on=['els_category__id'])
                df = pd.merge(df_els, df_els_rating, how='left',
                              left_on=['id'],
                              right_on=['els_category_id'])
                json_records = df.reset_index().to_json(orient='records')
                els = []
                els = json.loads(json_records)
        else:
            areaa = None
            els = ELSCategoryMaster.objects.filter(is_delete=False).values('id', 'category_name', 'four_P_category',
                                                                           'four_P_category__description',
                                                                           'category_description')
            els_rating = ELSCategoryRatingMaster.objects.filter()
            els_rating = ELSCategoryQuestionMaster.objects.filter()
            els_dict = {}
            if els is not None:
                for i in els:
                    els_rating = ELSCategoryQuestionMaster.objects.filter(els_category=i['id']).values('id',
                                                                                                       'els_category',
                                                                                                       'is_target',
                                                                                                       'weight_y',
                                                                                                       'weight_n',
                                                                                                       'description')
                    els_rating_submitted = ELSCategoryQuestionResponse.objects.filter(portco=portco, plant=plant,
                                                                     date_of_assessment=assessment_date, els_category=i['id']
                                                                     ).values(
                        'question__id', 'response')
                    if els_rating.count()>0:
                        df_els_rating = pd.DataFrame(list(els_rating))
                        if els_rating_submitted.count() > 0:
                            df_els_rating_submitted = pd.DataFrame(list(els_rating_submitted))
                            df = pd.merge(df_els_rating, df_els_rating_submitted, how='left',
                                                     left_on=['id'],
                                                     right_on=['question__id'])
                        else:
                            df_els_rating['response'] = False
                            df = df_els_rating
                        json_records = df.reset_index().to_json(orient='records')
                        els_rating = []
                        els_rating = json.loads(json_records)
                        els_dict[i['id']] = els_rating
                df_els = pd.DataFrame(list(els))
                els_rating = ELSCategoryRatingMaster.objects.filter().values('id', 'score', 'description',
                                                                             'els_category_id')
                df_els_rating = pd.DataFrame(list(els_rating))
                els_rating_submitted = ELSRatings.objects.filter(portco=portco, plant=plant,
                                                                 date_of_assessment_started=assessment_date).values('id', 'score__score', 'els_category__id', 'description', 'is_evidence', 'score_y_n')
                df_els_rating = df_els_rating.pivot(index='els_category_id', columns='score', values='description') \
                    .add_suffix('_value') \
                    .reset_index()
                if els_rating_submitted is not None:
                    els_rating_submitted = pd.DataFrame(list(els_rating_submitted))
                    df_els_rating = pd.merge(df_els_rating, els_rating_submitted, how='left',
                                  left_on=['els_category_id'],
                                  right_on=['els_category__id'])
                df = pd.merge(df_els, df_els_rating, how='left',
                              left_on=['id'],
                              right_on=['els_category_id'])
                json_records = df.reset_index().to_json(orient='records')
                els = []
                els = json.loads(json_records)
        context = {'els_list': els, 'els_dict':els_dict, 'portco': portco.portco_name, 'plant': plant.plant_name, 'area':areaa,
                   'assessment_date':assessment_date, 'is_acquired': is_acquired}
        request.session['els_add'] = None
        request.session['els_update'] = None
        return render(request, "assessment_section/els_assessment_yes_no/els_assessment.html", context)
    elif act=='2':
        portco = PorfolioCompanyMaster.objects.get(id=request.session['global_portco'])
        plant = PlantMaster.objects.get(id=request.session['global_plant'])
        area = AreaDetails.objects.get(id=area)
        els = ELSCategoryMaster.objects.filter(is_delete=False).values('id', 'category_name', 'four_P_category',
                                                                       'four_P_category__description',
                                                                       'category_description')
        msg = ''
        if 'els_add' in request.session and request.session['els_add'] is not None:
            if request.session['els_add']:
                msg = 'ELS Assessment successfully added'
            elif request.session['els_add'] == False:
                msg = 'ELS Category already exist'
        if 'els_update' in request.session and request.session['els_update'] is not None:
            if request.session['els_update']:
                msg = 'ELS Assessment successfully updated'
            elif request.session['els_update'] == False:
                msg = 'ELS Assessment update failed. Please check the category name before updating.'
        # context = {'plant_list': plant,'myFilter':filter, 'msg':msg}
        els_rating = ELSCategoryRatingMaster.objects.filter()
        if els is not None:
            df_els = pd.DataFrame(list(els))
            els_rating = ELSCategoryRatingMaster.objects.filter().values('id', 'score', 'description',
                                                                         'els_category_id')
            df_els_rating = pd.DataFrame(list(els_rating))
            df_els_rating = df_els_rating.pivot(index='els_category_id', columns='score', values='description') \
                .add_suffix('_value') \
                .reset_index()
            df = pd.merge(df_els, df_els_rating, how='left',
                          left_on=['id'],
                          right_on=['els_category_id'])
            df.rename({'id': 'id_x'}, axis=1, inplace=True)
            json_records = df.reset_index().to_json(orient='records')
            els = []
            els = json.loads(json_records)
        context = {'els_list': els, 'msg': msg, 'portco': portco.portco_name, 'plant': plant.plant_name, 'area':area, 'is_acquired': is_acquired}
        request.session['els_add'] = None
        request.session['els_update'] = None
        return render(request, "assessment_section/els_assessment_yes_no/els_assessment.html", context)


@is_portco_plant_selected
@login_required(login_url='login')
def elsYesNoRatingSubmmit(request):
    is_acquired = checkIsAcquired(selected_portco(request))
    assessment_date = request.POST['assessment_date']
    # area_list = request.POST.getlist('arealist')
    active_els = ELSCategoryMaster.objects.filter(is_delete=False)
    # marks = [v for k, v in request.POST.items() if k.startswith('marks_')]
    # description_list = request.POST.getlist('descriptionlist')
    # description = [v for k, v in request.POST.items() if k.startswith('description_')]
    # description = [i for i in description if i != 'None']
    portco = selected_portco(request)
    plant = selected_plant(request)

    j = 0
    for els in active_els:
        marks = [v for k, v in request.POST.items() if k.startswith('marks_' + str(els.id) + '_')]
        description = [v for k, v in request.POST.items() if k.startswith('description_' + str(els.id))]
        description = [i for i in description if i]
        if len(marks) > 0:
            target = False
            for i in marks:
                els_ques_id = i.split("_")[0]
                els_ques_id = ELSCategoryQuestionMaster.objects.get(id=els_ques_id)
                els_cat_id = i.split("_")[1]
                els_cat_id = ELSCategoryMaster.objects.get(id=els_cat_id)
                score = i.split("_")[2]
                if score=='y':
                    score = els_ques_id
                    if score.is_target == True:
                        target = True
                    score = score.weight_y
                    response = True
                else:
                    score = els_ques_id
                    score = score.weight_n
                    response = False
                try:
                    elss = ELSCategoryQuestionResponse.objects.get(portco=portco, plant=plant, date_of_assessment=assessment_date,
                                                  els_category=els_cat_id, question=els_ques_id)
                    ELSCategoryQuestionResponse.objects.filter(pk=elss.id).update(portco=portco, plant=plant,
                                                                 date_of_assessment=assessment_date, weight=score, response=response,
                                                                  els_category=els_cat_id, question=els_ques_id,
                                                                  modified_by=request.user,
                                                                 modified_on=datetime.now())
                except ELSCategoryQuestionResponse.DoesNotExist:
                    els_response = ELSCategoryQuestionResponse(portco=portco, plant=plant,
                                                                 date_of_assessment=assessment_date, weight=score, response=response,
                                                                  els_category=els_cat_id, question=els_ques_id,
                                            added_by=request.user,
                                            added_on=datetime.now(), modified_by=request.user, modified_on=datetime.now())
                    els_response.save()

            if ("image_" + str(els.id)) in request.FILES:
                upload_file1 = request.FILES.getlist("image_" + str(els.id))
                for upload_file in upload_file1:
                    fs = FileSystemStorage()
                    file_path = os.path.join(settings.BASE_DIR,
                                             'assessment_app/static/assessment_app/images/evidence/' + str(portco.portco_name) + '/' + str(plant.plant_name)
                                             + '/' + 'ELS_Assessment/' + str(assessment_date) + '/' + str(els.id) + '/' +
                                             upload_file.name).replace("\\", "/")
                    if os.path.exists(file_path):
                        os.remove(file_path)
                    file = fs.save(file_path, upload_file)
                    fileurl = fs.url(file)
                output_filename = os.path.join(settings.BASE_DIR,
                                             'assessment_app/static/assessment_app/images/evidence/' + str(portco.portco_name) + '/' + str(plant.plant_name)
                                             + '/' + 'ELS_Assessment/' + str(assessment_date) + '/' + str(els.id)).replace("\\", "/")
                dir_name = os.path.join(settings.BASE_DIR,
                                             'assessment_app/static/assessment_app/images/evidence/' + str(portco.portco_name) + '/' + str(plant.plant_name)
                                             + '/' + 'ELS_Assessment/' + str(assessment_date) + '/' + str(els.id)).replace("\\", "/")
                shutil.make_archive(output_filename, 'zip', dir_name)
                shutil.rmtree(dir_name)
                evidence = True
            else:
                file_path = os.path.join(settings.BASE_DIR,
                                         'assessment_app/static/assessment_app/images/evidence/' + str(
                                             portco.portco_name) + '/' + str(plant.plant_name)
                                         + '/' + 'ELS_Assessment/' + str(assessment_date) + '/' + str(els.id) + '.zip').replace("\\", "/")
                if os.path.exists(file_path):
                    evidence = True
                else:
                    evidence = False


            try:
                description_post = description[j]
            except:
                description_post = None
            score = calculateScore(target, els, assessment_date, plant, portco)
            try:
                elss = ELSRatings.objects.get(portco=portco, plant=plant, date_of_assessment=assessment_date, els_category=els)
                ELSRatings.objects.filter(pk=elss.id).update(portco=portco, plant=plant, date_of_assessment=assessment_date,
                                      score_y_n=score, els_category=els, description=description_post, modified_by=request.user, modified_on=datetime.now(), is_evidence=evidence)
            except ELSRatings.DoesNotExist:
                els_rating = ELSRatings(portco=portco, plant=plant, date_of_assessment=assessment_date,
                                      score_y_n=score, els_category=els, description=description_post, is_evidence=evidence,
                                       added_by=request.user,
                                       added_on=datetime.now(), modified_by=request.user, modified_on=datetime.now())
                els_rating.save()
            request.session['els_add'] = True
    # context = {'els_list': els, 'msg':msg, 'portco':portco.portco_name, 'plant':plant.plant_name}
    # request.session['els_add'] = None
    # request.session['els_update'] = None
    return redirect('home')


def calculateScore(target, els, assessment_date, plant, portco):
    if target == True:
        score = 0
    else:
        els_response = ELSCategoryQuestionResponse.objects.filter(portco=portco, plant=plant,
                                    date_of_assessment=assessment_date,
                                    els_category=els).values('weight').annotate(weights=Sum('weight'))
        els_response = els_response.values_list('weights',flat=True)[0]
        score = els_response
    return score


@user_passes_test(lambda u: u.is_superuser)
@login_required(login_url='login')
def elsYesNoCreate(request):
    if 'global_portco' in request.session:
        is_acquired = checkIsAcquired(selected_portco(request))
    else:
        is_acquired = False
    if request.method == 'POST':
        try:
            els = ELSCategoryMaster.objects.get(category_name=request.POST['category_name'])
            request.session['els_add'] = False
            return redirect('els_list')
            # return render(request, 'admin/plant_section/plant_list_page.html', {'plant_list': plant, 'error': "Fund Exist"})
        except ELSCategoryMaster.DoesNotExist:
            four_p = FourPMaster.objects.get(id=request.POST['four_P_category'])
            els_new = ELSCategoryMaster(category_name=request.POST['category_name'], category_description=request.POST['category_description'], four_P_category=four_p, added_by=request.user,
                                      added_on=datetime.now(), modified_by=request.user, modified_on=datetime.now())
            els_new.save()
            # els_new = ELSCategoryMaster.objects.get(id=1)
            target_list = request.POST.getlist('targetlist')
            target = [v for k, v in request.POST.items() if k.startswith('is_target_')]
            desc_list = request.POST.getlist('descriptionlist')
            desc = [v for k, v in request.POST.items() if k.startswith('description_')]
            yes_list = request.POST.getlist('yes')
            yes = [v for k, v in request.POST.items() if k.startswith('yes_')]
            no_list = request.POST.getlist('no')
            no = [v for k, v in request.POST.items() if k.startswith('no_')]
            j=0
            for i in target:
                if target_list[j] != '':
                    # x = i.split("_")[1]
                    if i == 'Target':
                        yes_wtg = 0
                        no_wtg = 0
                        is_target = True
                    else:
                        yes_wtg = yes[j]
                        no_wtg = no[j]
                        is_target = False
                    els_rating_new = ELSCategoryQuestionMaster(els_category=els_new, is_target=is_target,
                                                               weight_y=yes_wtg, weight_n=no_wtg, description=desc[j], added_by=request.user,
                                              added_on=datetime.now(), modified_by=request.user, modified_on=datetime.now())
                    els_rating_new.save()
                    j=j+1

            request.session['els_add'] = True
            return redirect('els_list')
    else:
        form = FormElsCategory()
        context = {'form':form, 'is_acquired': is_acquired}
    return render(request, "admin/els_section/els_yes_no_create.html", context)


@user_passes_test(lambda u: u.is_superuser)
@login_required(login_url='login')
def elsYesNoUpdate(request, pk):
    if 'global_portco' in request.session:
        is_acquired = checkIsAcquired(selected_portco(request))
    else:
        is_acquired = False
    els = get_object_or_404(ELSCategoryMaster, pk=pk)
    # rating = ELSCategoryRatingMaster.objects.filter(els_category=els).values('score', 'id', 'description').order_by('score')
    rating = ELSCategoryQuestionMaster.objects.filter(els_category=els).values('weight_y', 'weight_n', 'is_target', 'id', 'description').order_by(
        'id')
    if request.method == 'POST':
        four_p = get_object_or_404(FourPMaster, id=request.POST['four_P_category'])
        if els.category_name == request.POST['category_name']:
            ELSCategoryMaster.objects.filter(pk=pk).update(category_name=request.POST['category_name'], category_description=request.POST['category_description'], four_P_category=four_p,
                                                     modified_by=request.user, modified_on=datetime.now(), is_delete=request.POST['is_delete'])
            target_list = request.POST.getlist('targetlist')
            target = [v for k, v in request.POST.items() if k.startswith('is_target_')]
            desc_list = request.POST.getlist('descriptionlist')
            desc = [v for k, v in request.POST.items() if k.startswith('description_')]
            yes_list = request.POST.getlist('yes')
            yes = [v for k, v in request.POST.items() if k.startswith('yes_')]
            no_list = request.POST.getlist('no')
            no = [v for k, v in request.POST.items() if k.startswith('no_')]
            j = 0
            for i in target:
                if target_list[j] != '':
                    # x = i.split("_")[1]
                    if i == 'Target':
                        yes_wtg = 0
                        no_wtg = 0
                        is_target = True
                    else:
                        yes_wtg = yes[j]
                        no_wtg = no[j]
                        is_target = False
                    try:
                        els_rating_new = ELSCategoryQuestionMaster.objects.get(els_category=els, id=target_list[j].split("_")[2])
                        ELSCategoryQuestionMaster.objects.filter(id=els_rating_new.id).update(els_category=els, is_target=is_target,
                                                               weight_y=yes_wtg, weight_n=no_wtg, description=desc[j],
                                                                modified_by=request.user,
                                                               modified_on=datetime.now())
                    except ELSCategoryQuestionMaster.DoesNotExist:
                        els_rating_new = ELSCategoryQuestionMaster(els_category=els, is_target=is_target,
                                                                   weight_y=yes_wtg, weight_n=no_wtg, description=desc[j],
                                                                   added_by=request.user,
                                                                   added_on=datetime.now(), modified_by=request.user,
                                                                   modified_on=datetime.now())
                        els_rating_new.save()
                    j = j + 1
            request.session['els_update'] = True
        else:
            try:
                els = ELSCategoryMaster.objects.get(category_name=request.POST['category_name'])
                request.session['els_update'] = False
                return redirect('els_list')
            except ELSCategoryMaster.DoesNotExist:
                ELSCategoryMaster.objects.filter(pk=pk).update(category_name=request.POST['category_name'],
                                                               category_description=request.POST[
                                                                   'category_description'], four_P_category=four_p,
                                                               modified_by=request.user, modified_on=datetime.now(),
                                                               is_delete=request.POST['is_delete'])
                target_list = request.POST.getlist('targetlist')
                target = [v for k, v in request.POST.items() if k.startswith('is_target_')]
                desc_list = request.POST.getlist('descriptionlist')
                desc = [v for k, v in request.POST.items() if k.startswith('description_')]
                yes_list = request.POST.getlist('yes')
                yes = [v for k, v in request.POST.items() if k.startswith('yes_')]
                no_list = request.POST.getlist('no')
                no = [v for k, v in request.POST.items() if k.startswith('no_')]
                j = 0
                for i in target:
                    if target_list[j] != '':
                        # x = i.split("_")[1]
                        if i == 'Target':
                            yes_wtg = 0
                            no_wtg = 0
                            is_target = True
                        else:
                            yes_wtg = yes[j]
                            no_wtg = no[j]
                            is_target = False
                        try:
                            els_rating_new = ELSCategoryQuestionMaster.objects.get(els_category=els,
                                                                                   id=target_list[j].split("_")[2])
                            ELSCategoryQuestionMaster.objects.filter(id=els_rating_new.id).update(els_category=els,
                                                                                                  is_target=is_target,
                                                                                                  weight_y=yes_wtg,
                                                                                                  weight_n=no_wtg,
                                                                                                  description=desc[j],
                                                                                                  modified_by=request.user,
                                                                                                  modified_on=datetime.now())
                        except ELSCategoryQuestionMaster.DoesNotExist:
                            els_rating_new = ELSCategoryQuestionMaster(els_category=els, is_target=is_target,
                                                                       weight_y=yes_wtg, weight_n=no_wtg,
                                                                       description=desc[j],
                                                                       added_by=request.user,
                                                                       added_on=datetime.now(),
                                                                       modified_by=request.user,
                                                                       modified_on=datetime.now())
                            els_rating_new.save()
                        j = j + 1
                request.session['els_update'] = True
        return redirect('els_list')
    else:
        form = FormElsCategory(instance=els)
    context = {'form': form, 'ratings':rating, 'is_acquired': is_acquired}
    return render(request,'admin/els_section/els_yes_no_update.html', context)


@is_portco_plant_selected
@login_required(login_url='login')
def isYesNoElsAssessmentPresent(request):
    is_acquired = checkIsAcquired(selected_portco(request))
    assessment_date = request.GET.get('assessment_date')
    portco = selected_portco(request)
    plant = selected_plant(request)
    if request.GET.get('area') != '0':
        area = AreaDetails.objects.get(id=request.GET.get('area'))
        elss = AreaRatings.objects.filter(portco=portco, plant=plant, area=area, date_of_assessment_started=assessment_date)
        act = 1
    else:
        elss = ELSRatings.objects.filter(portco=portco, plant=plant, date_of_assessment_started=assessment_date, score_y_n__gte=0)
        act = 0
    if elss.count() > 0:
        request.session['assessment_date'] = assessment_date
        if act == 1:
            request.session['area_id'] = area.id
        else:
            request.session['area_id'] = 'abc'
        data = {
            'is_assessment_present': True,
            'assessment_date': assessment_date
        }
    else:
        data = {
            'is_assessment_present': False
        }
    return JsonResponse(data)


@is_portco_plant_selected
@login_required(login_url='login')
def yesNoElsAreaRedirection(request):
    return redirect('els_assessment_yes_no_xyz', act=1, assessment_date=request.session['assessment_date'], area=request.session['area_id'])
