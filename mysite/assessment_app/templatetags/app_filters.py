from django import template

register = template.Library()


@register.filter(name='remove_comma')
def remove_comma(value):
    value = str(value)
    return value.replace(",","")

@register.filter('get_value_from_dict')
def get_value_from_dict(dict_data, key):
    """
    usage example {{ your_dict|get_value_from_dict:your_key }}
    """
    if key:
        return dict_data.get(key)

@register.filter('get_nested loop value')
def get_nested_loop_value(first, second):
    """
    usage example {{ your_dict|get_value_from_dict:your_key }}
    """
    return first.second