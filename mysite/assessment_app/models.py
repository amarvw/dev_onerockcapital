from django.db import models
from django.contrib.auth.models import User
import os
import json
from uuid import uuid4
from datetime import datetime, date
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
# from .models import Role
# from audit_log.models import AuthStampedModel
# from audit_log.models.fields import CreatingUserField, CreatingSessionKeyField
# Create your models here.
# from django.db import models

# Create your models here.
# class Role(models.Model):
#     role_type = models.CharField(unique=True, max_length=100)

User.__str__ = lambda user: user.get_full_name()


class GlobalRegionMaster(models.Model):
    objects = models.Manager()
    region = models.CharField(max_length=500)

    def __str__(self):
        return self.region


class RegionCountryMapping(models.Model):
    objects = models.Manager()
    country = models.CharField(max_length=500)
    iso = models.CharField(max_length=10)
    region = models.ForeignKey(GlobalRegionMaster, related_name='region_global_category', on_delete=models.CASCADE)

    def __str__(self):
        return self.country


class CountryStateMapping(models.Model):
    objects = models.Manager()
    country = models.ForeignKey(RegionCountryMapping, related_name='country_mapping_category', on_delete=models.CASCADE)
    region = models.ForeignKey(GlobalRegionMaster, related_name='region_mapping_category', on_delete=models.CASCADE)
    state = models.CharField(max_length=500)

    def __str__(self):
        return self.state


class PorfolioCompanyMaster(models.Model):
    status = (
        (True, 'Inactive'),
        (False, 'Active')
    )

    aquired = (
        (True, 'Aquired'),
        (False, 'Due-Diligence')
    )

    objects = models.Manager()
    portco_name = models.CharField(unique=True,max_length=100)
    hq_region = models.ForeignKey(GlobalRegionMaster, related_name='portco_global_region', null=True, on_delete=models.CASCADE)
    hq_country = models.ForeignKey(RegionCountryMapping, related_name='portco_region_country', null=True, on_delete=models.CASCADE)
    hq_state = models.ForeignKey(CountryStateMapping, related_name='portco_country_state', null=True,
                                                   on_delete=models.CASCADE)
    industry = models.CharField(max_length=100)
    # industry = models.ForeignKey(IndustryMaster, related_name='client_industry', on_delete=models.CASCADE)
    primary_contact = models.CharField(max_length=100)
    primary_contact_mail = models.EmailField(max_length=100)
    description = models.CharField(max_length=500,blank=True, null=True)
    # logo = models.ImageField(upload_to='media/', default='default.png', blank=True)
    is_delete = models.BooleanField(choices=status, default=False)
    is_aquired = models.BooleanField(choices=aquired, default=False)
    added_by = models.ForeignKey(User, related_name='portco_added_by', null=True, on_delete=models.CASCADE)
    modified_by = models.ForeignKey(User, related_name='portco_modified_by', null=True, on_delete=models.CASCADE)
    added_on = models.DateTimeField(null=True)
    modified_on = models.DateTimeField(default=datetime.now, blank=True)

    def __str__(self):
        return self.portco_name


class UserDetails(models.Model):
    status = (
        (True, 'Inactive'),
        (False, 'Active')
    )
    is_orc_choice = (
        (u'', 'Employee Type'),
        (True, 'One Rock Capital Employee'),
        (False, 'Non One Rock Capital Employee')
    )

    objects = models.Manager()
    user = models.OneToOneField(User, related_name='user_id', null=True, on_delete=models.CASCADE)
    # tv_id = models.CharField(unique=True, max_length=50)
    # plant = models.ForeignKey(ManufacturingPlantMaster, max_length=10, on_delete=models.CASCADE)
    is_delete = models.BooleanField(choices=status, default=False)
    is_orc_employee = models.BooleanField(choices=is_orc_choice)
    is_confirm = models.BooleanField(default=False)
    designation_title = models.CharField(max_length=100)
    added_by = models.ForeignKey(User, related_name='user_detail_added_by', null=True, on_delete=models.CASCADE)
    modified_by = models.ForeignKey(User, related_name='user_detail_modified_by', null=True, on_delete=models.CASCADE)
    added_on = models.DateTimeField(default=datetime.now,blank=True)
    modified_on = models.DateTimeField(default=datetime.now, blank=True)
    portco = models.ForeignKey(PorfolioCompanyMaster, related_name='portco1', null=True, on_delete=models.CASCADE)


# class RegionMaster(models.Model):
#     objects = models.Manager()
#     country = models.CharField(max_length=500)
#     region = models.CharField(max_length=500)
#     state = models.CharField(max_length=500)
#     # added_by = models.ForeignKey(User, related_name='region_added_by', null=True, on_delete=models.CASCADE)
#     # modified_by = models.ForeignKey(User, related_name='region_modified_by', null=True, on_delete=models.CASCADE)
#     # added_on = models.DateTimeField(null=True)
#     # modified_on = models.DateTimeField(default=datetime.now, blank=True)


class PlantMaster(models.Model):
    class Meta:
        unique_together = (('plant_name', 'portco'),)

    status = (
        (True, 'Inactive'),
        (False, 'Active')
    )

    objects = models.Manager()
    plant_name = models.CharField(max_length=100)
    portco = models.ForeignKey(PorfolioCompanyMaster,related_name='portco', on_delete=models.CASCADE)
    region = models.ForeignKey(GlobalRegionMaster, related_name='plant_global_region', null=True, on_delete=models.CASCADE)
    country = models.ForeignKey(RegionCountryMapping, related_name='plant_region_country', null=True, on_delete=models.CASCADE)
    state = models.ForeignKey(CountryStateMapping, related_name='plant_country_state', null=True, on_delete=models.CASCADE)
    primary_contact = models.CharField(max_length=100)
    primary_contact_mail = models.EmailField(max_length=100)
    description = models.CharField(max_length=500, blank=True, null=True)
    is_delete = models.BooleanField(choices=status, default=False)
    added_by = models.ForeignKey(User, related_name='plant_added_by', null=True, on_delete=models.CASCADE)
    modified_by = models.ForeignKey(User, related_name='plant_modified_by', null=True, on_delete=models.CASCADE)
    added_on = models.DateTimeField(null=True)
    modified_on = models.DateTimeField(default=datetime.now, blank=True)

    def __str__(self):
        return self.plant_name


class FourPMaster(models.Model):
    objects = models.Manager()
    description = models.CharField(max_length=500)

    def __str__(self):
        return self.description


class ELSCategoryMaster(models.Model):
    status = (
        (True, 'Inactive'),
        (False, 'Active')
    )

    objects = models.Manager()
    category_name = models.CharField(max_length=500)
    category_description = models.CharField(max_length=500)
    four_P_category = models.ForeignKey(FourPMaster, related_name='four_P_master',
                               on_delete=models.CASCADE)
    is_delete = models.BooleanField(choices=status, default=False)
    added_by = models.ForeignKey(User, related_name='els_category_added_by', null=True, on_delete=models.CASCADE)
    modified_by = models.ForeignKey(User, related_name='els_category_modified_by', null=True, on_delete=models.CASCADE)
    added_on = models.DateTimeField(null=True)
    modified_on = models.DateTimeField(default=datetime.now, blank=True)
    priority_rank = models.IntegerField(null=True)

    def __str__(self):
        return self.category_name


class ELSCategoryRatingMaster(models.Model):
    objects = models.Manager()
    els_category = models.ForeignKey(ELSCategoryMaster, related_name='els_category_master', null=True,
                                        on_delete=models.CASCADE)
    els_category_priority = models.ForeignKey(ELSCategoryMaster, related_name='els_category_priority', null=True, on_delete = models.CASCADE)
    score = models.IntegerField()
    description = models.CharField(max_length=2000)
    added_by = models.ForeignKey(User, related_name='els_rating_added_by', null=True, on_delete=models.CASCADE)
    modified_by = models.ForeignKey(User, related_name='els_rating_modified_by', null=True, on_delete=models.CASCADE)
    added_on = models.DateTimeField(null=True)
    modified_on = models.DateTimeField(default=datetime.now, blank=True)

    def __str__(self):
        return self.description


class AreaDetails(models.Model):
    status = (
        (True, 'Inactive'),
        (False, 'Active')
    )

    objects = models.Manager()
    area_name = models.CharField(max_length=500)
    plant = models.ForeignKey(PlantMaster, related_name='plant_area',
                                  on_delete=models.CASCADE)
    portco = models.ForeignKey(PorfolioCompanyMaster, related_name='portco_area',
                                  on_delete=models.CASCADE)
    description = models.CharField(max_length=2000)
    is_delete = models.BooleanField(choices=status, default=False)
    added_by = models.ForeignKey(User, related_name='area_added_by', null=True, on_delete=models.CASCADE)
    modified_by = models.ForeignKey(User, related_name='area_modified_by', null=True, on_delete=models.CASCADE)
    added_on = models.DateTimeField(null=True)
    modified_on = models.DateTimeField(default=datetime.now, blank=True)

    def __str__(self):
        return self.area_name


class ELSRatings(models.Model):
    status = (
        (True, 'Inactive'),
        (False, 'Active')
    )

    objects = models.Manager()
    score = models.ForeignKey(ELSCategoryRatingMaster, related_name='score_els', on_delete=models.CASCADE, null=True)
    score_y_n = models.IntegerField(null=True)
    plant = models.ForeignKey(PlantMaster, related_name='portco_els', on_delete=models.CASCADE)
    portco = models.ForeignKey(PorfolioCompanyMaster, related_name='portco_els', on_delete=models.CASCADE)
    els_category = models.ForeignKey(ELSCategoryMaster, related_name='els_category_scored', on_delete=models.CASCADE)
    description = models.CharField(max_length=2000, null=True)
    date_of_assessment_started = models.DateField(default=date.today)
    date_of_assessment = models.DateField(default=date.today)
    is_draft = models.BooleanField(default=True)
    is_evidence = models.BooleanField(default=False)
    added_by = models.ForeignKey(User, related_name='els_added_by', null=True, on_delete=models.CASCADE)
    modified_by = models.ForeignKey(User, related_name='els_modified_by', null=True, on_delete=models.CASCADE)
    added_on = models.DateTimeField(null=True)
    modified_on = models.DateTimeField(default=datetime.now, blank=True)


class AreaRatings(models.Model):
    status = (
        (True, 'Inactive'),
        (False, 'Active')
    )

    objects = models.Manager()
    score = models.ForeignKey(ELSCategoryRatingMaster, related_name='score_els_area', on_delete=models.CASCADE)
    plant = models.ForeignKey(PlantMaster, related_name='portco_els_area', on_delete=models.CASCADE)
    portco = models.ForeignKey(PorfolioCompanyMaster, related_name='portco_els_area', on_delete=models.CASCADE)
    area = models.ForeignKey(AreaDetails, related_name='els_area', on_delete=models.CASCADE)
    els_category = models.ForeignKey(ELSCategoryMaster, related_name='els_category_scored_area', on_delete=models.CASCADE)
    description = models.CharField(max_length=2000, null=True)
    is_evidence = models.BooleanField(default=False)
    date_of_assessment_started = models.DateField(default=date.today)
    added_by = models.ForeignKey(User, related_name='els_area_added_by', null=True, on_delete=models.CASCADE)
    modified_by = models.ForeignKey(User, related_name='els_area_modified_by', null=True, on_delete=models.CASCADE)
    added_on = models.DateTimeField(null=True)
    modified_on = models.DateTimeField(default=datetime.now, blank=True)


class Leadership_Category(models.Model):
    status = (
        (True, 'Active'),
        (False, 'Inactive')
    )
    objects = models.Manager()
    category_name = models.CharField(unique=True,max_length=100)
    category_description = models.CharField(max_length=500,blank=True, null=True)
    is_active = models.BooleanField(choices=status, default=True)
    added_by = models.ForeignKey(User, related_name='lship_category_added_by', null=True, on_delete=models.CASCADE)
    modified_by = models.ForeignKey(User, related_name='lship_category_modified_by', null=True, on_delete=models.CASCADE)
    added_on = models.DateTimeField(default=datetime.now, blank=True)
    modified_on = models.DateTimeField(default=datetime.now, blank=True)

    def __str__(self):
        return self.category_name


class Leadership_Level(models.Model):
    status = (
        (True, 'Active'),
        (False, 'Inactive')
    )
    objects = models.Manager()
    level_name = models.CharField(max_length=100)
    level_description = models.CharField(max_length=500,blank=True, null=True)
    is_active = models.BooleanField(choices=status, default=True)
    added_by = models.ForeignKey(User, related_name='lship_level_added_by', null=True, on_delete=models.CASCADE)
    modified_by = models.ForeignKey(User, related_name='lship_level_modified_by', null=True,
                                    on_delete=models.CASCADE)
    added_on = models.DateTimeField(default=datetime.now, blank=True)
    modified_on = models.DateTimeField(default=datetime.now, blank=True)


class Leadership_Representation(models.Model):
    status = (
        (True, 'Active'),
        (False, 'Inactive')
    )
    objects = models.Manager()
    category_name = models.CharField(max_length=100)
    category_description = models.CharField(max_length=500, blank=True, null=True)
    level_name = models.CharField(max_length=100)
    level_description = models.CharField(max_length=500, blank=True, null=True)
    # score = models.IntegerField()
    # score_representation = models.CharField(max_length=2000, blank=True, null=True)
    is_active = models.BooleanField(choices=status, default=True)
    added_by = models.ForeignKey(User, related_name='leadership_added_by', null=True, on_delete=models.CASCADE)
    modified_by = models.ForeignKey(User, related_name='leadership_modified_by', null=True,
                                    on_delete=models.CASCADE)
    added_on = models.DateTimeField(default=datetime.now, blank=True)
    modified_on = models.DateTimeField(default=datetime.now, blank=True)


class LshipCategoryRatingMaster(models.Model):
    objects = models.Manager()
    lship_representation = models.ForeignKey(Leadership_Representation, related_name='lship_master', null=True,
                                        on_delete=models.CASCADE)
    score = models.IntegerField()
    score_representation = models.CharField(max_length=2000)
    added_by = models.ForeignKey(User, related_name='lship_rating_added_by', null=True, on_delete=models.CASCADE)
    modified_by = models.ForeignKey(User, related_name='lship_rating_modified_by', null=True, on_delete=models.CASCADE)
    added_on = models.DateTimeField(default=datetime.now, blank=True)
    modified_on = models.DateTimeField(default=datetime.now, blank=True)


class LeadershipRatings(models.Model):
    objects = models.Manager()
    score = models.ForeignKey(LshipCategoryRatingMaster, related_name='score_lship', on_delete=models.CASCADE)
    portco = models.ForeignKey(PorfolioCompanyMaster, related_name='portco_lship', on_delete=models.CASCADE)
    lship_id = models.ForeignKey(Leadership_Representation, related_name='lship_repre_id', on_delete=models.CASCADE)
    description = models.CharField(max_length=2000, null=True)
    date_of_assessment_started = models.DateField(default=date.today)
    date_of_assessment = models.DateField(default=date.today)
    is_draft = models.BooleanField(default=True)
    added_by = models.ForeignKey(User, related_name='lship_added_by', null=True, on_delete=models.CASCADE)
    modified_by = models.ForeignKey(User, related_name='lship_modified_by', null=True, on_delete=models.CASCADE)
    added_on = models.DateTimeField(null=True)
    modified_on = models.DateTimeField(default=datetime.now, blank=True)


class TrainingContentMaster(models.Model):
    status = (
        (True, 'Inactive'),
        (False, 'Active')
    )

    objects = models.Manager()
    module_name = models.CharField(max_length=50, blank=True, null=True)
    module_description = models.CharField(max_length=2000,blank=True, null=True)
    document = models.BooleanField(default=False, blank=True, null=True)
    document_name = models.CharField(max_length=100,blank=True, null=True)
    # logo = models.ImageField(upload_to='media/', default='default.png', blank=True)
    is_delete = models.BooleanField(choices=status, default=False)
    added_by = models.ForeignKey(User, related_name='training_added_by', null=True, on_delete=models.CASCADE)
    modified_by = models.ForeignKey(User, related_name='training_modified_by', null=True, on_delete=models.CASCADE)
    added_on = models.DateTimeField(null=True)
    modified_on = models.DateTimeField(default=datetime.now, blank=True)

    def __str__(self):
        return self.module_name


################################# Yes/No Modue #####################

class ELSCategoryQuestionMaster(models.Model):
    objects = models.Manager()
    els_category = models.ForeignKey(ELSCategoryMaster, related_name='els_category_question_master',
                                        on_delete=models.CASCADE)
    is_target = models.BooleanField(default=True)
    weight_y = models.IntegerField()
    weight_n = models.IntegerField()
    description = models.CharField(max_length=2000)
    added_by = models.ForeignKey(User, related_name='els_rating_question_added_by', null=True, on_delete=models.CASCADE)
    modified_by = models.ForeignKey(User, related_name='els_rating_question_modified_by', null=True, on_delete=models.CASCADE)
    added_on = models.DateTimeField(null=True)
    modified_on = models.DateTimeField(default=datetime.now, blank=True)

    def __str__(self):
        return self.description


class ELSCategoryQuestionResponse(models.Model):
    status = (
        (True, 'Inactive'),
        (False, 'Active')
    )

    objects = models.Manager()
    # score = models.ForeignKey(ELSCategoryQuestionMaster, related_name='score_question_els', on_delete=models.CASCADE)
    weight = models.IntegerField()
    question = models.ForeignKey(ELSCategoryQuestionMaster, related_name='score_question_els', on_delete=models.CASCADE)
    response = models.BooleanField(default=True)
    plant = models.ForeignKey(PlantMaster, related_name='portco_question_els', on_delete=models.CASCADE)
    portco = models.ForeignKey(PorfolioCompanyMaster, related_name='portco_question_els', on_delete=models.CASCADE)
    els_category = models.ForeignKey(ELSCategoryMaster, related_name='els_category_question_scored', on_delete=models.CASCADE)
    date_of_assessment = models.DateField(default=date.today)
    added_by = models.ForeignKey(User, related_name='els_added_question_by', null=True, on_delete=models.CASCADE)
    modified_by = models.ForeignKey(User, related_name='els_modified_question_by', null=True, on_delete=models.CASCADE)
    added_on = models.DateTimeField(null=True)
    modified_on = models.DateTimeField(default=datetime.now, blank=True)


# class ELSCategoryQuestionScore(models.Model):
#     status = (
#         (True, 'Inactive'),
#         (False, 'Active')
#     )
#
#     objects = models.Manager()
#     # score = models.ForeignKey(ELSCategoryQuestionMaster, related_name='score_question_els', on_delete=models.CASCADE)
#     score = models.IntegerField()
#     plant = models.ForeignKey(PlantMaster, related_name='portco_question_score_els', on_delete=models.CASCADE)
#     portco = models.ForeignKey(PorfolioCompanyMaster, related_name='portco_question_score_els', on_delete=models.CASCADE)
#     els_category = models.ForeignKey(ELSCategoryMaster, related_name='els_category_question_scored_score', on_delete=models.CASCADE)
#     date_of_assessment = models.DateField(default=date.today)
#     is_draft = models.BooleanField(default=True)
#     is_evidence = models.BooleanField(default=False)
#     added_by = models.ForeignKey(User, related_name='els_added_question_score_by', null=True, on_delete=models.CASCADE)
#     modified_by = models.ForeignKey(User, related_name='els_modified_question_score_by', null=True, on_delete=models.CASCADE)
#     added_on = models.DateTimeField(null=True)
#     modified_on = models.DateTimeField(default=datetime.now, blank=True)